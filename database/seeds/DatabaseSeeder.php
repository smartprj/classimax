<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
    		'name'=>'admin',
    		'username'=>'admin',
    		'email'=>'admin@classimax.com',
    		'password'=>bcrypt('admin@123')
    	]);
    }
}
