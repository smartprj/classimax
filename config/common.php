<?php
return 
    [
        'validity_type' => [
            "1" => 'Day(s)',
            "2" => 'Month(s)',
            "3" => 'Year(s)'
        ],
        'validity_type_dropdown' => [
            "1" => 'Day Wise',
            "2" => 'Month Wise',
            "3" => 'Year Wise'
        ],

        'status' => [
            "0" => 'Not Active',
            "1" => 'Active'
        ],
        'enquiry_status' => [
            "0" => 'In-Active',
            "1" => 'Processed / Active',
            "2" => 'Requested',
            "3" => 'Verification Pending'
        ],
        'enquiry_status_color' => [
            "0" => 'danger',
            "1" => 'success',
            "2" => 'warning',
            "3" => 'danger'
        ],
        'is_admin' => [
            "0" => 'No',
            "1" => 'Yes'
        ],
        'user_type' => [
            "0" => 'Not a user',
            "1" => 'Web User',
            "2" => 'Packer'
        ],
        'rating' => [
            "1" => '1 Star',
            "2" => '2 Star',
            "3" => '3 Star',
            "4" => '4 Star',
            "5" => '5 Star'
        ],
        'issues' => [
            "0" => 'In progress',
            "1" => 'Issue Resolved',
            "2" => 'Rejected',
            "3" => 'Refund Successfully',
           
        ],
         'issues_color' => [
            "0" => 'warning',
            "1" => 'success',
            "2" => 'danger',
            "3" => 'info'
        ],
        'profile_request_status' => [
            "0" => 'Approved',
            "1" => 'Pending',
            "2" => 'Rejected'
        ],
    ];
?>