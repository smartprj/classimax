<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Plan extends Model
{
    protected $table        = 'plans';
    protected $primaryKey   = 'id';
    protected $fillable     = [
        'name','price', 'discount_price','validity_type','validity','facilities','is_premium','description', 'status', 'created_at','created_by', 'updated_at', 'updated_by'
    ];

    public static function scopeSearch($query,$where=[]){
        return $query->where($where);
    }
}
