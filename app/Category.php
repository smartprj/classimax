<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table        = 'categories';
    protected $primaryKey   = 'id';
    protected $fillable     = [
        'parent_id', 'name','status','home_dispaly','image', 'created_at', 'created_by', 'updated_at', 'updated_by'
    ];

    public static function scopeSearch($query,$where=[]){
        return $query->where($where);
    }

    public static function uploadImages($image,$name) {
        $imageName = $name.time().'.'.$image->extension();
        $image->move(public_path('images/category/'), $imageName);
        $imageName = filter_var($imageName,FILTER_SANITIZE_STRING);
        return $imageName;
    }
}
