<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    protected $table        = 'leads';
    protected $primaryKey   = 'id';
    protected $fillable     = [
       'enquiry_id','user_id','status','amount','user_wallet'
    ];


}
