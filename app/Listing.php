<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Listing extends Model
{
    protected $table        = 'listing';
    protected $primaryKey   = 'id';
    protected $fillable     = [
        'name','code', 'state','location','address','mobile_no','alternate_no','image','description','slug','meta_title','meta_description', 'status', 'created_at','created_by', 'updated_at', 'updated_by','position','rating','email'
    ];

    public static function scopeSearch($query,$where=[]){
        return $query->where($where);
    }
    public function getCities(){

    	return Listing::join('districts', 'listing.location', '=', 'districts.id')->distinct()->pluck('districts.name','districts.id')->toArray();
    }

    public static function uploadImages($image,$name) {
        $imageName = $name.time().'.'.$image->extension();
        $image->move(public_path('images/listing/'), $imageName);
        $imageName = filter_var($imageName,FILTER_SANITIZE_STRING);
        return $imageName;
    }

}
