<?php

namespace App\Http\Controllers;

use App\Listing;
use App\State;
use App\District;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class ListingController extends Controller
{

    public function __construct(){
        // $this->middleware('auth');
        $this->middleware('auth:admin');
        $this->listing  = new Listing;
        $this->state    = new State;
        $this->district = new District;
    }

    public function index()
    {
        $states     = $this->state->where(['country_id'=>105])->pluck('name','id');
        $districts  = $this->district->where(['country_id'=>105])->pluck('name','id');
        $listings   = $this->listing->latest()->paginate(5);
        return view('listings.index',compact('listings','states','districts'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        $states = $this->state->where(['country_id'=>105,'status'=>1])->pluck('name','id');
        return view('listings.create',compact('states'));
    }

    public function store(Request $request)
    {
        $lid = $this->listing->select('id')->latest('id')->first();
        $this->listingValidate($request);
        $data           = $request->all();
        if(!empty($data['listing_image'])){
            $data['image']  = $this->listing->uploadImages($data['listing_image'],'listing');
        }
        if(empty($data['position'])){
            $data['position']=($lid->id+1);
        }

        $this->listing->create($data);
        return redirect()->route('listings.index')
                        ->with('success','Listing created successfully.');
    }

    public function show(Listing $listing)
    {
        $states     = $this->state->where(['country_id'=>105])->pluck('name','id');
        $districts  = $this->district->where(['country_id'=>105])->pluck('name','id');
        return view('listings.show',compact('listing','states','districts'));
    }

    public function edit(Listing $listing)
    {
        $states     = $this->state->where(['country_id'=>105,'status'=>1])->pluck('name','id');
        $districts  = $this->district->where(['state_id'=>$listing->state])->pluck('name','id');
        return view('listings.edit',compact('listing','states','districts'));
    }

    public function update(Request $request, Listing $listing)
    {
        $this->listingValidate($request,$listing->id);
        $data               = $request->all();
        if(!empty($data['listing_image'])){
            $data['image']  = $this->listing->uploadImages($data['listing_image'],'listing');
        }
        $listing->update($data);
        return redirect()->route('listings.index')
                        ->with('success','Listing updated successfully');
    }

    public function destroy(Listing $listing)
    {
        $listing->delete();
        return redirect()->route('listings.index')
                        ->with('success','Listing deleted successfully');
    }

    private function listingValidate($request,$id=null){
        $validate['name']               = 'required';
        $validate['status']             = 'required';
        $validate['state']              = 'required';
        $validate['location']           = 'required';
        $validate['address']            = 'required';
        $validate['mobile_no']          = 'required';
        // if(!empty($id)){
        //     $validate['code']           = 'required|unique:listing,code,'.$id.',id';
        // }else{
        //     $validate['code']           = 'required|unique:listing,code,NULL,id';
        // }

        $messages = [
           'address.required'           => __('Please Enter Address'),
           'mobile_no.required'         => __('Please Enter Mobile Number'),
           'status.required'            => __('Please Select Status'),
           'state.required'             => __('Please Select State'),
           'location.required'          => __('Please Select Location'),
           'name.required'              => __('Please Enter Listing Name'),
           // 'code.required'              => __('Please Enter Listing Code'),
           // 'code.unique'                => __('Please Enter Unique Listing Code')
        ];
        $request->validate($validate,$messages);  
    }

    public function getlocation(Request $request){
        $districts = [];
        if(!empty($request->id)){
            $districts = $this->district->where(['state_id'=>$request->id])->pluck('name','id')->toArray();
        }
        return view('listings.getlocation',compact('districts'));
    }
}
