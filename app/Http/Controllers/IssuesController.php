<?php


namespace App\Http\Controllers;
use Config;
use App\RaiseTicket;
use App\Category;
use Illuminate\Http\Request;

class IssuesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function __construct(){
        $this->middleware('auth:admin');
        $this->ticket                 = new RaiseTicket();
        $this->category                = new Category();
        $this->status                  = Config::get('common.issues');
        $this->statusColor             = Config::get('common.issues_color');
    }

    public function index(){

         $status         = $this->status;
        $statusColor    = $this->statusColor;
        
        $enquiries      = $this->ticket->join('users', 'users.id', '=', 'raise_ticket.user_id')->select('raise_ticket.*','users.name')->latest()->paginate(15);

        return view('issues.index',compact('enquiries','status','statusColor'))->with('i', (request()->input('page', 1) - 1) * 15);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
       $enquiry      = $this->ticket->join('users', 'users.id', '=', 'raise_ticket.user_id')->where('raise_ticket.id',$id)->select('raise_ticket.*','users.name','users.email')->first();
      
        $status         = $this->status;
        return view('issues.edit',compact('enquiry','status'));
    }

    public function update(Request $request,RaiseTicket $ticket)
    {
        $this->listingValidate($request,$ticket->id);
        $data               = $request->all();
       
        $res = $ticket->update($data);
         
        return redirect()->route('issue.index')->with('success','Ticket updated successfully');
    }
 private function listingValidate($request,$id=null){
    
        $validate['remark']               = 'required';
        $validate['status']                           = 'required';
        $messages = [
           'remark.required'              => __('Please Enter Remark'),
           'status.required'                          => __('Please Select Status'),
        ];
        $request->validate($validate,$messages);  
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
