<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use App\Providers\RouteServiceProvider;
use App\User;
use App\District;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'web_url' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $status = 4;
        if($data['type'] == 1){
            $status = 1;
        }

        $register['name']       = $data['name'];
        $register['email']      = $data['email'];
        $register['type']       = $data['type'];
        $register['web_url']    = $data['web_url'];
        $register['password']   = Hash::make($data['password']);
        $register['status']     = $status;
        if($data['type'] != 1){
            $locationName               = District::getLocationName($data['location']);
            $register['slug']           = Str::slug($data['name']).'-'.Str::lower($locationName);
            $register['phone_no']       = $data['phone_no']??"";
            $register['gst_no']         = $data['gst_no']??"";
            $register['location']       = $data['location']??"";
            $register['token']          = Str::random(4);
            if(!empty($data['company_logo'])){
                $register['company_logo']   = User::uploadCompanyLogo($data['company_logo'],'companylogo');
            }
            $template                   = "mail.verify";
            $register['subject']        = " Verification Token ";
            $register['to']             = $register['email'];
            USER::sendMail($register,$template);
            Session::flash('success', 'A verification token has been sent to your email ID!'); 
        }
        return User::create($register);
        // return User::create([
        //     'name' => $data['name'],
        //     'email' => $data['email'],
        //     'type' => $data['type'],
        //     'password' => Hash::make($data['password']),
        //     'status'=>$status,
        //     'is_admin'=>0
        // ]);
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        $redirect = "verifyPacker";
        if($request->type == 1){
            $this->guard('web')->login($user);
            $redirect = "home";
        }

        if ($response = $this->registered($request, $user)) {
            return $response;
        }

        return $request->wantsJson()
                    ? new JsonResponse([], 201)
                    : redirect()->route($redirect);
    }
}
