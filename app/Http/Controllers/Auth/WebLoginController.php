<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Route;
use App\WebUser;
use Session;

class WebLoginController extends Controller
{
    public function __construct(){
      $this->middleware('guest:web', ['except' => ['logout','weblogin']]);
      $this->user     = new WebUser;
    }
    
    public function weblogin(){
      if(!empty(Auth::user())){
        return redirect('home');
      }
      return view('auth.weblogin');
    }

    public function webRegister(){
      return view('auth.webregister');
    }

    public function packerRegister()
    {
      return view('auth.packerregister');
    }

    public function verifyPacker()
    {
      return view('auth.verifypacker');
    }

     public function webauthlogin_old(Request $request)
    {  
        $inputVal = $request->all();
   
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
   
        if(Auth::guard('admin')->attempt(array('email' => $inputVal['email'], 'password' => $inputVal['password'],'status'=>1))){
            if (Auth::guard('admin')->user()->is_admin == 1) {
                return redirect()->route('admin');
            }else{

              Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password,'status'=>1], $request->remember);
                return redirect()->route('home');
            }
        }else{
            if(Auth::guard('admin')->attempt(array('email' => $inputVal['email'], 'password' => $inputVal['password'],'is_admin' => 1,'status'=>1))){
                return redirect()->route('login')
                ->with('error','Email & Password are incorrect.');
            }else{
                return redirect()->route('weblogin')
                ->with('error','Email & Password are incorrect.');
            }
        }     
    }
    
    public function webauthlogin(Request $request){
      // Validate the form data
      $this->validate($request, [
        'email'   => 'required|email',
        'password' => 'required'
      ]);
      
      if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password,'status'=>1], $request->remember)) {
        // return redirect()->intended(route('home'));
        return redirect('dashboard');
      }
      // if unsuccessful, then redirect back to the login with the form data
      return redirect()->back()->withInput($request->only('email', 'remember'))->with('error','Email or Password are incorrect.');
    }

    public function verifyPackerEmail(Request $request){
      // Validate the form data
      $this->validate($request, [
        'email'   => 'required|email',
        'token' => 'required'
      ]);
      
      $chkStatus = $this->user->where(['email'=>$request->email])->first();
      if(!empty($chkStatus)){
        if($chkStatus->token == $request->token && $chkStatus->status == 4){
          $this->user->where(['email'=>$request->email])->update(['status'=>3,'token'=>""]);
          // Session::flash('success', 'Your registration request has been sent for approval!'); 
          return redirect()->route('weblogin')->with('success','Email verified successfully ! Your registration request has been sent for approval!.');
        }
      }
      // if unsuccessful, then redirect back to the login with the form data
      return redirect()->back()->withInput($request->only('email'))->with('error','Email and token did not match ! Please try again');
    }
    
    public function logout()
    {
      Auth::guard('web')->logout();
      return redirect('/');
    }
}
