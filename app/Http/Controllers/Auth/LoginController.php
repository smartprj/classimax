<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/admin/route';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function login(Request $request)
    {  
        $inputVal = $request->all();
   
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
   
        if(Auth::guard('admin')->attempt(array('email' => $inputVal['email'], 'password' => $inputVal['password'],'status'=>1))){
            if (Auth::guard('admin')->user()->is_admin == 1) {
                return redirect()->route('admin');
            }else{
                Auth::logout();
                return redirect()->route('home');
            }
        }else{
            if(Auth::guard('admin')->attempt(array('email' => $inputVal['email'], 'password' => $inputVal['password'],'is_admin' => 1,'status'=>1))){
                return redirect()->route('login')
                ->with('error','Email & Password are incorrect.');
            }else{
                return redirect()->route('weblogin')
                ->with('error','Email & Password are incorrect.');
            }
        }     
    }

    public function logout(Request $request){
        Auth::guard('admin')->logout();
        return redirect('/admin');
    }
}