<?php

namespace App\Http\Controllers;
use Config;
use App\User;
use App\WebUser;
use App\District;
use App\Enquiry;
use App\Lead;
use App\RaiseTicket;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Payment;

class PackersController extends Controller
{
    public function __construct(){
        $this->middleware('auth:web');
        $this->user         = new User;
        $this->webUser      = new WebUser;
        $this->district     = new District();
        $this->Enquiry      = new Enquiry();
        $this->Lead         = new Lead();
        $this->Order        = New Payment();
        $this->category     = New Category();
        $this->status                  = Config::get('common.status');
        $this->userType                = Config::get('common.user_type');
        $this->isAdmin                 = Config::get('common.is_admin');
        $this->statusColor             = Config::get('common.enquiry_status_color');
        $this->rating                  = Config::get('common.rating');
    }

    public function myProfile(){
        $disName            = "";
        $authUser           = Auth::user();
        $user               = $this->webUser->where('id','=',$authUser->id)->first();
        $status             = $this->status;
        $userType           = $this->userType;
        $isAdmin            = $this->isAdmin;
        $rating             = $this->rating;
        if(!empty($user->location)){
            $getDistrict    = $this->district->Where(['id'=>$user->location])->first()->toArray();
            $disName        = $getDistrict['name'];
        }
        return view('packers.my_profile',compact('authUser','user','status','userType','rating','disName'));
    }

    public function editMyProfile(){
        $disName            = "";
        $authUser           = Auth::user();
        $user               = $this->webUser->where('id','=',$authUser->id)->first();
        $status             = $this->status;
        $userType           = $this->userType;
        $isAdmin            = $this->isAdmin;
        $rating             = $this->rating;
        if(!empty($user->location)){
            $getDistrict    = $this->district->Where(['id'=>$user->location])->first()->toArray();
            $disName        = $getDistrict['name'];
        }
        return view('packers.edit_my_profile',compact('authUser','user','status','userType','rating','disName'));
    }

    public function saveProfileRequest(Request $request){
        $data                   = $request->all();
        $userID                 = Auth::user()->id;
        $jsonIns['id']          = $userID;
        $jsonIns['name']        = !empty($data['name'])?filter_var($data['name'],FILTER_SANITIZE_STRING):"";
        $jsonIns['phone_no']    = !empty($data['phone_no'])?filter_var($data['phone_no'],FILTER_VALIDATE_INT):"";
        $jsonIns['gst_no']      = !empty($data['gst_no'])?filter_var($data['gst_no'],FILTER_SANITIZE_STRING):"";
        $jsonIns['address']     = !empty($data['address'])?filter_var($data['address'],FILTER_SANITIZE_STRING):"";
        $jsonIns['location']    = !empty($data['location'])?filter_var($data['location'],FILTER_VALIDATE_INT):"";
        $jsonIns['pin_code']    = !empty($data['pin_code'])?filter_var($data['pin_code'],FILTER_SANITIZE_STRING):"";
        $jsonData               = json_encode($jsonIns);
        // $this->webUser->where('email', $userEmail)->update(['member_type' => $plan]);
        if($this->webUser->where('id', $userID)->update(['profile_request' => $jsonData,'profile_request_status'=>1])){
        return redirect()->route('myProfile')
            ->with('success','Profile request has been sent for approval successfully !!');
        }else{
            return redirect()->back()->with('error','Something Went wrong !!');
        }
        return redirect()->back()->with('error','Please try again !!');
    }

    public function myLeadsDetail($enId){
        $id = base64_decode($enId);
        $listing = $this->Enquiry->where('status','=','1')->where('id',$id)->first();
        $quantityArr        = [];
        if(!empty($listing->quantity)){
            $quantityArr    = json_decode($listing->quantity,true);
        }
        $categoriesArr  = $this->category->where(['status'=>1])->pluck('name','id')->toArray();
        return view('packers.myleadsdetail',compact('listing','quantityArr','categoriesArr'));

    }

    public function myLeads(){
    	$authUser = Auth::user();
        $userLeads =  $this->Lead->where([['user_id','=', $authUser->id],['status','=',1]])->pluck('enquiry_id')->toArray();
        $listings = $this->Enquiry->where('status','=','1')->whereIn('id',$userLeads)->latest()->paginate(10);
        return view('packers.myleads',compact('listings'));
    }

    public function dashboard(){
        $authUser = Auth::user();
        $location = "";
        $userLeads =  $this->Lead->where([['user_id','=', $authUser->id],['status','=',1]])->pluck('enquiry_id')->toArray();
        if(!empty($authUser->location)){
            $location = $authUser->location;
        }
            $listings = $this->Enquiry->where('status','=','1')->where('source_locality',$authUser->location)->whereNotIn('id',$userLeads)->latest()->paginate(10);
        return view('packers.dashboard',compact('listings'));
    }


    public function raiseIssue(Request $request,$lead_id){

    	if($request->isMethod('post')){

    		 $data = $request->all();
    		 $data['lead_id'] = base64_decode(base64_decode($lead_id));
    		 $data['user_id'] = Auth::user()->id;

    		   if(RaiseTicket::create($data)){
    		   	return redirect()->route('dashboard')
                        ->with('success','We will review your request please wait for our response !!');

					}else{
							return redirect()->back()->with('error','Something Went wrong !!');
					}    		  
    		
    	}

    		
 			return view('packers.raiseissue');
    }
    
    public function buyLead(Request $request){
     	$res=['type'=>'fail','msg'=>''];
        $data = $request->all();
        $user  = Auth::user();
        $user  = $this->user->select('id','wallet')->where('id',$user->id)->first();
        $wallet = $user->wallet;
        $lid = base64_decode(base64_decode($data['lead']));
        $lead = $this->Enquiry->where('id',$lid)->first();
        $lcount = $this->Lead->where([['enquiry_id','=',$lid],['user_id','=',$user->id],['status','=',1]])->count();
        if($lcount!=0){
            $res['msg']='You already purchased this lead !!';
        }else if($lead['amount']>$wallet){
    	   $res['msg']='Please recharge you wallet first !!';

        }else if($lead['max_purchase_count'] <= $lead['available_count'] ){
            $res['msg']='Leads already purchased by another packers !!';
        }else{
    		  $insertPurchageLead=[];
    		  $insertPurchageLead['enquiry_id'] = $lid;
    		  $insertPurchageLead['user_id'] =  $user->id;
    		  $insertPurchageLead['status'] = 1;
    		  $insertPurchageLead['amount'] = $lead['amount'];
    		  $insertPurchageLead['user_wallet']= $wallet;
    		  if(Lead::create($insertPurchageLead)){
    		      $remainingAmount = $wallet-$lead['amount'];

    		  	   $count = $this->Lead->where([['enquiry_id','=',$lid],['status','=',1]])->count();
    		  	   $userLeadcount = $this->Lead->where([['user_id','=',$lid],['status','=',1]])->count();
    		  	   $lead = $this->Enquiry->where('id',$lid)->first();
    		  	   $remainingPurchase = $lead['max_purchase_count']-$count;
    		  	
    		  	   $this->Enquiry->where('id',$lid)->update(['available_count'=>$remainingPurchase]);

    		  	   $this->user->where('id',$user->id)->update(['wallet'=>$remainingAmount,'total_purchase_leads'=>$userLeadcount]);

    		  }
			 $res['type']='success';
			 $res['msg']='Successfully purchased';
        }
    	echo json_encode($res);
    	exit;
    }

    public function myPurchase(){
        $userid = Auth::user()->id;
        $where[] = ['emp_id','=',$userid];
        $orders   = $this->Order->where($where)->latest()->paginate(10);
        return view('packers.mypurchase',compact('orders'));
    }

    public function userWallet(){

    	$res['type']='Fail';
		$res['amount']=0;
 		$user  = Auth::user();
	    $user  = $this->user->select('id','wallet')->where('id',$user->id)->first();
	    if(!empty($user)){
 			$res['type']='success';
			$res['amount']=$user->wallet;
    	}
    	echo json_encode($res);
    	exit;

    }
}
