<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Razorpay\Api\Api;
use App\Payment;
use App\User;
use Session;
use Auth;
use Redirect;

class PaymentController extends Controller
{
    //
	 public function __construct(){
        $api_key = 'rzp_live_W13ZvxXxkVkObd';
        $api_secret = 'NDC39lRl5Vped16K222vqmrZ';
		$this->Api = new Api($api_key, $api_secret);
        $this->Order = New Payment();
         $this->user     = new User;
    }


        public function create()

    {        

        return view('home.payment');

    }


    public function payment(Request $request)

    {


    $response=[];
    $response['type']='fail';
    $response['msg']='Payment faild there is some issue with order !! please contact with customer care';
   

         $api_secret = 'NDC39lRl5Vped16K222vqmrZ';

        $input = $request->all();
      
//$payment = $this->Api->payment->fetch($input['razorpay_payment_id']);
       

    // $input["razorpay_payment_id"]= "pay_GuUcgFTUfFzdbl";
    // $input["razorpay_order_id"]= "order_GuUcJ1vOLjksEl";
    // $input["razorpay_signature"]= "1025bb05d279435b0d78ac75ff3699c50ddf414fdf5d5e3e6a8ea01785410802";

         $userid = Auth::user()->id;
           $user = $this->user->where('id',$userid)->first();

if(!empty($input['error'])){

      $where[] = ['order_id','=',$input['error']['metadata']['order_id']];
            $where[] = ['emp_id','=',$userid];
            $order   = $this->Order->where($where)->first();
            $insert['updated_at']           = date('Y-m-d H:i:s');
            $insert['razorpay_payment_id']           = $input['error']['metadata']['payment_id'];
            $insert['status']               = 2;
            $insert['response']             = json_encode($input['error']);
            $this->Order->where('id',$order->id)->update($insert);

             $response['type']='fail';
             $response['msg']='Payment Failed  !! ';
   
}else{


        $string    = $input['razorpay_order_id'] ."|" .$input['razorpay_payment_id'];
        $signature = hash_hmac('sha256', $string,$api_secret);


        if(empty($input['razorpay_order_id'])){

             $response['type']='fail';
              $response['msg']='Order not found !! please contact with customer care';

            

        }else if(!empty($input['razorpay_signature']) && $input['razorpay_signature']==$signature){

            $where   = [];
            $where[] = ['order_id','=',$input['razorpay_order_id']];
            $where[] = ['emp_id','=',$userid];
            $order   = $this->Order->where($where)->first();
            $insert=[];
            $insert['razorpay_payment_id']= $input['razorpay_payment_id'];
            $insert['razorpay_signature'] = $input['razorpay_signature'];
            $insert['status']             = 1;
            $coins = ($order->amount/10);
            $insert['payment_coins']      = $coins;
            $insert['user_last_wallet']      = $user->wallet;
            $insert['updated_at']           = date('Y-m-d H:i:s');
            $this->Order->where('id',$order->id)->update($insert);
          

          $userupdate['last_wallet']      = $user->wallet;
          $userupdate['wallet']      = $user->wallet+$coins;
            $this->user->where('id',$userid)->update($userupdate);

            $response['type']='success';
                $response['msg']='Payment success !! ';
        }else{
            $where[] = ['order_id','=',$input['razorpay_order_id']];
            $where[] = ['emp_id','=',$userid];
            $order   = $this->Order->where($where)->first();
            $insert['updated_at']           = date('Y-m-d H:i:s');
            $insert['status']           = 2;
            $this->Order->where('id',$order->id)->update($insert);
        }
}
       echo json_encode($response);
exit;

$api_key = 'rzp_live_W13ZvxXxkVkObd';
       
		$api = new Api($api_key, $api_secret);
        // $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));


        $payment = $api->payment->fetch($input['razorpay_payment_id']);


        if(count($input)  && !empty($input['razorpay_payment_id'])) {

            try {

                $response = $api->payment->fetch($input['razorpay_payment_id'])->capture(array('amount'=>$payment['amount'])); 


            } catch (\Exception $e) {

                return  $e->getMessage();

                \Session::put('error',$e->getMessage());

                return redirect()->back();

            }

        }

       
        \Session::put('success', 'Payment successful');

        return redirect()->back();

    }

public function generateOrderID(Request $request){

    $response=[];
    $response['type']='fail';
    $response['msg']='There is some issue with order';
    $response['order_id']='';
  //  $response['second_order_id']='';

        $data     = $request->all();
        $userid = Auth::user()->id;
        $order  = Payment::where('emp_id',$userid)->count();
        $rec    = 0;
            if(!empty($order)){
                 $rec=$order;
            }
            $rec++;

            $insert['recipt_no'] = 'RC'.$userid.$rec;
            $insert['emp_id']    = $userid;
            $insert['amount']    = $data['amount'];
            $insert['created_at']    = date('Y-m-d H:i:s');

            $order    = $this->Api->order->create(array('receipt' => $insert['recipt_no'], 'amount' => ($data['amount']*100), 'currency' => 'INR')); // Creates order
            $orderId  = $order['id']; // Get the created Order ID
            $insert['order_id']= $orderId;
            $lastid            = Payment::insertGetId($insert);
            if(!empty($lastid) && !empty($orderId)){
                 $response['type']='success';
                 $response['msg']='success';
                 $response['order_id']=$orderId;
                 //$response['second_order_id']=$lastid;
            }
            // $response[]
            echo json_encode($response);
        exit;
         
}
     public function payment_old(Request $request){
        $order    = $this->Api->order->create(array('receipt' => '123', 'amount' => 100, 'currency' => 'INR')); // Creates order
		$orderId  = $order['id']; // Get the created Order ID
		echo "<pre>";
		print_r($orderId);
			echo "<br>";
		$order    = $this->Api->order->fetch($orderId);
		print_r($order);

			echo "<br>";

		//$orders   = $this->Api->order->all($order); // Returns array of order objects


	


			$payments = $this->Api->payment->all($order); // Returns array of payment objects
			// $payment  = $this->Api->payment->fetch($orderId); // Returns a particular payment
			// $payment  = $this->Api->payment->fetch($orderId)->capture(array('amount'=>$amount)); // Captures a payment

			echo "<pre>";
		print_r($payments);
			exit;
			// To get the payment details
			echo $payment->amount;
			echo $payment->currency;
			    }
}
