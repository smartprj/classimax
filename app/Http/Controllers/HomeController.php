<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Listing;
use App\WebUser;
use App\District;
use App\Category;
use Auth;
use App\Enquiry;
use App\User;
use Illuminate\Support\Str;
use Mail;

class HomeController extends Controller
{
    
    public function __construct(){

        $this->listing = new WebUser();
        $this->Enquiry = new Enquiry();
        $this->District = new District();
        $this->category = new Category();
        $this->user = new User();
        // $this->middleware('auth:web');
    }
    public function testmail(){

          Mail::send([],[], function ($m){
            $m->from('info@moversnest.com', 'Your test');

            $m->to('anuragsharma9266@gmail.com', 'test')->subject('Your Reminder!');
        });
    }

    public function getLocations(Request $request){
        $search         = $request->search;
        if($search == ''){
            $locations  = $this->District->leftJoin('states','districts.state_id','states.id')->where(['districts.country_id'=>105])->orderby('districts.name','asc')->select('districts.id as d_id','districts.name as d_name','states.name as s_name')->limit(10)->get();
        }else{
            $locations  = $this->District->leftJoin('states','districts.state_id','states.id')->where(['districts.country_id'=>105])->orderby('districts.name','asc')->select('districts.id as d_id','districts.name as d_name','states.name as s_name')->where('districts.name', 'like', '%' .$search . '%')->limit(10)->get();
        }

        $response       = array();
        foreach($locations as $location){
            $response[] = array("value"=>$location->d_id,"label"=>$location->d_name." (".$location->s_name.")");
        }
        return response()->json($response);
    }

    public function index(Request $request)
    {
        $where = [];
        $location = "";
        $locationName = "";
        if(!empty($request->location)){
            $where['location'] = $request->location;
            $location           = $request->location;
            $districtVal          = $this->District->Where(['id'=>$location])->first()->toArray();
            $locationName       = $districtVal['name'];
        }
        $userId     = Auth::user();

         $locations = $this->listing->getCities();

         $locationList=[];
         if(!empty($locations)){
            foreach ($locations as $key => $value) {
                $locationList['packers-and-movers-in-'.strtolower($value)]=$value;
            }
         }
         
        $districts  = $this->District->Where(['country_id'=>105])->pluck('name','id')->toArray();
        $users      = $this->user->where(['is_admin'=>0,'type'=>2,'is_top'=>1,'status'=>1])->get()->toArray();
        $packers    = $this->user->where(['is_admin'=>0,'type'=>2,'status'=>1])->where($where)->get()->toArray();
        return view('home.home',compact('users','districts','packers','location','locationName','locationList'));
    }

   
    public function category($slug)
    {
        $slugArr  = [];
        $slugArr  = explode('-',$slug);
        $index    = count($slugArr);
        $name     = $slugArr[$index-1];
$locations = $this->listing->getCities();


         $locationList=[];
         if(!empty($locations)){
            foreach ($locations as $key => $value) {
                $locationList['packers-and-movers-in-'.strtolower($value)]=$value;
            }
         }
        $listings = $this->listing->select(['name'=>'users.name','users.*'])->join('districts','users.location', '=', 'districts.id')->where(['districts.name'=>$name,'users.status'=>1,'users.type'=>2])->orderBy('users.total_purchase_leads', 'desc')->paginate(10);
        return view('home.listing',compact('listings','name','locationList'));
    }

    public function location(){

        $locations = $this->listing->getCities();


         $locationList=[];
         if(!empty($locations)){
            foreach ($locations as $key => $value) {
                $locationList['packers-and-movers-in-'.strtolower($value)]=$value;
            }
         }
        return view('home.location',compact('locations','locationList'));
    }
    public function single($slug)
    {

         $listing = $this->listing->select(['name'=>'users.name','users.*'])
         ->join('districts','users.location', '=', 'districts.id')
         ->where('users.slug',$slug)->first();

         $locations = $this->listing->getCities();


         $locationList=[];
         if(!empty($locations)){
            foreach ($locations as $key => $value) {
                $locationList['packers-and-movers-in-'.strtolower($value)]=$value;
            }
         }
         // echo "<pre>";
         // print_r($listing);
         // exit;
        return view('home.single',compact('listing','locationList'));
    }

    public function aboutUs()
    {
        return view('home.aboutus');
    }

    public function verifyenquiry(Request $request){
        return view('auth.verifyenquiry');
    }

    public function verifyEnqiryForm(Request $request){
        // Validate the form data
        $this->validate($request, [
            'email'   => 'required|email',
            'token' => 'required'
        ]);
      
        $chkStatus = $this->Enquiry->where(['contact_email'=>$request->email,'token'=>$request->token,'status'=>3])->first();
        if(!empty($chkStatus)){
            $this->Enquiry->where(['contact_email'=>$request->email,'token'=>$request->token,'status'=>3])->update(['status'=>2,'token'=>""]);
            return redirect()->route('enquiry')->with('success','Enquiry Form has been verified and sent for approval.');
        }
        return redirect()->back()->withInput($request->only('email'));
    }

    public function enquiry(Request $request){
        if($request->method()=='POST'){
            $data                           = $request->all();
            $data['status']                 = 3;
            $data['max_purchase_count']     = 3;
            $data['token']                  = Str::random(4);
            if(!empty($data['quantity'])){
                $data['quantity']           = json_encode($data['quantity']);
            }
            Enquiry::create($data);
            $template                   = "mail.verifyEnquiry";
            $data['subject']            = " Verification Enquiry Form ";
            $data['to']                 = $data['contact_email'];
            USER::sendMail($data,$template);
            return redirect()->route('verifyenquiry')->with('success','Token has been sent to your email Address Successfully.');
        }
        $categorylist = $this->Enquiry->getCategories();
         $locations = $this->listing->getCities();


         $locationList=[];
         if(!empty($locations)){
            foreach ($locations as $key => $value) {
                $locationList['packers-and-movers-in-'.strtolower($value)]=$value;
            }
         }
        return view('home.enquiry',compact('categorylist','locations','locationList'));
    }
   
    
    
    public function contactUs()
    {
        return view('home.contactus');
    }

    public function profile()
    {
        return view('home.profile');
    }

    public function package()
    {
        return view('home.package');
    }

    public function storeSingle()
    {
        return view('home.storesingle');
    }

    public function blog()
    {
        return view('home.blog');
    }

    public function blogDetails()
    {
        return view('home.blogdetails');
    }

    public function termsConditions()
    {
        return view('home.termsconditions');
    }

    public function error()
    {
        return view('home.error');
    }

    public function categoryList()
    {
        return view('home.categorylist');
    }

    public function createAd()
    {
        return view('home.createad');
    }
    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
