<?php

namespace App\Http\Controllers;
use Config;
use App\User;
use App\District;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function __construct(){
        // $this->middleware('auth');
        $this->middleware('auth:admin');
        $this->user                    = new User;
        $this->district                = new District();
        $this->status                  = Config::get('common.enquiry_status');
        $this->userType                = Config::get('common.user_type');
        $this->isAdmin                 = Config::get('common.is_admin');
        $this->statusColor             = Config::get('common.enquiry_status_color');
        $this->rating                  = Config::get('common.rating');
    }

    public function profileRequest(){
        $users   = $this->user->where(['is_admin'=>0])->where([['status','=',1],['profile_request_status','=',1]])->latest()->paginate(10);
        return view('users.profile_request',compact('users'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    public function viewProfileRequest($id){
        $jsonUser       = [];
        $jsondisName    = "";
        $user           = $this->user->where(['id'=>$id])->first();
        if(!empty($user->profile_request)){
            $jsonUser = json_decode($user->profile_request,true);
            if(!empty($jsonUser['location'])){
                $getJsonDistrict    = $this->district->Where(['id'=>$jsonUser['location']])->first()->toArray();
                $jsondisName        = $getJsonDistrict['name'];
            }

        }else{
            return redirect()->back()->with('error','Not able to find profile requested data !!');
        }
        $statusColor    = $this->statusColor;
        $disName        = "";
        $status         = $this->status;
        $userType       = $this->userType;
        $isAdmin        = $this->isAdmin;
        if(!empty($user->location)){
            $getDistrict    = $this->district->Where(['id'=>$user->location])->first()->toArray();
            $disName        = $getDistrict['name'];
        }
        return view('users.view_profile_request',compact('user','status','userType','isAdmin','disName','statusColor','jsonUser','jsondisName'));
    }

    public function approveProfileRequest($id){
        $user                                     = $this->user->where(['id'=>$id])->first();
        if(!empty($user->profile_request)){
            $data                                 = [];
            $data['profile_request_status']       = 0;
            $jsonUser                             = json_decode($user->profile_request,true);
            foreach ($jsonUser as $key => $value) {
                if($key == "id"){
                    continue;
                }else{
                    $data[$key] =  $value;
                }
            }
            if(!empty($data)){
                if($this->user->where('id', $id)->update($data)){
                    return redirect()->route('profileRequest')
                    ->with('success','Profile request has been approved successfully !!');
                }
            }
        }else{
            return redirect()->back()->with('error','Not able to find profile requested data !!');
        }
        return redirect()->back()->with('error','Please try again !!');
    }

    public function rejectProfileRequest($id){
        if($this->user->where('id', $id)->update(['profile_request_status'=>2])){
        return redirect()->route('profileRequest')
            ->with('success','Profile request has been rejected successfully !!');
        }else{
            return redirect()->back()->with('error','Something Went wrong !!');
        }
        return redirect()->back()->with('error','Please try again !!');
    }

    public function registeredUser(){
        $users   = $this->user->where(['is_admin'=>0,'type'=>1])->latest()->paginate(10);
        return view('users.registereduser',compact('users'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    public function registeredPacker(){
        $districts = $this->district->Where(['country_id'=>105])->pluck('name','id')->toArray();
        $users   = $this->user->where(['is_admin'=>0,'type'=>2])->where('status','!=',4)->latest()->paginate(10);
        return view('users.registeredpacker',compact('users','districts'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }
    
    public function buyLead(Request $request){
        $data = $request->all();
        print_r($data);
        exit;

    }

    public function changeRegisteredStatus(Request $request){
        $return = [];
        if(!empty($request->id)){
            $chkStatus = $this->user->where('id',$request->id)->first();
            if($chkStatus['status'] == 0){
                $data['status'] = 1;
                $return = ['status'=>'Active','color'=>'success'];
            }else{
                $data['status'] = 0;
                $return = ['status'=>'In-Active','color'=>'danger'];
            }
            $this->user->where('id',$request->id)->update($data);
        }
        return $return;
    }

    public function changeTopPacker(Request $request){
        $return = [];
        if(!empty($request->id)){
            $chkStatus = $this->user->where('id',$request->id)->first();
            if($chkStatus['is_top'] == 0){
                $data['is_top'] = 1;
                $return = ['status'=>'Active','color'=>'success'];
            }else{
                $data['is_top'] = 0;
                $return = ['status'=>'In-Active','color'=>'danger'];
            }
            $this->user->where('id',$request->id)->update($data);
        }
        return $return;
    }

    public function changeRegisteredpackerStatus(Request $request){
        $return = [];
        if(!empty($request->id)){
            $chkStatus = $this->user->where('id',$request->id)->first();
            if($chkStatus['status'] == 3){
                $data['status'] = 1;
                $return = ['status'=>'Active','color'=>'success'];
            }else{
                $data['status'] = 3;
                $return = ['status'=>'Registered','color'=>'danger'];
            }
            $this->user->where('id',$request->id)->update($data);
            if($data['status'] == 1){
                $data                       = $this->user->where('id',$request->id)->first()->toArray();
                $template                   = "mail.approvedPacker";
                $data['subject']            = " Registration Request Approved ";
                $data['to']                 = $data['email'];
                USER::sendMail($data,$template);
            }
        }
        return $return;
    }

    public function index()
    {
        $userID     = Auth::user();
        $users      = $this->user->where('id','!=',$userID['id'])->latest()->paginate(20);
        return view('users.index',compact('users'))
            ->with('i', (request()->input('page', 1) - 1) * 20);
    }

    public function create()
    {
        $status         = $this->status;
        $userType       = $this->userType;
        $isAdmin        = $this->isAdmin;
        $rating         = $this->rating;
        return view('users.create',compact('status','userType','isAdmin','rating'));
    }

    public function store(Request $request)
    {
        $this->userValidate($request);
        $data                       = $request->all();
        $data['password']           = Hash::make($data['password']);
        if(!empty($data['company_logo'])){
            $data['company_logo']   = $this->user->uploadCompanyLogo($data['company_logo'],'user');
        }
        $this->user->create($data);
        return redirect()->route('users.index')
                        ->with('success','User updated successfully');
    }

    public function show(User $user)
    {
        $statusColor    = $this->statusColor;
        $disName        = "";
        $status         = $this->status;
        $userType       = $this->userType;
        $isAdmin        = $this->isAdmin;
        if(!empty($user->location)){
            $getDistrict    = $this->district->Where(['id'=>$user->location])->first()->toArray();
            $disName        = $getDistrict['name'];
        }
        return view('users.show',compact('user','status','userType','isAdmin','disName','statusColor'));
    }

    public function edit(User $user)
    {
        $disName        = "";
        $status         = $this->status;
        $userType       = $this->userType;
        $isAdmin        = $this->isAdmin;
        $rating         = $this->rating;
        if(!empty($user->location)){
            $getDistrict    = $this->district->Where(['id'=>$user->location])->first()->toArray();
            $disName        = $getDistrict['name'];
        }
        return view('users.edit',compact('user','status','userType','isAdmin','disName','rating'));
    }

    public function update(Request $request, User $user)
    {
        // $this->userValidate($request,$user->id);
        $data               = $request->all();
        if(!empty($data['company_logo'])){
            $data['company_logo']  = $this->user->uploadCompanyLogo($data['company_logo'],'user');
        }
        $user->update($data);
        return redirect()->route('users.index')
                        ->with('success','User updated successfully');
    }

    public function destroy($id)
    {
        //
    }

    private function userValidate($request,$id=null){
        $validate['email']              = 'required|unique:users,email,NULL,id';
        $validate['slug']               = 'required|unique:users,slug,NULL,id';
        $validate['username']           = 'unique:users,username,NULL,id';
        $validate['status']             = 'required';
        // if(!empty($id)){
        //     $validate['code']           = 'required|unique:listing,code,'.$id.',id';
        // }else{
        //     $validate['code']           = 'required|unique:listing,code,NULL,id';
        // }

        $messages = [
           'status.required'             => __('Please Select Status'),
           'email.required'              => __('Please Enter Email Id'),
           'email.unique'                => __('Email Id already registered !'),
           'username.unique'             => __('Username already exists, Please try another username'),
           'slug.unique'                 => __('Slug already exists, Please try another slug')
        ];
        $request->validate($validate,$messages);  
    }
}
