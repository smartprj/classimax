<?php

namespace App\Http\Controllers;
use Config;
use App\User;
use App\Enquiry;
use App\Category;
use Illuminate\Http\Request;

class EnquiryController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
        $this->user                    = new User;
        $this->enquiry                 = new Enquiry;
        $this->category                = new Category();
        $this->status                  = Config::get('common.enquiry_status');
        $this->statusColor             = Config::get('common.enquiry_status_color');
    }

    public function index(){
        $status         = $this->status;
        $statusColor    = $this->statusColor;
        $enquiries      = $this->enquiry->latest()->paginate(15);
        return view('enquiries.index',compact('enquiries','status','statusColor'))->with('i', (request()->input('page', 1) - 1) * 15);
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        
    }

    public function show(Enquiry $enquiry){
        // $categorylist    = $this->enquiry->getCategories();
        $quantityArr        = [];
        if(!empty($enquiry->quantity)){
            $quantityArr    = json_decode($enquiry->quantity,true);
        }
        $categoriesArr  = $this->category->where(['status'=>1])->pluck('name','id')->toArray();
        $status         = $this->status;
        $statusColor    = $this->statusColor;
        return view('enquiries.show',compact('enquiry','status','statusColor','quantityArr','categoriesArr'));
    }

    public function edit(Enquiry $enquiry){
        $quantityArr        = [];
        if(!empty($enquiry->quantity)){
            $quantityArr    = json_decode($enquiry->quantity,true);
        }
        $categoriesArr  = $this->category->where(['status'=>1])->pluck('name','id')->toArray();
        $status         = $this->status;
        return view('enquiries.edit',compact('enquiry','status','quantityArr','categoriesArr'));
    }

    public function update(Request $request, Enquiry $enquiry)
    {
        $senMailToPacker = "No";
        $location = $enquiry['source_locality']??"";
        $this->listingValidate($request,$enquiry->id);
        $data               = $request->all();
        if($data['status'] == 1 && ($data['status'] != $enquiry['status'])){
            $senMailToPacker = "Yes";
        }
        $enquiry->update($data);
        if($senMailToPacker == "Yes"){
            $getUsers = $this->user->where(['location'=>$location,'type'=>2,'is_admin'=>0,'status'=>1])->get();
            if(!empty($getUsers)){
                foreach ($getUsers as $key => $user) {
                    $data                       = [];
                    $data                       = $user;
                    $template                   = "mail.enquiryDetailsToPackers";
                    $data['subject']            = " Enquiry request near your location ";
                    $data['to']                 = $data['email'];
                    USER::sendMail($data,$template);
                    unset($data);
                }
            }
        }
        return redirect()->route('enquiries.index')->with('success','Enquiry updated successfully');
    }

    public function destroy($id)
    {
        
    }

    private function listingValidate($request,$id=null){
        $validate['max_purchase_count']               = 'required';
        $validate['status']                           = 'required';
        $messages = [
           'max_purchase_count.required'              => __('Please Enter Max Purchase Count'),
           'status.required'                          => __('Please Select Status'),
        ];
        $request->validate($validate,$messages);  
    }
}
