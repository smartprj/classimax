<?php

namespace App\Http\Controllers;
use Config;
use App\Plan;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class PlanController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
        $this->plan                 = new Plan;
        $this->validityType         = Config::get('common.validity_type');
        $this->validityTypeDropDown = Config::get('common.validity_type_dropdown');
    }

    public function index(){
        $plans   = $this->plan->latest()->paginate(10);
        return view('plans.index',compact('plans'))->with('i', (request()->input('page', 1) - 1) * 10);
    }

    public function create(){
        $validityTypeDropDown = $this->validityTypeDropDown;
        return view('plans.create',compact('validityTypeDropDown'));
    }

    public function store(Request $request){
        $data           = $request->all();
        $this->plan->create($data);
        return redirect()->route('plans.index')->with('success','Plan created successfully.');
    }

    public function show(Plan $plan){
        return view('plans.show',compact('plan'));
    }

    public function edit(Plan $plan){
        return view('plans.edit',compact('plan'));
    }

    public function update(Request $request, Plan $plan){
        $data           = $request->all();
        $plan->update($data);
        return redirect()->route('plans.index')->with('success','Plan updated successfully');
    }

    public function destroy(Plan $plan){
        $plan->delete();
        return redirect()->route('plans.index')->with('success','Plan deleted successfully');
    }
}
