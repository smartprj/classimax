<?php

namespace App\Http\Middleware;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Route;

class Authenticate extends Middleware
{
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
        	if (Route::is('dashboard') || Route::is('leadbuy') || Route::is('myLeads') || Route::is('wallet')){
                return route('weblogin');
            }
            return route('login');
        }
    }
}
