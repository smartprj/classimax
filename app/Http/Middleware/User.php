<?php

namespace App\Http\Middleware;

use Closure;

class User
{
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
}
