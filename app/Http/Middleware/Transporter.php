<?php

namespace App\Http\Middleware;

use Closure;

class Transporter
{
    public function handle($request, Closure $next)
    {
        if(auth()->user()->is_admin == 1){
            return $next($request);
        }
        return redirect(‘home’)->with(‘error’,"You don't have admin access.");
    }
}
