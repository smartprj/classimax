<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Mail;

class User extends Authenticatable
{
    protected $guard = 'admin';
    use Notifiable;

    protected $fillable = [
        'name','username', 'email', 'password','is_admin','type','status','phone_no','gst_no','location','company_logo','wallet','total_purchase_leads','token','last_wallet','slug','rating','address','pin_code','profile_request_status','is_top','description','web_url'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function uploadCompanyLogo($image,$name) {
        $imageName = $name.time().'.'.$image->extension();
        $image->move(public_path('images/company_logo/'), $imageName);
        $imageName = filter_var($imageName,FILTER_SANITIZE_STRING);
        return $imageName;
    }

    public static function sendMail($data,$template){
        Mail::send(
          ['html' => $template],
          ['data' => $data],
          function($message) use ($data) {
              $message->to($data['to'],config('app.name'));
              if(!empty($data['subject'])){
                $message->subject($data['subject']);
              }
              $message->from('info@moversnest.com',config('app.name'));
          }
       );
    }

    public function callUserLocation(){
        return $this->belongsTo('App\District','location','id');
    }
}
