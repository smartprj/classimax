<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table        = 'states';
    protected $primaryKey   = 'id';
    protected $fillable     = [
        'name','country_id', 'status', 'added_by', 'created_at', 'updated_at', 'updated_by'
    ];

    public static function scopeSearch($query,$where=[]){
        return $query->where($where);
    }
}
