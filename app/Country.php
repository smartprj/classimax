<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table        = 'countries';
    protected $primaryKey   = 'id';
    protected $fillable     = [
        'name','code', 'isd_code','pr_resident','currecy_code','currency_description','status', 'added_by', 'created_at', 'updated_at', 'updated_by'
    ];

    public static function scopeSearch($query,$where=[]){
        return $query->where($where);
    }
}
