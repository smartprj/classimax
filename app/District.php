<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table        = 'districts';
    protected $primaryKey   = 'id';
    protected $fillable     = [
        'name','country_id','state_id', 'status', 'added_by', 'created_at', 'updated_at', 'updated_by'
    ];

    public static function scopeSearch($query,$where=[]){
        return $query->where($where);
    }

    public static function getLocationName($id){
    	$locationName = "";
        $query =  District::where('id',$id)->first();
        if(!empty($query['name'])){
        	$locationName = $query['name'];
        }
        return $locationName;
    }
}
