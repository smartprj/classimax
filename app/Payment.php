<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table        = 'payment_history';
    protected $primaryKey   = 'id';
    protected $fillable     = [
       'order_id ','amount','contact_email','status ','emp_id','razorpay_payment_id','razorpay_signature','payment_coins','user_last_wallet','response'
    ];


}
