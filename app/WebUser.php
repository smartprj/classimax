<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class WebUser extends Authenticatable
{
    protected $guard = 'web';
    use Notifiable;
    protected $table        = 'users';
    protected $primaryKey   = 'id';
    protected $fillable = [
        'name','username', 'email', 'password','is_admin','type','status','phone_no','gst_no','location','company_logo','wallet','total_purchase_leads','token','last_wallet','slug','rating','address','pin_code','profile_request_status','is_top','description','web_url'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getCities(){

        return User::join('districts', 'users.location', '=', 'districts.id')->distinct()->pluck('districts.name','districts.id')->toArray();
    }

    public function callWebUserLocation(){
        return $this->belongsTo('App\District','location','id');
    }
}
