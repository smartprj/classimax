<?php

namespace App;
use App\Category;
use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
    protected $table        = 'enquiries';
    protected $primaryKey   = 'id';
    protected $fillable     = [
       'contact_name','contact_number','contact_email','source_address','source_locality','source_pin','source_house_type','source_floor','source_lift','destination_check','destination_address','destination_location','destination_pin','destination_house_type','destination_lift','quantity','status','max_purchase_count','amount','available_count','token'
    ];
    const CREATED_AT = 'created';
	const UPDATED_AT = 'modified';




	public function callSourceLocation(){
        return $this->belongsTo('App\District','source_locality','id');
    }

    public function callDestinationLocation(){
        return $this->belongsTo('App\District','destination_location','id');
    }

    public static function getCategories($parentid=''){
        $arr=[];
        $subArr=[];
        if(empty($parentid)){
            $catListing = Category::WhereNull('parent_id')->pluck('name','id')->toArray();
        }else{
            $catListing = Category::Where('parent_id',$parentid)->pluck('name','id')->toArray();
        }
        foreach ($catListing as $key => $value) {
            $arr[$key]['parent']=$value;
            $arr[$key]['menu_id']=$key;
            $subArr = Enquiry::getCategories($key);   
            if(!empty($subArr)){
                $arr[$key]['child']=[];
                $arr[$key]['child']=$subArr;
            }
        }
        return $arr;
    }

}
