<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class RaiseTicket extends Model
{	
    protected $table        = 'raise_ticket';
    protected $primaryKey   = 'id';
    protected $fillable     = [
        'user_id','lead_id','type','status','remark','issue'
    ];

}
