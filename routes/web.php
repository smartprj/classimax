<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home.home');
});

  Route::post('register', 'Auth\RegisterController@register')->name('register');
Route::group(['prefix' => 'admin'],function () {
	Auth::routes();
  Route::post('login', 'Auth\LoginController@login')->name('login');

  Route::resource('listings','ListingController')->middleware('admin');
  Route::resource('plans','PlanController')->middleware('admin');
  Route::resource('enquiries','EnquiryController')->middleware('admin');
   Route::resource('issue','IssuesController')->middleware('admin');
  Route::resource('categories','CategoryController')->middleware('admin');
  Route::get('getlocation','ListingController@getlocation')->name('getlocation')->middleware('admin');
  Route::any('/','AdminController@index')->name('admin')->middleware('admin');
  Route::any('changelistingstatus','ProductController@changelistingstatus')->name('changelistingstatus')->middleware('admin');
	Route::any('changebrandingstatus','ProductController@changebrandingstatus')->name('changebrandingstatus')->middleware('admin');
	Route::any('categorylistingstatus','CategoryController@categorylistingstatus')->name('categorylistingstatus')->middleware('admin');
	Route::resource('users','UserController')->middleware('admin');
	Route::any('registered-user','UserController@registeredUser')->name('registeredUser')->middleware('admin');

  Route::any('profile-request','UserController@profileRequest')->name('profileRequest')->middleware('admin');
  Route::any('view-profile-request/{id?}','UserController@viewProfileRequest')->name('viewProfileRequest')->middleware('admin');
  Route::any('approve-profile-request/{id?}','UserController@approveProfileRequest')->name('approveProfileRequest')->middleware('admin');
  Route::any('reject-profile-request/{id?}','UserController@rejectProfileRequest')->name('rejectProfileRequest')->middleware('admin');

  Route::any('registered-packer','UserController@registeredPacker')->name('registeredPacker')->middleware('admin');
  Route::any('changeRegisteredStatus','UserController@changeRegisteredStatus')->name('changeRegisteredStatus')->middleware('admin');
  Route::any('changeTopPacker','UserController@changeTopPacker')->name('changeTopPacker')->middleware('admin');
  Route::any('changeRegisteredpackerStatus','UserController@changeRegisteredpackerStatus')->name('changeRegisteredpackerStatus')->middleware('admin');
});

// Web Auth Routes Start
Route::post('/web-logins-submit', 'Auth\WebLoginController@webauthlogin')->name('web.logins.submit');
Route::any('web-logout-submit/', 'Auth\WebLoginController@logout')->name('web.logout');
Route::any('web-login','Auth\WebLoginController@weblogin')->name('weblogin');
Route::any('web-register','Auth\WebLoginController@webRegister')->name('webRegister');
Route::any('packer-register','Auth\WebLoginController@packerRegister')->name('packerRegister');
Route::any('verify-packer','Auth\WebLoginController@verifyPacker')->name('verifyPacker');
Route::any('verify-packer-email','Auth\WebLoginController@verifyPackerEmail')->name('verifyPackerEmail');
// Web Auth Routes END

Route::resource('home','HomeController');
// Route::any('home','HomeController@index')->name('home');
Route::any('dashboard','PackersController@dashboard')->name('dashboard');
Route::any('myPurchase','PackersController@myPurchase')->name('myPurchase');
Route::any('leadbuy','PackersController@buyLead')->name('leadbuy');
Route::any('myLeads','PackersController@myLeads')->name('myLeads');
Route::any('myProfile','PackersController@myProfile')->name('myProfile');
Route::any('editMyProfile','PackersController@editMyProfile')->name('editMyProfile');
Route::any('detail-myleads/{slug?}','PackersController@myLeadsDetail')->name('myLeadsDetail');
Route::any('wallet','PackersController@userWallet')->name('wallet');
Route::any('raiseIssue/{lead_id?}','PackersController@raiseIssue')->name('raiseIssue');
Route::any('saveProfileRequest','PackersController@saveProfileRequest')->name('saveProfileRequest');


Route::any('location','HomeController@location')->name('location');
Route::any('testmail','HomeController@testmail')->name('testmail');
Route::any('paymentProceed','PaymentController@create')->name('paymentProceed');
Route::any('payment','PaymentController@payment')->name('payment');
Route::any('generateOrder','PaymentController@generateOrderID')->name('generateOrder');


Route::any('profile','HomeController@profile')->name('profile');
//Route::any('single','HomeController@single')->name('single');
Route::any('about-us','HomeController@aboutUs')->name('aboutus');
Route::any('contact-us','HomeController@contactUs')->name('contactus');
Route::any('package','HomeController@package')->name('package');
Route::any('store','HomeController@storeSingle')->name('storesingle');
Route::any('blog','HomeController@blog')->name('blog');
Route::any('blog-details','HomeController@blogDetails')->name('blogdetails');
Route::any('terms-conditions','HomeController@termsConditions')->name('termsconditions');
Route::any('error','HomeController@error')->name('error');
Route::any('category-list','HomeController@categoryList')->name('categorylist');

Route::any('create-ad','HomeController@createAd')->name('createad');
Route::any('enquiry','HomeController@enquiry')->name('enquiry');
Route::any('verifyenquiry','HomeController@verifyenquiry')->name('verifyenquiry');
Route::any('verifyEnqiryForm','HomeController@verifyEnqiryForm')->name('verifyEnqiryForm');

Route::any('/','HomeController@index')->name('home');
Route::any('/detail/{slug?}','HomeController@single')->name('detail');
Route::any('/getLocations','HomeController@getLocations')->name('getLocations');
Route::any('/{slug?}','HomeController@category')->name('category');