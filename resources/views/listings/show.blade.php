@extends('layouts.admin')
@section('content')

    <div class="content-wrapper">
        <div class="page-header">
          <h3 class="page-title"> View Product </h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Product</li>
              <li class="breadcrumb-item active" aria-current="page">View Product</li>
            </ol>
          </nav>
        </div>

        <div class="row">
          <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                  
                  <div class="form-group">
                    <table class="table table-hover">
                        <tr>
                          <th>Name</th>
                          <td>{{ $listing->name }}</td>
                        </tr>
                        <tr>
                          <th>Code</th>
                          <td>{{ $listing->code }}</td>
                        </tr>
                        <tr>
                          <th>State</th>
                          <td>{{ $states[$listing->state]??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Location</th>
                          <td>{{ $districts[$listing->location]??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Address</th>
                          <td>{{ $listing->address }}</td>
                        </tr>
                        <tr>
                          <th>Mobile Number</th>
                          <td>{{ $listing->mobile_no }}</td>
                        </tr>
                        <tr>
                          <th>Alternate Contact Number</th>
                          <td>{{ $listing->alternate_no??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Description</th>
                          <td>{{ $listing->description }}</td>
                        </tr>
                        <tr>
                          <th>Slug</th>
                          <td>{{ $listing->slug }}</td>
                        </tr>
                        <tr>
                          <th>Meta Title</th>
                          <td>{{ $listing->meta_title??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Meta Description</th>
                          <td>{{ $listing->meta_description??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Status</th>
                          <td>
                             @if($listing->status == 1)
                              <label class="badge badge-success">Active</label>
                            @else
                                <label class="badge badge-danger">In-Active</label>
                            @endif
                          </td>
                        </tr>
                        <tr>
                          <th colspan="2"><a href="{{route('listings.index')}}" class="btn btn-primary">Back to Listing</a></th>
                        </tr>
                    </table>
                  </div>

                </div>
            </div>
          </div>

        </div>
    </div>

@endsection