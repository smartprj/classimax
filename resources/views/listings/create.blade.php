@extends('layouts.admin')
@section('content')

    <div class="content-wrapper">
        <div class="page-header">
          <h3 class="page-title"> Add Listing </h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Listing</li>
              <li class="breadcrumb-item active" aria-current="page">Add Listing</li>
            </ol>
          </nav>
        </div>

        @if ($errors->any())
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
          <div class="col-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    
                    <form action="{{ route('listings.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                      <div class="form-group">
                        <label for="exampleInputName1"><span style="color: red;">*</span> Name </label>
                        <input type="text" class="form-control" id="exampleInputName1" required="required" placeholder="Name" name="name">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputName1"> Email </label>
                        <input type="email" class="form-control" id="exampleInputName1"  placeholder="Email" name="email">
                      </div>

                      <!-- <div class="form-group">
                        <label for="exampleInputName1"><span style="color: red;">*</span> Code </label>
                        <input type="text" class="form-control" id="exampleInputName1" placeholder="Code" name="code">
                      </div> -->

                      <div class="form-group">
                        <label for="exampleSelectcategory"><span style="color: red;">*</span> State</label>
                        <select class="form-control" id="exampleSelectcategory" name="state" required="required" onchange="getLocation(this.value)">
                            <option value="">Select State</option>
                            @foreach($states as $key=>$val)
                                <option value="{{$key}}">{{$val}}</option>
                            @endforeach
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleSelectcategory"><span style="color: red;">*</span> Location</label>
                        <div class="location-input">
                          <select class="form-control" id="exampleSelectcategory" name="location" required="required">
                              <option value="">Select Location</option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="exampleTextarea1"><span style="color: red;">*</span> Address</label>
                        <textarea class="form-control" id="exampleTextarea1" rows="4" name="address"></textarea>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputName1"><span style="color: red;">*</span> Mobile Number </label>
                        <input type="number" class="form-control" id="exampleInputName1" required="required" placeholder="Enter Mobile Number" name="mobile_no">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputName1"> Alternate Contact Number </label>
                        <input type="number" class="form-control" id="exampleInputName1" placeholder="Enter Alternate Contact Number" name="alternate_no">
                      </div>

                      <div class="form-group">
                        <label> Upload Image</label>
                        <input type="file" name="listing_image" accept="image/*" class="file-upload-default">
                        <div class="input-group col-xs-12">
                          <input type="text" name="file" accept="image/*" class="form-control file-upload-info" disabled placeholder="Upload Image">
                          <span class="input-group-append">
                            <button name="sasa" class="file-upload-browse btn btn-gradient-primary" type="button">Upload</button>
                          </span>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="exampleTextarea1"> Description</label>
                        <textarea class="form-control" id="exampleTextarea1" rows="4" name="description"></textarea>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputName1"> Slug </label>
                        <input type="text" class="form-control" id="exampleInputName1"  placeholder="Slug" name="slug">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputName1">Meta Title </label>
                        <input type="text" class="form-control" id="exampleInputName1" placeholder="Meta Title" name="meta_title">
                      </div>

                      <div class="form-group">
                        <label for="exampleTextarea1">Meta Description</label>
                        <textarea class="form-control" id="exampleTextarea1" rows="4" name="meta_description"></textarea>
                      </div>
                      <div class="form-group">
                        <label for="number">Position </label>
                        <input type="number" class="form-control" id="number" placeholder="Position" name="meta_title">
                      </div>
                        <div class="form-group">
                        <label for="rating"> Rating </label>
                        <select class="form-control" name="rating">

                          @for ($i=5; $i>0 ;$i--)
                          <option value="{{ $i }}">{{ $i }}</option>
                          @endfor
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleSelectstatus"><span style="color: red;">*</span> Status </label>
                        <select class="form-control" id="exampleSelectstatus" required="required" name="status">
                          <option value="1">Active</option>
                          <option value="0">Inactive</option>
                        </select>
                      </div>
                      
                      <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                      <a href="{{route('listings.index')}}" class="btn btn-light">Cancel</a>
                    </form>
                </div>
            </div>
          </div>

        </div>
    </div>

    <script>
      function getLocation(id){
          $.ajax({
              url: '{{route("getlocation")}}',
              data: { id: id},
              type: "GET",
              success: function (data) {
                  if(data != ""){
                      $('.location-input').html(data);
                  }
              }
         });
      }
  </script> 

@endsection