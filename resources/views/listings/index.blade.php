@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
          <h3 class="page-title">Listing</h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Listing</li>
            </ol>
            
          </nav>
        </div>

        @if ($message = Session::get('success'))
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
          <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <p class="card-description"> <a class="btn btn-success btn-sm float-sm-right" href="{{ route('listings.create') }}"> Add Listing</a></p>
                
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Image</th>
                      <th>Name</th>
                      <th>Code</th>
                      <th>State</th>
                      <th>Location</th>
                      <th>Mobile Number</th>
                      <th>Status</th>
                      <th width="280px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($listings as $listing)
                    <tr>
                      <td>{{ ++$i }}</td>
                      <td class="py-1">
                        @if(!empty($listing->image))
                          <img src="{{ asset('images/listing/'.$listing->image) }}" alt="image" />
                        @else
                          -
                        @endif
                      </td>
                      <td>{{ $listing->name }}</td>
                      <td>{{ $listing->code }}</td>
                      <td>{{ $states[$listing->state]??"" }}</td>
                      <td>{{ $districts[$listing->location]??"" }}</td>
                      <td>{{ $listing->mobile_no }}</td>
                      <td>
                        @if($listing->status == 1)
                            <label class="badge badge-success">Active</label>
                        @else
                            <label class="badge badge-danger">In-Active</label>
                        @endif
                      </td>
                      <td>
                        <form action="{{ route('listings.destroy',$listing->id) }}" method="POST">
                            <a class="btn btn-primary btn-xs" href="{{ route('listings.edit',$listing->id) }}">Edit</a>
                            <a class="btn btn-info btn-xs" href="{{ route('listings.show',$listing->id) }}">View</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-xs">Delete</button>
                        </form>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {!! $listings->links() !!}
              </div>
            </div>
          </div>
        </div>
    </div>      
@endsection