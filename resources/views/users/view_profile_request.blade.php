@extends('layouts.admin')
@section('content')

    <div class="content-wrapper">
        <div class="page-header">
          <h3 class="page-title"> View User </h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">User</li>
              <li class="breadcrumb-item active" aria-current="page">View User</li>
            </ol>
          </nav>
        </div>

        <div class="row">
          <!-- Old Data -->
          <div class="col-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                  <h3>Old Details</h3>
                  
                  <div class="form-group">
                    <table class="table table-hover">
                        <tr>
                          <th>Name</th>
                          <td>{{ $user->name }}</td>
                        </tr>
                        <tr>
                          <th>Email</th>
                          <td>{{ $user->email }}</td>
                        </tr>
                        <tr>
                          <th>Location</th>
                          <td>{{ $disName??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Mobile Number</th>
                          <td>{{ $user->phone_no??"-" }}</td>
                        </tr>
                        <tr>
                          <th>GST Number</th>
                          <td>{{ $user->gst_no??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Address</th>
                          <td>{{ $user->address??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Pin Code</th>
                          <td>{{ $user->pin_code??"-" }}</td>
                        </tr>
                        <tr>
                          <th colspan="2"><a href="{{route('profileRequest')}}" class="btn btn-primary">Back to Listing</a></th>
                        </tr>
                    </table>
                  </div>

                </div>
            </div>
          </div>

          <!-- new data -->
          <div class="col-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                  
                  <h3>New Details</h3>
                  <div class="form-group">
                    <table class="table table-hover">
                        <tr>
                          <th>Name</th>
                          <td>{{ $jsonUser['name'] }}</td>
                        </tr>
                        <tr>
                          <th>Email</th>
                          <td>{{ $user->email }}</td>
                        </tr>
                        <tr>
                          <th>Location</th>
                          <td>{{ $jsondisName??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Mobile Number</th>
                          <td>{{ $jsonUser['phone_no']??"-" }}</td>
                        </tr>
                        <tr>
                          <th>GST Number</th>
                          <td>{{ $jsonUser['gst_no']??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Address</th>
                          <td>{{ $jsonUser['address']??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Pin Code</th>
                          <td>{{ $jsonUser['pin_code']??"-" }}</td>
                        </tr>
                        <tr>
                          <th ><a href="{{route('approveProfileRequest',$user->id)}}" class="btn btn-success">Approve</a></th>
                          <th ><a href="{{route('rejectProfileRequest',$user->id)}}" class="btn btn-danger">Reject</a></th>
                        </tr>
                    </table>
                  </div>

                </div>
            </div>
          </div>
        </div>
    </div>

@endsection