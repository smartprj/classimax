@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
          <h3 class="page-title">Users</h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Users</li>
            </ol>
          </nav>
        </div>

        @if ($message = Session::get('success'))
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
          <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <p class="card-description"> <a class="btn btn-success btn-sm float-sm-right" href="{{ route('users.create') }}"> Add User</a></p>
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Company Logo</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Type</th>
                      <th>Status</th>
                      <th>Change Status</th>
                      <th>Top Packers</th>
                      <th width="280px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($users as $user)
                    <tr>
                      <td>{{ ++$i }}</td>
                      <td class="py-1">
                        @if(!empty($user->company_logo))
                          <img src="{{ asset('images/company_logo/'.$user->company_logo) }}" alt="image" />
                        @else
                          -
                        @endif
                      </td>
                      <td>{{ $user->name }}</td>
                      <td>{{ $user->email }}</td>
                      <td>
                        @if($user->is_admin == 1)
                            <label class="badge badge-success">Admin</label>
                        @elseif($user->type == 1 && $user->is_admin == 0)
                            <label class="badge badge-primary">User</label>
                        @elseif($user->type == 2 && $user->is_admin == 0)
                            <label class="badge badge-info">Packer</label>
                        @endif
                      </td>
                      <td id="status_{{$user->id}}">
                        @if($user->status == 1)
                            <label class="badge badge-success">Active</label>
                        @elseif($user->status == 3)
                            <label class="badge badge-danger">Requested</label>
                        @else
                            <label class="badge badge-danger">In- Active</label>
                        @endif
                      </td>
                      <td>
                        <div class="form-check" style="margin-bottom: 45px">
                            <label class="form-check-label">
                            <input type="checkbox" name="status" onchange="changeRegisteredStatus({{$user->id}})" class="form-check-input" <?= ($user->status == 1)?"Checked":""?>> </label>
                        </div>
                      </td>
                      <td>
                        @if($user->type == 2 && $user->is_admin == 0)
                        <div class="form-check" style="margin-bottom: 45px">
                            <label class="form-check-label">
                            <input type="checkbox" name="is_top" onchange="changeTopPacker({{$user->id}})" class="form-check-input" <?= ($user->is_top == 1)?"Checked":""?>> </label>
                        </div>
                        @endif
                      </td>
                      <td>
                        <form action="{{ route('users.destroy',$user->id) }}" method="POST">
                            <a class="btn btn-primary btn-xs" href="{{ route('users.edit',$user->id) }}">Edit</a>
                            <a class="btn btn-info btn-xs" href="{{ route('users.show',$user->id) }}">View</a>
                            @csrf
                            @method('DELETE')
                            <!-- <button type="submit" class="btn btn-danger btn-xs">Delete</button> -->
                        </form>

                          
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {!! $users->links() !!}
              </div>
            </div>
          </div>
        </div>
    </div>
    <script>

      function changeRegisteredStatus(id){
          $.ajax({
              url: '{{route("changeRegisteredStatus")}}',
              data: { id: id},
              type: "GET",
              success: function (data) {
                  console.log('Status Updated Succesfully');
                  $("#status_"+id).html('<label class="badge badge-'+data["color"]+'">'+data["status"]+'</label>');
              }
         });
      }

      function changeTopPacker(id){
          $.ajax({
              url: '{{route("changeTopPacker")}}',
              data: { id: id},
              type: "GET",
              success: function (data) {
                  console.log('Top Packer Updated Succesfully');
              }
         });
      }
    </script>  
@endsection