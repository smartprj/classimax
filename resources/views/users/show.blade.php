@extends('layouts.admin')
@section('content')

    <div class="content-wrapper">
        <div class="page-header">
          <h3 class="page-title"> View User </h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">User</li>
              <li class="breadcrumb-item active" aria-current="page">View User</li>
            </ol>
          </nav>
        </div>

        <div class="row">
          <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                  
                  <div class="form-group">
                    <table class="table table-hover">
                        <tr>
                          <th>Name</th>
                          <td>{{ $user->name }}</td>
                        </tr>
                        <tr>
                          <th>Email</th>
                          <td>{{ $user->email }}</td>
                        </tr>
                        <tr>
                          <th>Location</th>
                          <td>{{ $disName??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Mobile Number</th>
                          <td>{{ $user->phone_no??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Address</th>
                          <td>{{ $user->address??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Pin Code</th>
                          <td>{{ $user->pin_code??"-" }}</td>
                        </tr>
                        <tr>
                          <th>GST Number</th>
                          <td>{{ $user->gst_no??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Slug</th>
                          <td>{{ $user->slug??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Username</th>
                          <td>{{ $user->username??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Wallet</th>
                          <td>{{ $user->wallet??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Total Purchase Lead</th>
                          <td>{{ $user->total_purchase_leads??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Last Wallet</th>
                          <td>{{ $user->last_wallet??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Is Admin?</th>
                          <td>{{ $isAdmin[$user->is_admin]??"-" }}</td>
                        </tr>
                        <tr>
                          <th>User Type</th>
                          <td><label class="badge badge-{{$statusColor[$user->type]??''}}">{{$userType[$user->type]??""}}</label></td>
                        </tr>
                        <tr>
                          <th>Status</th>
                          <td>
                            <label class="badge badge-{{$statusColor[$user->status]??''}}">{{$status[$user->status]??""}}</label>
                          </td>
                        </tr>
                        <tr>
                          <th colspan="2"><a href="{{route('users.index')}}" class="btn btn-primary">Back to User</a></th>
                        </tr>
                    </table>
                  </div>

                </div>
            </div>
          </div>

        </div>
    </div>

@endsection