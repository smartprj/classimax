@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
          <h3 class="page-title">Registered Packers</h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Registered Packers</li>
            </ol>
            
          </nav>
        </div>

        @if ($message = Session::get('success'))
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
          <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Company Logo</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Mobile Number</th>
                      <th>Location</th>
                      <th>Type</th>
                      <th>Status</th>
                      <th width="280px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($users as $user)
                    <tr>
                      <td>{{ ++$i }}</td>
                      <td class="py-1">
                        @if(!empty($user->company_logo))
                          <img src="{{ asset('images/company_logo/'.$user->company_logo) }}" alt="image" />
                        @else
                          -
                        @endif
                      </td>
                      <td>{{ $user->name }}</td>
                      <td>{{ $user->email }}</td>
                      <td>{{ $user->phone_no??"-" }}</td>
                      <td>{{ $user->callUserLocation['name']??"-" }}</td>
                      <td>
                        @if($user->type == 1)
                            <label class="badge badge-primary">User</label>
                        @elseif($user->type == 2)
                            <label class="badge badge-info">Packer</label>
                        @endif
                      </td>
                      <td id="status_{{$user->id}}">
                        @if($user->status == 1)
                            <label class="badge badge-success">Active</label>
                        @elseif($user->status == 3)
                            <label class="badge badge-danger">Requested</label>
                        @else
                            <label class="badge badge-danger">In- Active</label>
                        @endif
                      </td>
                      <td>
                        <a class="btn btn-info btn-xs" href="{{ route('viewProfileRequest',$user->id) }}">View</a>
                        <a class="btn btn-success btn-xs" href="{{ route('approveProfileRequest',$user->id) }}">Approve</a>
                        <a class="btn btn-danger btn-xs" href="{{ route('rejectProfileRequest',$user->id) }}">Reject</a>
                          <!-- <div class="form-check" style="margin-bottom: 45px">
                            <label class="form-check-label">
                            <input type="checkbox" name="status" onchange="changeRegisteredpackerStatus({{$user->id}})" class="form-check-input" <?= ($user->status == 1)?"Checked":""?>> </label>
                          </div> -->
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {!! $users->links() !!}
              </div>
            </div>
          </div>
        </div>
    </div>
    <script>

      function changeRegisteredpackerStatus(id){
          $.ajax({
              url: '{{route("changeRegisteredpackerStatus")}}',
              data: { id: id},
              type: "GET",
              success: function (data) {
                  console.log('Status Updated Succesfully');
                  $("#status_"+id).html('<label class="badge badge-'+data["color"]+'">'+data["status"]+'</label>');
              }
         });
      }
    </script>  
@endsection