@extends('layouts.admin')
@section('content')



    <div class="content-wrapper">
        <div class="page-header">
          <h3 class="page-title"> User </h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">User</li>
              <li class="breadcrumb-item active" aria-current="page">Add User</li>
            </ol>
          </nav>
        </div>

        @if ($errors->any())
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
          <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    
                    <form action="{{ route('users.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                      <div class="form-group">
                        <div class="row">

                          <div class="col-md-3">
                            <div class="form-group">
                              <label for="exampleInputName1"><span style="color: red;">*</span> Name </label>
                              <input type="text" class="form-control" id="exampleInputName1" value="{{old('name')}}" required="required" placeholder="Enter Name" name="name">
                            </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                              <label for="exampleInputName1"><span style="color: red;">*</span> Email </label>
                              <input type="email" class="form-control" id="exampleInputName1" value="{{old('email')}}" required="required" autocomplete="off" placeholder="Enter Email" name="email">
                            </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                              <label for="exampleInputName1"><span style="color: red;">*</span> Password </label>
                              <input type="password" class="form-control" id="exampleInputName1" value="{{old('password')}}" autocomplete="off" required="required" placeholder="Enter Password" name="password">
                            </div>
                          </div>

                          <div class="col-md-3">
                            <label for="exampleInputName1">Location </label>
                            <input type="text" id='ad_locations' class="form-control mb-3" name="locations" value="{{old('location')}}" autocomplete="location" placeholder="Enter Location">
                            <input type="hidden" id="ad_location" name="location" value="{{old('location')}}">
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                              <label for="exampleInputName1"><span style="color: red;">*</span> Slug </label>
                              <input type="text" class="form-control" id="exampleInputName1" value="{{old('slug')}}" required="required" placeholder="Enter Slug" name="slug">
                            </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                              <label for="exampleInputName1"> Username </label>
                              <input type="text" autocomplete="off" class="form-control" id="exampleInputName1" value="{{old('username')}}"  placeholder="Enter Username" name="username">
                            </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                              <label for="exampleInputName1"><span style="color: red;">*</span> Mobile Number </label>
                              <input type="number" class="form-control" id="exampleInputName1" value="{{old('phone_no')}}" required="required" placeholder="Enter Mobile Number" name="phone_no">
                            </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                              <label for="exampleInputName1">GST Number </label>
                              <input type="text" class="form-control" id="exampleInputName1" value="{{old('gst_no')}}"  placeholder="Enter GST Number" name="gst_no">
                            </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                              <label for="company_img"><b>Upload Company Logo</b></label>
                              <input type="file" name="company_logo" id="company_img" accept="image/*" class="form-control" placeholder="Upload Company Logo">
                            </div>
                          </div>


                          <div class="col-md-3">
                            <div class="form-group">
                              <label for="exampleInputName1">Wallet</label>
                              <input type="number" class="form-control" id="exampleInputName1" value="{{old('wallet')}}" placeholder="Enter Wallet" name="wallet">
                            </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                              <label for="exampleInputName1">Total Purchase Lead</label>
                              <input type="number" class="form-control" id="exampleInputName1" value="{{old('total_purchase_leads')}}" placeholder="Enter Total Purchase Lead" name="total_purchase_leads">
                            </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                              <label for="exampleInputName1">Last Wallet</label>
                              <input type="number" class="form-control" id="exampleInputName1" value="{{old('last_wallet')}}" placeholder="Enter Last Wallet" name="last_wallet">
                            </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                              <label for="exampleSelectstatus"><span style="color: red;">*</span> Is Admin?</label>
                              <select class="form-control" id="exampleSelectstatus" required="required" name="is_admin">
                                @foreach($isAdmin as $key => $val)
                                  <option value="{{$key}}" <?= (old('is_admin') == $key)?"Selected":""?>>{{$val}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                              <label for="exampleSelectstatus"><span style="color: red;">*</span> User type</label>
                              <select class="form-control" id="exampleSelectstatus" required="required" name="type">
                                @foreach($userType as $key => $val)
                                  <option value="{{$key}}" <?= (old('type') == $key)?"Selected":""?>>{{$val}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                              <label for="exampleSelectstatus">User Rating</label>
                              <select class="form-control" id="exampleSelectstatus" name="rating">
                                <option value="">Select User Rating</option>
                                @foreach($rating as $key => $val)
                                  <option value="{{$key}}" <?= (old('rating') == $key)?"Selected":""?>>{{$val}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                              <label for="exampleSelectstatus"><span style="color: red;">*</span> Status</label>
                              <select class="form-control" id="exampleSelectstatus" required="required" name="status">
                                @foreach($status as $key => $val)
                                  <option value="{{$key}}" <?= (old('status') == $key)?"Selected":""?>>{{$val}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>

                        </div>
                      </div>

                      
                      
                      <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                      <a href="{{route('users.index')}}" class="btn btn-light">Cancel</a>
                    </form>
                </div>
            </div>
          </div>

        </div>
    </div>
    <script type="text/javascript">
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      $(document).ready(function(){

        $( "#ad_locations" ).autocomplete({
          source: function( request, response ) {
            $.ajax({
              url:"{{route('getLocations')}}",
              type: 'post',
              dataType: "json",
              data: {
                 _token: CSRF_TOKEN,
                 search: request.term
              },
              success: function( data ) {
                 response( data );
              }
            });
          },
          select: function (event, ui) {
             $('#ad_locations').val(ui.item.label);
             $('#ad_location').val(ui.item.value);
             return false;
          }
        });

      });
    </script>
    
@endsection