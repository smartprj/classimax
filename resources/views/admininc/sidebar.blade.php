<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">



    <li class="nav-item">
      <a class="nav-link" href="{{route('admin')}}">
        <span class="menu-title">Dashboard</span>
        <i class="mdi mdi-home menu-icon"></i>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{route('users.index')}}">
        <span class="menu-title">Users</span>
        <i class="mdi mdi-contacts menu-icon"></i>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{route('categories.index')}}">
        <span class="menu-title">Categories</span>
        <i class="mdi mdi-medical-bag menu-icon"></i>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{route('listings.index')}}">
        <span class="menu-title">Listing</span>
        <i class="mdi mdi-format-list-bulleted menu-icon"></i>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{route('registeredUser')}}">
        <span class="menu-title">Registerd Users</span>
        <i class="mdi mdi-contacts menu-icon"></i>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{route('registeredPacker')}}">
        <span class="menu-title">Registerd Packers</span>
        <i class="mdi mdi-contacts menu-icon"></i>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{route('enquiries.index')}}">
        <span class="menu-title">Enquiries</span>
        <i class="mdi mdi-format-list-bulleted menu-icon"></i>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{route('profileRequest')}}">
        <span class="menu-title">Profile Requests</span>
        <i class="mdi mdi-format-list-bulleted menu-icon"></i>
      </a>
    </li>

    <li class="nav-item">
      <a class="dropdown-item" href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
        <span class="menu-title">Log Out</span>
        <i class="mdi mdi-logout mr-2 text-primary"></i>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>
      </a>
    </li>

    <!-- <li class="nav-item">
      <a class="nav-link" href="{{route('plans.index')}}">
        <span class="menu-title">Plans</span>
        <i class="mdi mdi-format-list-bulleted menu-icon"></i>
      </a>
    </li> -->

  </ul>
</nav>