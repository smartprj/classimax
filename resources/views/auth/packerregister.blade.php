@extends('layouts.web')
@section('content')
<style type="text/css">
  .nice-select {
    display: block;
  }
</style>
<section class="login py-5 border-top-1">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-5 col-md-8 align-item-center">
        <div class="border">
          <h3 class="bg-gray p-4">Packer Registration</h3>
          <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
          @csrf
            <fieldset class="p-4">

              <input id="name" type="text" class="form-control mb-3 @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Enter Name">

              @error('name')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror

              <input id="email" type="email" class="form-control mb-3 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Enter Email">

              @error('email')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror

              <input id="phone_no" type="number" class="form-control mb-3 @error('phone_no') is-invalid @enderror" name="phone_no" value="{{ old('phone_no') }}" required autocomplete="phone_no" placeholder="Enter Phone Number">

              @error('phone_no')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror

              <input id="gst_no" type="text" class="form-control mb-3 @error('gst_no') is-invalid @enderror" name="gst_no" value="{{ old('gst_no') }}" required autocomplete="gst_no" placeholder="Enter GST Number">

              @error('gst_no')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror

              <!-- <input id="location" type="text" class="form-control mb-3 @error('location') is-invalid @enderror" name="location" value="{{ old('location') }}" required autocomplete="location" placeholder="Enter Location"> -->
              <input type="text" id='locations' class="form-control mb-3 @error('location') is-invalid @enderror" name="locations" value="{{ old('location') }}" required autocomplete="location" placeholder="Enter Location">
              <input type="hidden" id="location" name="location" value="">
              @error('location')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror

              <div class="form-group">
                <input type="file" name="company_logo" id="company_img" accept="image/*" class="form-control" placeholder="Upload Company Logo">
                <label for="company_img"><small>Upload Company Logo</small></label>
              </div>
              @error('company_logo')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror

              <input id="web_url" type="url" class="form-control mb-3" name="web_url" placeholder="Enter Website URL  Ex.https://www.xyz.com" required>
              @error('web_url')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
               @enderror

              <input id="password" type="password" class="form-control mb-3 @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Enter password">

              @error('password')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror

              <input id="password-confirm" type="password" class="form-control mb-3" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password">


              <!-- for packers type = 2 -->
              <input type="hidden" name="type" value="2">

              <button type="submit" class="btn btn-primary font-weight-bold mt-3">Register</button>
              <!-- <a class="mt-3 d-block text-primary" href="{{route('webRegister')}}">Register As User</a> -->
              <a class="mt-2 d-block text-primary" href="{{route('weblogin')}}">Login</a>
              <a class="mt-3 d-block text-primary" href="{{route('verifyPacker')}}">Verify Packer</a>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  $(document).ready(function(){

    $( "#locations" ).autocomplete({
      source: function( request, response ) {
        $.ajax({
          url:"{{route('getLocations')}}",
          type: 'post',
          dataType: "json",
          data: {
             _token: CSRF_TOKEN,
             search: request.term
          },
          success: function( data ) {
             response( data );
          }
        });
      },
      select: function (event, ui) {
         $('#locations').val(ui.item.label);
         $('#location').val(ui.item.value);
         return false;
      }
    });

  });
</script>
@endsection