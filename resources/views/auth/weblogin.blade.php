@extends('layouts.web')
@section('content')
<section class="login py-5 border-top-1">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-5 col-md-8 align-item-center">
        <div class="border">
          <h3 class="bg-gray p-4">Login Now</h3>
          @if ($message = Session::get('success'))
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                </div>
            </div>
          @endif
          @if ($message = Session::get('error'))
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-error">
                        <p>{{ $message }}</p>
                    </div>
                </div>
            </div>
          @endif
          <form method="POST" action="{{ route('web.logins.submit') }}">
          @csrf
            <fieldset class="p-4">
              <input id="email" type="email" class="form-control mb-3 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
              <input id="password" type="password" class="form-control mb-3 @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
              <div class="loggedin-forgot">
                <label class="form-check-label text-muted pt-3 pb-2 pl-4">
                <input type="checkbox" class="form-check-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>Keep me logged in</label>
              </div>
              <button type="submit" class="btn btn-primary font-weight-bold mt-3">Log in</button>
              <!-- <a class="mt-3 d-block text-primary" href="#!">Forget Password?</a> -->
              <!-- <a class="mt-3 d-inline-block text-primary" href="{{route('webRegister')}}">Register Now</a> -->
              <!-- <a class="mt-3 d-block text-primary" href="{{route('webRegister')}}">Register As User</a> -->
              <a class="mt-2 d-block text-primary" href="{{route('packerRegister')}}">Register As Packer</a>
              <a class="mt-2 d-block text-primary" href="{{route('verifyPacker')}}">Verify Packer</a>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection