@extends('layouts.web')
@section('content')
<style type="text/css">
  .nice-select {
    display: block;
  }
</style>
<section class="login py-5 border-top-1">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-5 col-md-8 align-item-center">
        <div class="border">
          <h3 class="bg-gray p-4">User Registration</h3>
          <form method="POST" action="{{ route('register') }}">
          @csrf
            <fieldset class="p-4">

              <input id="name" type="text" class="form-control mb-3 @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Enter Name">

              @error('name')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror

              <input id="email" type="email" class="form-control mb-3 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Enter Email">

              @error('email')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror

              <!-- <select class="form-control mb-3 @error('type') is-invalid @enderror" name="type">
                <option value="2" {{ old('type')==2?"Selected":"" }}>Packer</option>
                <option value="1" {{ old('type')==1?"Selected":"" }}>User</option>
              </select>
              @error('type')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror -->

              <input id="password" type="password" class="form-control mb-3 @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Enter password">

              @error('password')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror

              <input id="password-confirm" type="password" class="form-control mb-3" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password">

              <!-- for users type = 1 -->
              <input type="hidden" name="type" value="1">

              <button type="submit" class="btn btn-primary font-weight-bold mt-3">Register</button>
              <a class="mt-2 d-block text-primary" href="{{route('weblogin')}}">Login</a>
              <a class="mt-2 d-block text-primary" href="{{route('packerRegister')}}">Register As Packer</a>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection