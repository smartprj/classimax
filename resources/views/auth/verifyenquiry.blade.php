@extends('layouts.web')
@section('content')
<section class="login py-5 border-top-1">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-5 col-md-8 align-item-center">
        <div class="border">
          <h3 class="bg-gray p-4">Verify Enquiry Form</h3>
          @if ($message = Session::get('success'))
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                </div>
            </div>
          @endif
          <form method="POST" action="{{ route('verifyEnqiryForm') }}">
          @csrf
            <fieldset class="p-4">
              <input id="email" type="email" class="form-control mb-3 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Enter Email" autofocus>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
              <input type="text" class="form-control mb-3 @error('token') is-invalid @enderror" name="token" placeholder="Enter Token" required autocomplete="current-token">
                    @error('token')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

              <button type="submit" class="btn btn-primary font-weight-bold mt-3">Verify Enquiry Form</button>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection