@extends('layouts.web')

@section('content')

<style>
.center-to-btn{
	margin-top: 15%;

}
.to-circle i{
	font-size: 31px;
margin: 20%;
}
	.center-to .to-circle {
    height: 50px;
    width: 50px;
    margin: 0 auto;
    border-radius: 100%;
    background-color: #275c6f;
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
    box-shadow: 0 1px 2px rgba(0,0,0,.2);
    color: #fff;
    position: relative;
}
</style>
<section class="page-search">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- Advance Search -->
				<div class="advance-search nice-select-white">
					<form>
						<div class="form-row align-items-center">
							<div class="form-group col-xl-4 col-lg-3 col-md-6">
								<input type="text" class="form-control my-2 my-lg-0" id="inputtext4" placeholder="What are you looking for">
							</div>
							<div class="form-group col-lg-3 col-md-6">
								<select class="w-100 form-control my-2 my-lg-0">
									<option>Category</option>
									<option value="1">Top rated</option>
									<option value="2">Lowest Price</option>
									<option value="4">Highest Price</option>
								</select>
							</div>
							<div class="form-group col-lg-3 col-md-6">
								<input type="text" class="form-control my-2 my-lg-0" id="inputLocation4" placeholder="Location">
							</div>
							<div class="form-group col-xl-2 col-lg-3 col-md-6">

								<button type="submit" class="btn btn-primary active w-100">Search Now</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="section-sm">
	  @if ($errors->any())
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif


            <div class="container">

                <div class="row">

                    <div class="col-md-6 offset-3 col-md-offset-6">

                        @if($message = Session::get('error'))

                            <div class="alert alert-danger alert-dismissible fade in" role="alert">

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">

                                    <span aria-hidden="true">×</span>

                                </button>

                                <strong>Error!</strong> {{ $message }}

                            </div>

                        @endif

                        @if($message = Session::get('success'))

                            <div class="alert alert-success alert-dismissible fade {{ Session::has('success') ? 'show' : 'in' }}" role="alert">

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">

                                    <span aria-hidden="true">×</span>

                                </button>

                                <strong>Success!</strong> {{ $message }}

                            </div>

                        @endif

                        <div class="card card-default">

                            <div class="card-header">

                                Laravel 7 - Razorpay Payment Gateway Integration

                            </div>


                            <div class="card-body text-center">

                                <form action="{{ route('payment') }}" method="POST" >

                                    @csrf

                                    <script src="https://checkout.razorpay.com/v1/checkout.js"

                                            data-key="{{ env('RAZOR_KEY') }}"

                                            data-amount="1000"

                                            data-buttontext="Pay 1 INR"
.
                                            data-name="NiceSnippets"

                                              data-contact="+917838083846"

                                            data-description="Rozerpay"

                                            data-image="{{ asset('/image/nice.png') }}"

                                            data-prefill.name="Anurag Sharma"

                                            data-prefill.email="anuragsharma9266@gmail.com"

                                            data-theme.color="#ff7529">

                                    </script>

                                </form>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        
</section>

@endsection

