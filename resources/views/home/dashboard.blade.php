@extends('layouts.web')

@section('content')

<section class="dashboard section">
  <!-- Container Start -->
  <div class="container">
    <!-- Row Start -->
    <div class="row">
      <div class="col-lg-4">
        <div class="sidebar">
          <!-- User Widget -->
          <div class="widget user-dashboard-profile">
            <!-- User Image -->
            <div class="profile-thumb">
              <img src="images/user/user-thumb.jpg" alt="" class="rounded-circle">
            </div>
            <!-- User Name -->
            @php
           
            $auth = Auth::user();
    //         echo "<pre>";
    //         print_r($auth);
    // echo "</pre>";
      
                           @endphp
            <h5 class="text-center">{{ $auth->name }}</h5>
            <h5 class="text-center text text-info userwallet" >{{ $auth->wallet }}</h5>
        
            <a href="user-profile.html" class="btn btn-main-sm">Purchase Coins</a>
          </div>
          <!-- Dashboard Links -->
          <div class="widget user-dashboard-menu">
            <ul>
              <li class="active">
                <a href="{{ route('dashboard') }}"><i class="fa fa-user"></i> Leads</a>

              </li>
           {{--    <li><a href="dashboard-favourite-ads.html"><i class="fa fa-bookmark-o"></i>My Plans
                  </a></li>
 --}}           {{--    <li><a href="dashboard-archived-ads.html"><i class="fa fa-file-archive-o"></i>Special Offers
                  </a></li> --}}

              <li><a href="{{ route('myLeads') }}"><i class="fa fa-bolt"></i> My Leads<span></span></a>
              </li>
              <li><a href="dashboard-pending-ads.html"><i class="fa fa-bolt"></i> My Purchase<span></span></a>
              </li>

              <li><a href="dashboard-pending-ads.html"><i class="fa fa-bolt"></i> My Payments<span></span></a>
              </li>
              <li><a href="{{ route('home')}}"><i class="fa fa-cog"></i> Logout</a></li>
              {{-- <li><a href="#!" data-toggle="modal" data-target="#deleteaccount"><i class="fa fa-power-off"></i>Delete Account</a></li> --}}
            </ul>
          </div>
          
          <!-- delete-account modal -->
          <!-- delete account popup modal start-->
          <!-- Modal -->
          <div class="modal fade" id="deleteaccount" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header border-bottom-0">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body text-center">
                  <img src="images/account/Account1.png" class="img-fluid mb-2" alt="">
                  <h6 class="py-2">Are you sure you want to delete your account?</h6>
                  <p>Do you really want to delete these records? This process cannot be undone.</p>
                  <textarea class="form-control" name="message" id="" cols="40" rows="4" class="w-100 rounded"></textarea>
                </div>
                <div class="modal-footer border-top-0 mb-3 mx-5 justify-content-center">
                  <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                  <button type="button" class="btn btn-danger">Delete</button>
                </div>
              </div>
            </div>
          </div>
          <!-- delete account popup modal end-->
          <!-- delete-account modal -->

        </div>
      </div>
      <div class="col-lg-8">
        <!-- Recently Favorited -->
        <div class="widget dashboard-container my-adslist">
          <h3 class="widget-header">Leads</h3>
          <table class="table table-responsive product-dashboard-table">
            <thead>
              <tr>
                {{-- <th>Image</th> --}}
                <th>Product Title</th>
                <th class="text-center">Category</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($listings as $listing)


              @php

             $leadid = base64_encode(base64_encode($listing->id))
              @endphp
              <tr>
                {{-- <td class="product-thumb">
                  <img width="80px" height="auto" src="images/products/products-1.jpg" alt="image description"></td> --}}
                <td class="product-details">
                  <span class="add-id"><strong>Name :</strong>{{ $listing->contact_name }}</span>
                  <span class="add-id"><strong>Email ID:</strong> XXXXXXX@XXX.com</span>
                  <span class="add-id"><strong>Contact No:</strong> XXXXXX </span>
                  <span><strong>Posted on: </strong>{{ date('d M Y',strtotime($listing['created'])) }}</span>
                  {{-- <span class="status active"><strong>Status</strong>Active</span> --}}
                  <span class="location"><strong>Location</strong>{{ $listing->callSourceLocation['name'] }},{{ $listing->callDestinationLocation['name'] }}</span>
                </td>
                <td class="product-category">

                  <span class="categories">{{ $listing['source_house_type'] }}</span>

                </td>
                <td class="product-category">

                  <span class="status active"><strong >{{ $listing['amount'] }}</strong></span>
                  <br>
                  <a href="javascript:void(0);" data-id = "{{ $leadid }}" onclick='changeRegisteredpackerStatus(this)' class="btn  btn-outline-info btn-sm">Purchase</a>

                </td>
               
                
              </tr>
              @endforeach
            
            </tbody>
          </table>

        </div>

        <!-- pagination -->
        <div class="pagination justify-content-center">
          <nav aria-label="Page navigation example">
           {{ $listings->links() }}
          </nav>
        </div>
        <!-- pagination -->

      </div>
    </div>
    <!-- Row End -->
  </div>
  <!-- Container End -->
</section>
 <script>

      function changeRegisteredpackerStatus(curr){
        var curr = $(curr);
          $.ajax({
             url: '{{ route('leadbuy') }}',
              data: { lead: curr.data('id')},
              type: "GET",
              dataType:'JSON',
              success: function (data) {

                if(data.type=='fail'){
                  $.notify(data.msg, "warn");
                }else{
              
                    $.notify(data.msg, "success");
                   curr.attr('class','btn btn-success');
                 curr.html('Purchased');
                 getUserWallet();

                }
                 
              }
         });
      }
      function getUserWallet(){
          $.ajax({
             url: '{{ route('wallet') }}',
             
              type: "GET",
              dataType:'JSON',
              success: function (data) {

                if(data.type=='success'){
                  $('.userwallet').text(data.amount);
                }
                 
              }
         });
      }
    </script>  
@endsection