@extends('layouts.web')

@section('content')
<section class="page-search">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- Advance Search -->
				<div class="advance-search nice-select-white">
					<form>
						<div class="form-row align-items-center">
                <div class="col-lg-5 col-md-5">
                      <select name="locations" id='slocation' class="form-control">
                        <option value=''> Select City ...</option>
                        @foreach($locationList as $value => $city)

                        <option value="{{ $value }}">{{ $city }}</option>

                        @endforeach
                      </select>
                      {{-- <input type="select" id='ad_locations' class="form-control my-2 my-lg-1" name="locations" value="{{$locationName??old('locations')}}" autocomplete="location" placeholder="Location">
                                    <input type="hidden" id="ad_location" name="location" value="{{$location??old('location')}}"> --}}
                      <!-- <input type="text" class="form-control my-2 my-lg-1" id="inputLocation4" placeholder="Location"> -->
                    </div>
                    <div class="form-group col-lg-2 col-md-2">

                      <button class="btn btn-success pull-right orbtn">OR</button>
                    </div>
                    <div class="form-group col-xl-5 col-lg-5 col-md-5 align-self-center">
                      <a href="{{route('enquiry')}}" class="btn btn-primary active w-100">Hire Us</a>
                      
                    </div>
            </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="section-sm">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="search-result bg-gray">
					<h2>Results For "Packers And Movers {{ ucfirst($name) }}"</h2>
					{{-- <p>123 Results on 12 December, 2017</p> --}}
				</div>
			</div>
		</div>
		<div class="row">
		
			<div class="col-lg-12 col-md-12">
				
				<!-- ad listing list  -->

				@forelse($listings as $listing)

			
				<div class="ad-listing-list mt-20">
				    <div class="row p-lg-3 p-sm-5 p-4">
				        <div class="col-lg-4 align-self-center">
				            <a href="{{ route('detail',$listing->slug) }}">
				                <img src="{{ asset('images/company_logo/'.$listing->company_logo) }}" class="img-fluid" alt="">
				            </a>
				        </div>
				        <div class="col-lg-8">
				            <div class="row">
				                <div class="col-lg-6 col-md-10">
				                    <div class="ad-listing-content">
				                        <div>
				                            <a href="{{ route('detail',$listing->slug) }}" class="font-weight-bold">{{ $listing->name }}</a>
				                        </div>
				                        <ul class="list-inline mt-2 mb-3">
				                        	@if(!empty($listing->phone_no))
				                            <li class="list-inline-item"><a href="{{ route('detail',$listing->slug) }}"> <i class="fa fa-mobile"></i> {{ $listing->phone_no }} 
				                            </a></li>
				                            	@endif
				                            	@if(!empty($listing->email))
				                            <li class="list-inline-item"><a href="{{ route('detail',$listing->slug) }}"><i class="fa fa-envelope-o"></i> {{ $listing->email }}</a></li>
				                             @endif
				                             @if(!empty($listing->web_url))
				                              <li class="list-inline-item"><a href="{{ $listing->web_url }}"><i class="fa fa-globe"></i> {{  $listing->web_url }}</a></li>
				                                 @endif
				                           
				                        </ul>
				                        <p class="pr-5">{!! $listing->address !!}</p>
				                    </div>
				                </div>
				                <div class="col-lg-6 align-self-center">
				                    <div class="product-ratings float-lg-right pb-3">
				                        <ul class="list-inline">

				                        			<div class="starrr">
							@for($i=1;$i<6;$i++)
					                        	@php

					                        	if($i<=$listing->rating){

					                        	echo '<i class="fa-star fa"></i>';
					                        	}else{
					                        		echo '<i class="fa fa-star-o fa"></i>';
					                        	}
					                        	@endphp
					                           
				                            @endfor

						</div>
				                           
				                        </ul>
				                    </div>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>
				@empty


				@endforelse
				
				<!-- ad listing list  -->

				<!-- pagination -->
				<div class="pagination justify-content-center py-4">

					                {!! $listings->links() !!}

				{{-- 	<nav aria-label="Page navigation example">
						<ul class="pagination">
							<li class="page-item">
								<a class="page-link" href="ad-list-view.html" aria-label="Previous">
									<span aria-hidden="true">&laquo;</span>
									<span class="sr-only">Previous</span>
								</a>
							</li>
							<li class="page-item"><a class="page-link" href="ad-list-view.html">1</a></li>
							<li class="page-item active"><a class="page-link" href="ad-list-view.html">2</a></li>
							<li class="page-item"><a class="page-link" href="ad-list-view.html">3</a></li>
							<li class="page-item">
								<a class="page-link" href="ad-list-view.html" aria-label="Next">
									<span aria-hidden="true">&raquo;</span>
									<span class="sr-only">Next</span>
								</a>
							</li>
						</ul>
					</nav> --}}
				</div>
				<!-- pagination -->
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      $(document).ready(function(){

        $('#slocation').change(function(){
          var value = $(this).val();
         
          if(value!=""){
            window.location.href="{{ route('category') }}/"+value;
          }
          
        });
       
      });
    </script>
<style type="text/css">
  .nice-select{
    width: 100%;
  }
</style>
@endsection