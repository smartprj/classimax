@extends('layouts.web')

@section('content')
<section class="page-title">
	<!-- Container Start -->
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-md-2 text-center">
				<!-- Title text -->
				<h3>About Us</h3>
			</div>
		</div>
	</div>
	<!-- Container End -->
</section>

<section class="section">
  <div class="container">
    <div class="row">
    {{--   <div class="col-lg-6">
        <div class="about-img">
          <img src="images/about/about.jpg" class="img-fluid w-100 rounded" alt="">
        </div>
      </div> --}}
      <div class="col-lg-12 pt-5 pt-lg-0">
        <div class="about-content">
          <h3 class="font-weight-bold">Introduction</h3>
          <p>Movers Nest is a professional service provider. We are here to connect you with professional &amp; quality
service providers.</p>
          <h3 class="font-weight-bold">How we can help</h3>
          <p>You are relocating to a new place! Moving your valuables from place to another place and taking care of
            them is the difficult task. You may face many unpleasant issues, hassles, difficulties while transporting
            and rearranging of goods. But don’t worry! Relocation is now so easy by using the best movers and
            packers services.</p>
              <p>We understand your time is very precious. You don’t need to call so many packers &amp; movers for
            shifting.All you have to do is just fill the form &amp; sit comfortable. We are here to help you. We will find
            out best movers for you and you will get the options of best service provider.</p>
            <p>Here in our website we are helping you to find out best &amp; professional service provider to help you to
            move easily.  These professional experts help you in scheduling the complete procedure from beginning
            to the end. They are trained well with the latest techniques and tactics to handle all types of
            commodities and also capable of handling any kinds of goods.</p>
            <p>By hiring the most reputable services from our website , you can get full assistance in the entire
            operation. These professional will help you from packing of all your valuable items at your new place to
            unpacking the things at your new home.</p>
            <p>There are many moving services available in the market offering their valuable services. But the fact is
            the most of the service provider fail to show their quality in their services, but in our website you will get
            professional service providers with best quality services.</p>
            <p>We understand your time is very precious. You don’t need to call so many packers &amp; movers for shifting.</p>
            <p>All you have to do is just fill the form &amp; sit comfortabl. We are here to help you. We will find out best
            movers for you and you will get the options of best service provider.</p>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="mb-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="heading text-center text-capitalize font-weight-bold py-5">
          <h2>Why Choose us</h2>
        </div>
      </div>
      <div class="col-lg-12 col-sm-12">
     
          <img  src="{{ asset('images/banner/choose_us.png') }}" class="img-fluid" alt="choose us">
         
      
      </div>

    </div>
  </div>
</section>

<section class="section bg-gray">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-sm-6 my-lg-0 my-3">
        <div class="counter-content text-center bg-light py-4 rounded">
          <i class="fa fa-smile-o d-block"></i>
          <span class="counter my-2 d-block" data-count="2314">0</span>
          <h5>Happy Customers</h5>
          </script>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6 my-lg-0 my-3">
        <div class="counter-content text-center bg-light py-4 rounded">
          <i class="fa fa-user-o d-block"></i>
          <span class="counter my-2 d-block" data-count="1013">0</span>
          <h5>Active Members</h5>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6 my-lg-0 my-3">
        <div class="counter-content text-center bg-light py-4 rounded">
          <i class="fa fa-bookmark-o d-block"></i>
          <span class="counter my-2 d-block" data-count="2413">0</span>
          <h5>Verified Ads</h5>
        </div>
      </div>
      <div class="col-lg-3 col-sm-6 my-lg-0 my-3">
        <div class="counter-content text-center bg-light py-4 rounded">
          <i class="fa fa-smile-o d-block"></i>
          <span class="counter my-2 d-block" data-count="200">0</span>
          <h5>Happy Customers</h5>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection