@extends('layouts.web')

@section('content')

<section class="hero-area bg-1 text-center overly">
	<!-- Container Start -->
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- Header Contetnt -->
				<div class="content-block">
					<h1> Movers Who Care</h1>
					<p>Find industry's best servies providers.</p>
					<div class="short-popular-category-list text-center">
						<h2>Popular Category</h2>
						<ul class="list-inline">
							<li class="list-inline-item">
								<a href="{{ route('enquiry')}}"><i class="fa fa-bed"></i> Home Shifting</a></li>
							<li class="list-inline-item">
								<a href="{{ route('enquiry')}}"><i class="fa fa-grav"></i> Office Shifting</a>
							</li>
							<li class="list-inline-item">
								<a href="{{ route('enquiry')}}"><i class="fa fa-car"></i> Vehicle shifting</a>
							</li>
							<li class="list-inline-item">
								<a href="{{ route('enquiry')}}"><i class="fa fa-cutlery"></i> Warehousing</a>
							</li>
							
						</ul>
					</div>

				</div>
				<!-- Advance Search -->
				<div class="advance-search">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-lg-12 col-md-12 align-content-center">
								{{-- <form method="post" accept="{{route('home')}}"> --}}
									@csrf
									<div class="form-row">
										<!-- <div class="form-group col-xl-4 col-lg-3 col-md-6">
											<input type="text" class="form-control my-2 my-lg-1" id="inputtext4"
												placeholder="What are you looking for">
										</div>
										<div class="form-group col-lg-3 col-md-6">
											<select class="w-100 form-control mt-lg-1 mt-md-2">
												<option>Category</option>
												<option value="1">Top rated</option>
												<option value="2">Lowest Price</option>
												<option value="4">Highest Price</option>
											</select>
										</div> -->
										<div class="col-lg-5 col-md-5">
											<select name="locations" id='slocation' class="form-control">
												<option value=''> Select City ...</option>
												@foreach($locationList as $value => $city)

												<option value="{{ $value }}">{{ $city }}</option>

												@endforeach
											</select>
											{{-- <input type="select" id='ad_locations' class="form-control my-2 my-lg-1" name="locations" value="{{$locationName??old('locations')}}" autocomplete="location" placeholder="Location">
                            				<input type="hidden" id="ad_location" name="location" value="{{$location??old('location')}}"> --}}
											<!-- <input type="text" class="form-control my-2 my-lg-1" id="inputLocation4" placeholder="Location"> -->
										</div>
										<div class="form-group col-lg-2 col-md-2">

											<button class="btn btn-success pull-right orbtn">OR</button>
										</div>
										<div class="form-group col-xl-5 col-lg-5 col-md-5 align-self-center">
											<a href="{{route('enquiry')}}" class="btn btn-primary active w-100">Hire Us</a>
											
										</div>
									</div>
								{{-- </form> --}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Container End -->
</section>


<!--===========================================
=            Popular deals section            =
============================================-->
<section class="popular-deals section bg-gray" style="
padding-bottom: 0px;">
	
		<div class="row">
			<div class="col-md-12">
				<div class="section-title">
					<h2>How IT Work</h2>
					{{-- <p></p> --}}
				</div>
			</div>
		</div>
		<div class="row">
			<!-- offer 01 -->
			<div class="col-lg-12">
				
					<img class="img-fluid" src="{{ asset('images/banner/banner.png') }}" alt="Card image cap">
			
			</div>
		
	</div>
</section>
@if($location == "")
<section class="popular-deals section bg-gray">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section-title">
					<h2>Top Packers</h2>
					{{-- <p></p> --}}
				</div>
			</div>
		</div>
		<div class="row">
			<!-- offer 01 -->
			<div class="col-lg-12">
				<div class="">
					@foreach($users as $user)
						<div class="col-sm-12 col-lg-4">
							<!-- product card -->
							<div class="product-item bg-light">
								<div class="card">
									<div class="thumb-content">
										<!-- <div class="price">$200</div> -->
										<a href="{{ route('detail',$user['slug']) }}">
											<img class="card-img-top img-fluid" style="height: 340px;width: 340px;" src="{{ asset('images/company_logo/'.$user['company_logo']) }}" alt="Card image cap">
										</a>
									</div>
									<div class="card-body">
									    <h4 class="card-title"><a href="{{ route('detail',$user['slug']) }}">{{ $user['name'] }}</a></h4>
									    <ul class="list-inline product-meta">
									    	<li class="list-inline-item">
									    		<!-- <a href="#"><i class="fa fa-envelope"></i>{{ $user['email'] }}</a> -->
									    		<a href="{{ route('detail',$user['slug']) }}"><i class="fa fa-map-marker"></i>{{ $districts[$user['location']]??"" }}</a>
									    	</li>
									    	<li class="list-inline-item">
									    		<!-- <a href="{{ route('category')}}"><i class="fa fa-phone"></i>{{ $user['phone_no'] }}</a> -->
									    	</li>
									    </ul>
									    <p class="card-text">
									    <!-- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, aliquam! -->
									</p>
									    <div class="product-ratings">
									    	<ul class="list-inline">
									    		@if(!empty($user['rating']))
										    		@for($i = 0;$i <= 5 ;$i++)
										    			@if($i <= $user['rating'])
										    				<li class="list-inline-item selected"><i class="fa fa-star"></i></li>
										    			@else
										    				<li class="list-inline-item"><i class="fa fa-star"></i></li>
										    			@endif
										    		@endfor
										    	@endif
									    		
									    	</ul>
									    </div>
									</div>
								</div>
							</div>
						</div>
					@endforeach


				</div>
			</div>
		</div>
	</div>
</section>
@endif


<!--==========================================
=            All Packer Section            =
===========================================-->

<section class=" section">
	<!-- Container Start -->
	<div class="container">
		<div class="row">
			<div class="col-12">
				<!-- Section title -->
				<div class="section-title">
					<h2>All Packers</h2>
					<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis, provident!</p> -->
				</div>
				<div class="row">

					@foreach($packers as $packer)
					<!-- Category list -->
					<div class="col-lg-3 offset-lg-0 col-md-5 offset-md-1 col-sm-6">
						<div class="category-block">
							<div class="header">
								<img class="card-img-top img-fluid" style="height: 50px;width: 50px;border-radius: 50%;" src="{{ asset('images/company_logo/'.$packer['company_logo']) }}" alt="Card image cap">
								<!-- <i class="fa fa-laptop icon-bg-1"></i> -->
								<h4>{{$packer['name']}}</h4>
							</div>
							<ul class="category-list">
								<li><a href="{{ route('category')}}">Location <span>{{$districts[$packer['location']]}}</span></a></li>
								<li><a href="{{ route('category')}}">Rating <span><?php echo !empty($packer['rating'])?$packer['rating']:0 ?> star</span></a></li>
							</ul>
						</div>
					</div> <!-- /Category List -->
					@endforeach

				</div>
			</div>
		</div>
	</div>
	<!-- Container End -->
</section>


<!--====================================
=            Call to Action            =
=====================================-->

<section class="call-to-action overly bg-3 section-sm">
	<!-- Container Start -->
	<div class="container">
		<div class="row justify-content-md-center text-center">
			<div class="col-md-8">
				<div class="content-holder">
					<h2>Start today to get more exposure and
					grow your business</h2>
					<ul class="list-inline mt-30">
						<li class="list-inline-item"><a class="btn btn-main" href="{{ route('enquiry') }}">Add Listing</a></li>
						<li class="list-inline-item"><a class="btn btn-secondary" href="{{ route('location')}}">Browser Listing</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- Container End -->
</section>
<script type="text/javascript">
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      $(document).ready(function(){

      	$('#slocation').change(function(){
      		var value = $(this).val();
      		
      		if(value!=""){
      			window.location.href="{{ route('category') }}/"+value;
      		}
      		
      	});
        $( "#ad_locations" ).autocomplete({
          source: function( request, response ) {
            $.ajax({
              url:"{{route('getLocations')}}",
              type: 'post',
              dataType: "json",
              data: {
                 _token: CSRF_TOKEN,
                 search: request.term
              },
              success: function( data ) {
                 response( data );
              }
            });
          },
          select: function (event, ui) {
             $('#ad_locations').val(ui.item.label);
             $('#ad_location').val(ui.item.value);
             return false;
          }
        });

      });
    </script>
<style type="text/css">
	.hero-area .advance-search .form-control{
		width: 100%;
	}
</style>
<style>
	.
	</style>
@endsection
