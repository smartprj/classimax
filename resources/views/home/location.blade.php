@extends('layouts.web')

@section('content')

<section class="page-search">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <!-- Advance Search -->
        <div class="advance-search nice-select-white">
          <form>
            <div class="form-row align-items-center">
                <div class="col-lg-5 col-md-5">
                      <select name="locations" id='slocation' class="form-control">
                        <option value=''> Select City ...</option>
                        @foreach($locationList as $value => $city)

                        <option value="{{ $value }}">{{ $city }}</option>

                        @endforeach
                      </select>
                      {{-- <input type="select" id='ad_locations' class="form-control my-2 my-lg-1" name="locations" value="{{$locationName??old('locations')}}" autocomplete="location" placeholder="Location">
                                    <input type="hidden" id="ad_location" name="location" value="{{$location??old('location')}}"> --}}
                      <!-- <input type="text" class="form-control my-2 my-lg-1" id="inputLocation4" placeholder="Location"> -->
                    </div>
                    <div class="form-group col-lg-2 col-md-2">

                      <button class="btn btn-success pull-right orbtn">OR</button>
                    </div>
                    <div class="form-group col-xl-5 col-lg-5 col-md-5 align-self-center">
                      <a href="{{route('enquiry')}}" class="btn btn-primary active w-100">Hire Us</a>
                      
                    </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section-sm">
  <div class="container">
    {{-- <div class="row">
      <div class="col-md-12">
        <div class="search-result bg-gray">
          <h2>Results For "Electronics"</h2>
          <p>123 Results on 12 December, 2017</p>
        </div>
      </div>
    </div> --}}
    <div class="row">
      
      <div class="col-lg-12 col-md-12">
        <div class="category-search-filter">
          <div class="row">
            <div class="col-md-6 text-center text-md-left">
              <strong>Locations</strong>
              {{-- <select>
                <option>Most Recent</option>
                <option value="1">Most Popular</option>
                <option value="2">Lowest Price</option>
                <option value="4">Highest Price</option>
              </select> --}}
            </div>
            <div class="col-md-6 text-center text-md-right mt-2 mt-md-0">
              <div class="view">
                <strong>Views</strong>
                <ul class="list-inline view-switcher">
                  <li class="list-inline-item">
                    <a href="#!" onclick="event.preventDefault();" class="text-info"><i class="fa fa-th-large"></i></a>
                  </li>
                  <li class="list-inline-item">
                    <a href=""><i class="fa fa-reorder"></i></a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="product-grid-list">
          <div class="row mt-30">

            @forelse($locations as $lid => $location)
            <div class="col-lg-3 col-md-6">
              <!-- product card -->
              <div class="product-item bg-light">
                <div class="card">
                 
                  <div class="card-body text text-center"> 
                      <h4 class="card-title"><a href="{{ route('category','packers-and-movers-in-'.strtolower($location)) }}" >{{ $location }}</a></h4>
                      
                  </div>
                </div>
              </div>
            </div>
            @empty
              <div class="col-lg-12 col-md-12">
              <!-- product card -->
              <div class="product-item bg-light">
                <div class="card">
                 
                  <div class="card-body">
                      <h4 class="card-title">No Location Found</h4>
                      
                  </div>
                </div>
              </div>
            </div>
          @endforelse
           
         
        
          </div>
        </div>
   
      </div>
    </div>
  </div> 
</section>
<script type="text/javascript">
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      $(document).ready(function(){

        $('#slocation').change(function(){
          var value = $(this).val();
         
          if(value!=""){
            window.location.href="{{ route('category') }}/"+value;
          }
          
        });
       
      });
    </script>
<style type="text/css">
  .nice-select{
    width: 100%;
  }
</style>
@endsection