@extends('layouts.web')

@section('content')
<section class="page-search">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- Advance Search -->
				<div class="advance-search nice-select-white">
					<form>
						 <div class="form-row align-items-center">
                <div class="col-lg-5 col-md-5">
                      <select name="locations" id='slocation' class="form-control">
                        <option value=''> Select City ...</option>
                        @foreach($locationList as $value => $city)

                        <option value="{{ $value }}">{{ $city }}</option>

                        @endforeach
                      </select>
                      {{-- <input type="select" id='ad_locations' class="form-control my-2 my-lg-1" name="locations" value="{{$locationName??old('locations')}}" autocomplete="location" placeholder="Location">
                                    <input type="hidden" id="ad_location" name="location" value="{{$location??old('location')}}"> --}}
                      <!-- <input type="text" class="form-control my-2 my-lg-1" id="inputLocation4" placeholder="Location"> -->
                    </div>
                    <div class="form-group col-lg-2 col-md-2">

                      <button class="btn btn-success pull-right orbtn">OR</button>
                    </div>
                    <div class="form-group col-xl-5 col-lg-5 col-md-5 align-self-center">
                      <a href="{{route('enquiry')}}" class="btn btn-primary active w-100">Hire Us</a>
                      
                    </div>
            </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<!--===================================
=            Store Section            =
====================================-->
<section class="section bg-gray">
	<!-- Container Start -->
	<div class="container">
		<div class="row">
			<!-- Left sidebar -->
			<div class="col-lg-8">
				<div class="product-details">
					<h1 class="product-title">{{ $listing->name }}</h1>
					<div class="product-meta">
						<ul class="list-inline">
						{{-- 	<li class="list-inline-item"><i class="fa fa-user-o"></i> By <a href="user-profile.html">Andrew</a></li> --}}
							<li class="list-inline-item"><i class="fa fa-folder-open-o"></i> Mobile<a href="#">{{ $listing->phone_no }} </a></li>
							<li class="list-inline-item"><i class="fa fa-location-arrow"></i> Email<a href="#">{{ $listing->email }}</a></li>
							  @if(!empty($listing->web_url))
							<li class="list-inline-item"><i class="fa fa-globe"></i> Email<a href="{{ $listing->web_url }}">{{ $listing->web_url }}</a></li>
							@endif
						</ul>
					</div>

					<!-- product slider -->
					<div class="">
						<div class="product-slider-item my-4" data-image="">
							<img class="img-fluid w-100" src="{{ asset('images/company_logo/'.$listing->company_logo) }}">
						</div>

					</div>
					<!-- product slider -->
 
					<div class="content mt-5 pt-5">
						
						<div class="tab-content" id="pills-tabContent">
							<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
								<h3 class="tab-title">{{ $listing->name }}</h3>
								<p>{{ $listing->address }}</p>
								<p>{{ $listing->description }}</p>

							

							</div>
					
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="sidebar">
					{{-- <div class="widget price text-center">
						<h4>Price</h4>
						<p>$230</p>
					</div> --}}
					<!-- User Profile widget -->
	{{-- 				<div class="widget user text-center">
						<img class="rounded-circle img-fluid mb-5 px-5" src="{{ asset('images/user/user-thumb.jpg') }}" alt="">
						<h4><a href="user-profile.html">Jonathon Andrew</a></h4>
						<p class="member-time">Member Since Jun 27, 2017</p>
						<a href="single.html">See all ads</a>
						<ul class="list-inline mt-20">
							<li class="list-inline-item"><a href="contact-us.html" class="btn btn-contact d-inline-block  btn-primary px-lg-5 my-1 px-md-3">Contact</a></li>
							<li class="list-inline-item"><a href="single.html" class="btn btn-offer d-inline-block btn-primary ml-n1 my-1 px-lg-4 px-md-3">Make an
									offer</a></li>
						</ul>
					</div> --}}
					<!-- Map Widget -->
					{{-- <div class="widget map">
						<div class="map">
							<div id="map" data-latitude="51.507351" data-longitude="-0.127758"></div>
						</div>
					</div> --}}
					<!-- Rate Widget -->
					<div class="widget rate">
						<!-- Heading -->
						<h5 class="widget-header text-center">Packers Rating
							</h5>
						<!-- Rate -->
						<div class="starrr">
							@for($i=1;$i<6;$i++)
					                        	@php

					                        	if($i<=$listing->rating){

					                        	echo '<i class="fa-star fa"></i>';
					                        	}else{
					                        		echo '<i class="fa fa-star-o fa"></i>';
					                        	}
					                        	@endphp
					                           
				                            @endfor

						</div>
					</div>
					<!-- Safety tips widget -->
					<div class="widget disclaimer">
						<h5 class="widget-header">Services</h5>
						<ul id="list">
							<li>Household Shifting</li>
							<li>Office Shifting</li>
							<li>Domestic Moving</li>
							<li>International Moving</li>
							<li>Pet Moving</li>
							<li>Fine Art Moving</li>
							<li>Vehicle Transportation</li>
							<li>Home Storage</li>
							<li>Warehousing</li>
							<li>Moving Insurance</li>
						</ul>
					</div>
					<!-- Coupon Widget -->
				{{-- 	<div class="widget coupon text-center">
						<!-- Coupon description -->
						<p>Have a great product to post ? Share it with
							your fellow users.
						</p>
						<!-- Submii button -->
						<a href="single.html" class="btn btn-transparent-white">Submit Listing</a>
					</div> --}}

				</div>
			</div>

		</div>
	</div>
	<!-- Container End -->
</section>
<script type="text/javascript">
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      $(document).ready(function(){

        $('#slocation').change(function(){
          var value = $(this).val();
         
          if(value!=""){
            window.location.href="{{ route('category') }}/"+value;
          }
          
        });
       
      });
    </script>
<style type="text/css">
  .nice-select{
    width: 100%;
  }

          
       
        #list{
            color: green;
            background: white;
            font-size: 30px;
        }
  
   
        li{
            list-style: none;
            font-size: 20px;
        }
  
    
        li::before{
  
         
            content: "\00BB ";
             font-size: 22px;
        }
   
</style>
@endsection