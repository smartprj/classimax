@extends('layouts.web')

@section('content')

<style>
.center-to-btn{
	margin-top: 15%;

}
.to-circle i{
	font-size: 31px;
margin: 20%;
}
	.center-to .to-circle {
    height: 50px;
    width: 50px;
    margin: 0 auto;
    border-radius: 100%;
    background-color: #275c6f;
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
    box-shadow: 0 1px 2px rgba(0,0,0,.2);
    color: #fff;
    position: relative;
}
</style>
<section class="page-search">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- Advance Search -->
				<div class="advance-search nice-select-white">
					<form>
						 <div class="form-row align-items-center">
                <div class="col-lg-5 col-md-5">
                      <select name="locations" id='slocation' class="form-control">
                        <option value=''> Select City ...</option>
                        @foreach($locationList as $value => $city)

                        <option value="{{ $value }}">{{ $city }}</option>

                        @endforeach
                      </select>
                      {{-- <input type="select" id='ad_locations' class="form-control my-2 my-lg-1" name="locations" value="{{$locationName??old('locations')}}" autocomplete="location" placeholder="Location">
                                    <input type="hidden" id="ad_location" name="location" value="{{$location??old('location')}}"> --}}
                      <!-- <input type="text" class="form-control my-2 my-lg-1" id="inputLocation4" placeholder="Location"> -->
                    </div>
                    <div class="form-group col-lg-2 col-md-2">

                      <button class="btn btn-success pull-right orbtn">OR</button>
                    </div>
                    <div class="form-group col-xl-5 col-lg-5 col-md-5 align-self-center">
                      <a href="{{route('enquiry')}}" class="btn btn-primary active w-100">Hire Us</a>
                      
                    </div>
            </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="section-sm">
	  @if ($errors->any())
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif

	<form action="{{ route('enquiry') }}" method="post" >
		@csrf
	<div class="container">
		{{-- <div class="row">
			<div class="col-md-12">
				<div class="search-result bg-gray">
					<h2>Results For "Packers And Movers {{ ucfirst($name) }}"</h2>
				
				</div>
			</div>
		</div> --}}
		<div class="row">
		
			<div class="col-lg-12 col-md-12">

        <div class="row border border-gary px-3 px-md-4 py-4 mb-5">
          <div class="col-lg-12">
            <h3>Enquiry Form</h3>
          </div>
          <div class="col-lg-6">
            <h6 class="font-weight-bold pt-4 pb-1">Contact Name:</h6>
            <input type="text" name="contact_name" placeholder="Contact name" class="form-control bg-white" required="">
            <h6 class="font-weight-bold pt-4 pb-1">Contact Number:</h6>
            <input type="text" name="contact_number" placeholder="Contact Number" class="form-control bg-white" required="">
          </div>
          <div class="col-lg-6">
            <h6 class="font-weight-bold pt-4 pb-1">Contact Email:</h6>
            <input type="email" name="contact_email"  placeholder="name@yourmail.com" class="form-control bg-white" required="">
            
          </div>
        </div>
    
				{{-- <div class="category-search-filter">
					<div class="row">

						<div class="col-md-6 text-center text-md-left">
							<strong>Short</strong>
							<select>
								<option>Most Recent</option>
								<option value="1">Most Popular</option>
								<option value="2">Lowest Price</option>
								<option value="4">Highest Price</option>
							</select>
						</div>
						<div class="col-md-6 text-center text-md-right mt-2 mt-md-0">
							<div class="view">
								<strong>Views</strong>
								<ul class="list-inline view-switcher">
									<li class="list-inline-item">
									
									</li>
									<li class="list-inline-item">
										<a href="category.html" class="text-info"><i class="fa fa-reorder"></i></a>
									</li>
								</ul>
							</div>
						</div>

					</div>
				</div> --}}
				<div class="row border border-gary px-3 px-md-4 py-4 mb-5">
            <div class="col-md-5 pam-form-from-left">
              <div class="space-for-checbox">
                <span>Source Details *</span>
              </div>
              <div formgroupname="from" class="ng-touched ng-dirty ng-invalid">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                    <textarea class="form-control light-placeholder ta-ver-resize ng-untouched ng-pristine ng-invalid" name="source_address" placeholder="Enter Current Address" rows="2"></textarea>
                      <!---->
                    </div>
                  </div>
                </div>

                <!---->

                <div class="row">
                  <div class="col-xs-7 col-sm-8">
                    <div class="form-group">
                      <p class="form-label">Location<!----></p>
                      <input class="form-control location" name="" placeholder="Area/Locality" type="text">
                        <input type="hidden" class="locationhidden" name="source_locality" value="">
                      <!---->
                    </div>
                  </div>

                  <div class="col-xs-5 col-sm-4">
                    <div class="form-group">
                      <p class="form-label">Pin Code *</p>
                      <input class="form-control " name="source_pin" maxlength="6" placeholder="Pin Code" type="text">
                      <!---->
                    </div>
                  </div>
                </div>
              </div>

              
              <div class="pam-type-complete-section">
                <div class="row">
     
                    <div class="col-sm-4 col-md-6 col-lg-4">
                      <div class="form-group">
                        <p class="form-label">Property Type *</p>
                        <div class="hl-select-arrow-outline">
                          <select class="form-control" name="source_house_type">
                            <option disabled="" selected="" value="null">Select</option>
                            <!----><option value="1 BHK">1 BHK
                            </option><option value="2 BHK">2 BHK
                            </option><option value="3 BHK">3 BHK
                            </option><option value="4 BHK">4 BHK
                            </option><option value="Villa">Villa
                            </option><option value="Vehicle">Vehicle
                            </option><option value="Office">Office
                            </option>
                          </select>
                        </div>
                        <!---->
                      </div>
                    </div>

                    <div class="col-sm-4 col-md-6 col-lg-4">
                      <div class="form-group">
                        <p class="form-label">Select Floor *</p>
                        <div class="hl-select-arrow-outline">
                          <select class="form-control" name="source_floor">
                            <option disabled="" selected="" value="null">Select</option>
                            <!----><option value="N/A">N/A
                            </option><option value="Ground">Ground
                            </option><option value="1st">1st
                            </option><option value="2nd">2nd
                            </option><option value="3rd">3rd
                            </option><option value="4th">4th
                            </option><option value="5th">5th
                            </option><option value="6th">6th
                            </option><option value="7th">7th
                            </option><option value="8th">8th
                            </option><option value="9th">9th
                            </option><option value="10+">10+
                            </option>
                          </select>
                        </div>
                        <!---->
                      </div>
                    </div>

                    <div class="col-sm-4 col-md-6 col-lg-4">
                      <div class="form-group">
                        <p class="form-label">Service Lift*</p>
                        <div class="hl-select-arrow-outline">
                          <select class="form-control" name="source_lift">
                            <option disabled="" selected="" value="null">Select</option>
                            <option value="1">Available</option>
                            <option value="0">Not Available</option>
                          </select>
                        </div>
                        <!---->
                      </div>
                    </div>
                  

                  <!---->

                </div>
              </div>
              

            </div>
            <div class="col-md-2 center-to-btn">
              <div class="center-to">
                <div class="line-dotted"></div>
                <div class="to-circle">
                	<i class="fa fa-arrow-right" aria-hidden="true"></i>

                  {{-- <div class="center-it">TO</div> --}}
                </div>
              </div>
            </div>
            <div class="col-md-5 pam-form-to-right ng-touched ng-dirty ng-invalid" formgroupname="to">
              <div class="row">
                <div class="col-sm-12">
                  <div class="space-for-checbox">
                    <span>Destination Details</span>
                    <label class="not-decided-block radio-inline pull-right">
                      <input class="hl-input-checkbox not-decided-check" name="destination_check" type="checkbox">
                      <span class="checkbox-name-in">Not decided</span>
                    </label>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <textarea class="form-control light-placeholder ta-ver-resize ng-untouched ng-pristine ng-invalid" name="destination_address" placeholder="Enter Moving Address" rows="2"></textarea>
                    <!---->
                  </div>
                </div>
              </div>

              <!---->

              <div class="row">
                <div class="col-xs-7 col-sm-8">
                  <div class="form-group">
                    <p class="form-label">Location<!----></p>
                    <input class="form-control location" name="locality" data-class="locationhidden" placeholder="Area/Locality" id="locations" type="text">
                     <input type="hidden" class="locationhidden" name="destination_location" value="">
                    <!---->
                  </div>
                </div>

                <div class="col-xs-5 col-sm-4">
                  <div class="form-group">
                    <p class="form-label">Pin Code *</p>
                    <input class="form-control " name="destination_pin" maxlength="6" placeholder="Pin Code" type="text">
                    <!---->
                  </div>
                </div>
              </div>

              
              <div class="pam-type-complete-section">
                <div class="row">
                  <div class="col-sm-4 col-md-6 col-lg-4">
                    <div class="form-group">
                      <p class="form-label">Property Type *</p>
                      <div class="hl-select-arrow-outline">
                        <select class="form-control" name="destination_house_type">
                          <option disabled="" selected="" value="null">Select</option>
                          <!----><option value="1 BHK">1 BHK
                          </option><option value="2 BHK">2 BHK
                          </option><option value="3 BHK">3 BHK
                          </option><option value="4 BHK">4 BHK
                          </option><option value="Villa">Villa
                          </option><option value="Vehicle">Vehicle
                          </option><option value="Office">Office
                          </option>
                        </select>
                      </div>
                      <!---->
                    </div>
                  </div>

                  <div class="col-sm-4 col-md-6 col-lg-4">
                    <div class="form-group">
                      <p class="form-label">Select Floor *</p>
                      <div class="hl-select-arrow-outline">
                        <select class="form-control" name="floor">
                          <option disabled="" selected="" value="null">Select</option>
                          <!----><option value="">N/A
                          </option><option value="Ground">Ground
                          </option><option value="1st">1st
                          </option><option value="2nd">2nd
                          </option><option value="3rd">3rd
                          </option><option value="4th">4th
                          </option><option value="5th">5th
                          </option><option value="6th">6th
                          </option><option value="7th">7th
                          </option><option value="8th">8th
                          </option><option value="9th">9th
                          </option><option value="10+">10+
                          </option>
                        </select>
                      </div>
                      <!---->
                    </div>
                  </div>

                  <div class="col-sm-4 col-md-6 col-lg-4">
                    <div class="form-group">
                      <p class="form-label">Service Lift<sup>#</sup> *</p>
                      <div class="hl-select-arrow-outline">
                        <select class="form-control" name="destination_lift">
                          <option disabled="" selected="" value="null">Select</option>
                          <option value="1">Available</option>
                          <option value="0">Not Available</option>
                        </select>
                      </div>
                      <!---->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

				<!-- ad listing list  -->
				<div class="row  border border-gary px-3 px-md-4 py-4 mb-5">
				<div class="col-md-3">
				<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					@foreach($categorylist as $menu)
					
					@if($loop->first)
					  <a class="nav-link btnOverlay active" id="menu{{ $menu['menu_id'] }}" data-toggle="pill" href="#tabmenu{{ $menu['menu_id'] }}" role="tab" aria-controls="v-pills-home" aria-selected="true">{{ $menu['parent'] }}</a>

					 @else
					 	  <a class="btnOverlay nav-link" id="menu{{ $menu['menu_id'] }}" data-toggle="pill" href="#tabmenu{{ $menu['menu_id'] }}" role="tab" aria-controls="v-pills-home" aria-selected="true">{{ $menu['parent'] }}</a>
					 	  @endif
					  @endforeach
					{{--   <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Profile</a>
					  <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Messages</a>
					  <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Settings</a> --}}
					</div>
				</div>
					<div class="col-md-9">
					<div class="tab-content parentmenu" id="v-pills-tabContent">

						@foreach($categorylist as $menu)
					@if($loop->first)

					  <div class="tab-pane fade show active " id="tabmenu{{ $menu['menu_id'] }}" role="tabpanel" aria-labelledby="tabmenu{{ $menu['menu_id'] }}">
					 

					 @else
					 	  <div class="tab-pane fade" id="tabmenu{{ $menu['menu_id'] }}" role="tabpanel" aria-labelledby="tabmenu{{ $menu['menu_id'] }}">
						 @endif

						 {{-- Sub tab start --}}

						 @if(!empty($menu['child']))
						 		<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
						 	@foreach($menu['child'] as $submenu)

							  <li class="nav-item">
						 		@if($loop->first)
							    <a class="nav-link btnOverlay active" id="submenu{{ $submenu['menu_id'] }}" data-toggle="tab" href="#tabsubmenu{{ $submenu['menu_id'] }}" role="tab" aria-controls="home" aria-selected="true">{{ $submenu['parent'] }}</a>
							     @else
							       <a class="nav-link btnOverlay" id="submenu{{ $submenu['menu_id'] }}" data-toggle="tab" href="#tabsubmenu{{ $submenu['menu_id'] }}" role="tab" aria-controls="home" aria-selected="true">{{ $submenu['parent'] }}</a>
							    @endif
							  </li>
							 
							@endforeach
							</ul>
							<div class="tab-content row" id="pills-tabContent">

								 	@foreach($menu['child']  as $submenu)
								 	@if($loop->first)
							  <div class="tab-pane fade show active" id="tabsubmenu{{ $submenu['menu_id'] }}" role="tabpanel" aria-labelledby="home-tab" style="width: 100%">
							  	 @else
							  	 			  <div class="tab-pane fade" id="tabsubmenu{{ $submenu['menu_id'] }}" role="tabpanel" aria-labelledby="home-tab" style="width: 100%">

							  	  @endif
							  	


							  	  {{-- selective items start --}}
 									@if(!empty($submenu['child']))
							  	  @foreach($submenu['child']  as $items)

							  	 
							  		<div class="col-md-4 divOverlay" style="float: left">
							  			  {{ $items['parent'] }}
								  		<div class="input-group">
										  <input type="button" value="-" class="button-minus" data-field="quantity">
										  <input type="number" step="1" max="" value="1" name="quantity[{{ $items['menu_id'] }}]" class="quantity-field">
										  <input type="button" value="+" class="button-plus" data-field="quantity">
										</div>
									</div>

									@endforeach
 	  							@endif

							  			


							  	  {{-- selective items end --}}
							  </div>
							  	@endforeach
							  
							</div>
							@endif

						 {{-- Sub tab End --}}


					 	  

					  </div>
					  @endforeach
					
				</div>
				</div>
				</div>

				
				<!-- ad listing list  -->

				<!-- pagination -->
				
				<!-- pagination -->
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 offset-md-9">
			<input type="submit" class="btn btn-success pull-right"></input>
				
			</div>
		</div>
	</div>
</div>
</div>
</form>
</section>


<script type="text/javascript">
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  $(document).ready(function(){

    $( ".location" ).autocomplete({
      source: function( request, response ) {
        $.ajax({
          url:"{{route('getLocations')}}",
          type: 'post',
          dataType: "json",
          data: {
             _token: CSRF_TOKEN,
             search: request.term
          },
          success: function( data ) {
             response( data );
          }
        });
      },
      select: function (event, ui) {

         $(this).val(ui.item.label);
         // $('#location').val(ui.item.value);
      	$(this).closest('.form-group').find('.locationhidden').val(ui.item.value);
         return false;
      }
    });

  });
</script>
<script type="text/javascript">
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      $(document).ready(function(){

        $('#slocation').change(function(){
          var value = $(this).val();
         
          if(value!=""){
            window.location.href="{{ route('category') }}/"+value;
          }
          
        });
       
      });
    </script>
<style type="text/css">
  .nice-select{
    width: 100%;
  }
</style>
@endsection
<style>
	.btnOverlay {
    text-decoration: none;
    font-size: 1.2rem;
    color: #007bff;
    font-weight: bolder;
    border: 2px solid #007bff;
    border-radius: 5px;
    padding: 5px 10px;
    margin: 14px;
}

.divOverlay {
    text-decoration: none;
    font-size: 1.2rem;
    color: #007bff;
    font-weight: bolder;
    border: 2px solid #007bff;
    border-radius: 5px;
   
   
}
.parentmenu{
	 text-decoration: none;
	padding: 1% 2%;
	 border-radius: 5px;
	   margin: 14px;
	  border: 1px solid #007bff;
	}
	input,
textarea {
  border: 1px solid #eeeeee;
  box-sizing: border-box;
  margin: 0;
  outline: none;
  padding: 10px;
}

input[type="button"] {
  -webkit-appearance: button;
  cursor: pointer;
}

input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
}

.input-group {
  clear: both;
  margin: 15px 0;
  position: relative;
}

.input-group input[type='button'] {
  background-color: #eeeeee;
  min-width: 38px;
  width: auto;
  transition: all 300ms ease;
}

.input-group .button-minus,
.input-group .button-plus {
  font-weight: bold;
  height: 38px;
  padding: 0;
  width: 38px;
  position: relative;
}

.input-group .quantity-field {
  position: relative;
  height: 38px;
  left: -6px;
  text-align: center;
  width: 62px;
  display: inline-block;
  font-size: 13px;
  margin: 0 0 5px;
  resize: vertical;
}

.button-plus {
  left: -13px;
}

input[type="number"] {
  -moz-appearance: textfield;
  -webkit-appearance: none;
}

	</style>

