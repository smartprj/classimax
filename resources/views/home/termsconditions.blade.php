@extends('layouts.web')

@section('content')
<section class="page-title">
	<!-- Container Start -->
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-md-2 text-center">
				<!-- Title text -->
				<h3>Terms & Conditions</h3>
			</div>
		</div>
	</div>
	<!-- Container End -->
</section>

<section class="section">
  <div class="container">
    <div class="row">
      <div class="col-lg-10 mx-auto p-0">
        <div class="terms-condition-content">
          <h3 class="py-3">Movers Nest Terms & Condition</h3>
          <p>Moversnest is the sole owner &amp; operator of moversnest.com and the app moversnest. We (moversnest)
are only a online medium which users can avail required services from third party service providers.
Service providers will provide required services directly to the customer after accepting job through
moversnest.
Once you have a booked a service , you consent that your information may be shared with the service
providers.
You are (customer) responsible to verify and ensure that the Service Provider(s) you are dealing with
is/are genuine, before getting into any oral or written agreement, contracts, or dealings with them;
moversnest will not be liable to you in case of any loss or damages of any kind, incurred by you, as a
result of your failure to do so. </p>
          <p>Rules for customers or users:-
Moversnest doo not require Service Users to create an account for the request, use or purchase of
Services listed on the Website, but we will require you to furnish fill the given form on the website
The details that you provide are completely your responsibility, and moversnest is not liable for any
of the information provided by you.
You also acknowledge and agree that by submitting your contact details, you consent to be
contacted through SMS, Phone calls, Emails, or any other form of electronic communications,
related to the Services requested by you. </p>
          <h5 class="py-3">Requesting for a Service: </h5>
          <p><span class="font-weight-bold text-dark">• Personal Information you provide to us:</span> We receive and
            store any information that you enter on the Service or provide to us in any other way, for example, when you
            download the Service, set up a profile within the Service, or access, upload or download material to or from
            the Service, including when that material is accessed on a third party platform, service or social network
            (such as Facebook), that social network or third-party may provide us with the information you agreed the
            social network or other third party platform could provide to as through the social networks€TMs or third
            party platforma€TMs Application Programming Interface (API) based on your settings on the third party social
            network or platform. The types of personal information collected may include your email address, profile
            picture, username and password. We use this information to validate you as a user when using the Service, to
            provide the Service to you, including administration of your user account, to notify you of changes to the
            Service or about any changes to our terms and conditions or this privacy policy, to manage our business,
            including financial reporting, for the development of new products and services, to send you newsletters to
            market and advertise our products and services by email, to comply with applicable laws, court orders and
            government enforcement agency requests, for research and analytics purposes including to improve the quality
            of the Service and to ensure that the Service is presented in the most effective manner for you and your
            device. Details of Correspondence: If you contact us, we may keep a record of that correspondence. We will
            not retain a record of that correspondence for longer than is reasonably necessary. </p>
          <p><span class="font-weight-bold text-dark">• Personal Information that we automatically collect:</span> When
            you use the Service, we automatically collect information about the device you use to access it and your
            usage of the Service. The information we collect may include (where available) the type and model of device
            you use, the device's unique device identifier, operating system, browser type, language options, current
            time zone and mobile network information to allow you to use the Service, for system administration purposes
            and to report aggregated, anonymised information to our business partners. If you do not wish for as to
            provide aggregated, anonymised information to our trusted business partners, please find out how to opt out
            of our use of analytics cookies in the cookies section below. We use this information to administer the
            Service and for our internal operations including troubleshooting, data analysis, testing, research,
            statistical and survey purposes, to improve the Service, how it is presented and its safety and security.
            Please note that the Service requires access to your devices€TMs photograph storage application in order to
            store the completed videos, but we do not take any information, videos, photos or other content from your
            devices photograph storage application. </p>
          <p><span class="font-weight-bold text-dark">• Log information:</span> When you use the Service, we may
            automatically collect and store the following information in server logs: Internet protocol (IP) addresses,
            Internet service provider (ISP), clickstream data, browser type and language, viewed and exit pages and date
            or time stamps. We use this information to communicate with the Service and to better understand our users'
            operating systems, for system administration and to audit the use of the Service. We do not use this data to
            identify the name, address or other personal details of any individual. </p>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection