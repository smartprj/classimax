@extends('layouts.admin')
@section('content')



    <div class="content-wrapper">
        <div class="page-header">
          <h3 class="page-title"> Issue </h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Issue</li>
              <li class="breadcrumb-item active" aria-current="page">Edit Issue</li>
            </ol>
          </nav>
        </div>

        @if ($errors->any())
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
          <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    
                    <form action="{{ route('issue.update',$enquiry->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                      <div class="form-group">
                        <h3>Details</h3>
                        <table class="table table-hover">
                            <tr>
                              <th>User Name</th>
                              <td>{{ $enquiry->name??"-" }}</td>

                              <th>Contact Email</th>
                              <td>{{ $enquiry->email??"-" }}</td>
                            </tr>
                          {{--   <tr>
                              <th>Contact Number</th>
                              <td>{{ $enquiry->contact_number??"-" }}</td>

                              <th>Source Address</th>
                              <td>{{ $enquiry->source_address??"-" }}</td>
                            </tr> --}}
                            <tr>
                              <th>Type</th>
                              <td>{{ strtoupper($enquiry->type)??"-" }}</td>

                              <th>Remark</th>
                              <td>{{ $enquiry->issue??"-" }}</td>
                            </tr>
                           
                        </table>
                      </div>

                      <!-- enquiry listing categories START -->
                     
                      <!-- enquiry listing categories END -->

                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="exampleInputName1"><span style="color: red;">*</span> Remark</label>
                              <textarea type="text" class="form-control" id="exampleInputName1"name="remark">{{$enquiry->remark??''}}</textarea>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                            <label for="exampleSelectstatus"><span style="color: red;">*</span> Status</label>
                            <select class="form-control" id="exampleSelectstatus" required="required" name="status">
                              @foreach($status as $key => $val)
                                <option value="{{$key}}" <?= ($enquiry->status == $key)?"Selected":""?>>{{$val}}</option>
                              @endforeach
                            </select>
                          </div>
                          </div>

                        </div>
                        
                      </div>

                      
                      
                      <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                      <a href="{{route('issue.index')}}" class="btn btn-light">Cancel</a>
                    </form>
                </div>
            </div>
          </div>

        </div>
    </div>
    
@endsection