@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
          <h3 class="page-title">Category List</h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Category List</li>
            </ol>
            
          </nav>
        </div>

        @if ($message = Session::get('success'))
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
          <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <p class="card-description"> <a class="btn btn-success btn-sm float-sm-right" href="{{ route('categories.create') }}"> Add Category</a></p>
                
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Image</th>
                      <th>Parent Category</th>
                      <th>Category</th>
                      <!-- <th>Home Listing</th> -->
                      <th>Status</th>
                      <th width="280px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($categories as $category)
                    <tr>
                      <td>{{ ++$i }}</td>
                      <td class="py-1">
                        @if(!empty($category->image))
                          <img src="{{ asset('images/category/'.$category->image) }}" alt="image" />
                        @else
                          -
                        @endif
                      </td>
                      <td>{{ $catListing[$category->parent_id]??"" }}</td>
                      <td>{{ $category->name }}</td>
                      <!-- <td>
                        @if(empty($category->parent_id))
                        <div class="form-check">
                          <label class="form-check-label" style="display: inline-table;">
                          <input type="checkbox" name="home_dispaly" onchange="changeListing({{$category->id}})" class="form-check-input" <?= ($category->home_dispaly == 1)?"Checked":""?>> </label>
                        </div>
                        @endif
                      </td> -->
                      <td>
                        @if($category->status == 1)
                            <label class="badge badge-success">Active</label>
                        @else
                            <label class="badge badge-danger">In-Active</label>
                        @endif
                      </td>
                      <td>
                        <form action="{{ route('categories.destroy',$category->id) }}" method="POST">
                            <a class="btn btn-primary btn-xs" href="{{ route('categories.edit',$category->id) }}">Edit</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-xs">Delete</button>
                        </form>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {!! $categories->links() !!}
              </div>
            </div>
          </div>
        </div>
    </div>
    <script>
      function changeListing(id){
          $.ajax({
              url: '{{route("categorylistingstatus")}}',
              data: { id: id},
              type: "GET",
              success: function (data) {
                  console.log('Status Updated Succesfully');
              }
         });
      }
    </script>    
@endsection