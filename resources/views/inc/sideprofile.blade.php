      <div class="col-lg-3">
        <div class="sidebar">
          <!-- User Widget -->
          <div class="widget user-dashboard-profile">
            <!-- User Image -->
              @php
           
            $auth = Auth::user();
    //         echo "<pre>";
    //         print_r($auth);
    // echo "</pre>";

      
                           @endphp
            <div class="profile-thumb">
         
              <img src="{{ asset('images/company_logo/'.$auth->company_logo) }}" alt="" class="rounded-circle">
            </div>

            <!-- User Name -->
          
            <h5 class="text-center">{{ $auth->name }}</h5>
            <h5 class="text-center text text-info userwallet" >{{ $auth->wallet }}</h5>
        
            
            <a href="" class="btn btn-main-sm">Purchase Coins</a>
          </div>
             <div class="widget user-dashboard-menu form-group">
                <input type='text' class="from-control amount" placeholder="Enter Amount">
              <button type="butoon" id="paymentclick" class="btn btn-success btn-sm">pay amount</button>

             </div>
             <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
             <script>
            $(document).ready(function(){
              $('#paymentclick').click(function(){
              $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
              });

              $.ajax({
                type:'POST',
                url:"{{ route('generateOrder') }}",
                data:{'amount':$('.amount').val()},
                dataType:'JSON',
                success:function(data){
                // console.log(data);
                // alert(data.type);
                if(data.type=='success'){
                  var options = {
                  "key": "{{ env('RAZOR_KEY') }}",
                  "amount": ($('.amount').val()*100), // Example: 2000 paise = INR 20
                  "name": "Classimax",
                  "description": "description",
                  "image": "img/logo.png",// COMPANY LOGO
                  "order_id": data.order_id,
                  "handler": function (response){
                  //             console.log(response);
                  //               alert(response.razorpay_payment_id);
                  // alert(response.razorpay_order_id);
                  // alert(response.razorpay_signature);
                  $.ajaxSetup({
                      headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                  });
                  $.ajax({
                      type:'POST',
                      url:"{{ route('payment') }}",
                      data:response,
                       dataType:'JSON',
                      success:function(data){
                        if(data.type=='success'){
                           $.notify(data.msg, "success");
                          window.location="{{ route('dashboard') }}";
                        }else{
                           $.notify(data.msg, "error");
                        }
                      }
                  });
              },


              "prefill": {
                  "name": "{{ Auth::guard('web')->user()->name }}", // pass customer name
                  "email": '{{ Auth::guard('web')->user()->email }}',// customer email
                  "contact": '{{ Auth::guard('web')->user()->phone_no }}' //customer phone no.
              },
              "notes": {
                  "address": "address" //customer address 
              },
              "theme": {
                  "color": "#15b8f3" // screen color
              }
          };


          var propay = new Razorpay(options);
          propay.on('payment.failed', function (response){


            $.ajaxSetup({

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                }

            });

            $.ajax({

                type:'POST',

                url:"{{ route('payment') }}",

                data:response,
                 dataType:'JSON',

                success:function(data){

                 
                     $.notify(data.msg, "error");
                 
                }

            });
                  // alert(response.error.code);
                  // alert(response.error.description);
                  // alert(response.error.source);
                  // alert(response.error.step);
                  // alert(response.error.reason);
                  // alert(response.error.metadata.order_id);
                  // alert(response.error.metadata.payment_id);
          });
          propay.open();
          }else{
           $.notify(data.msg, "error");
          }

        }

    });

    });


   });
   </script>
          <!-- Dashboard Links -->
          <div class="widget user-dashboard-menu">
            <ul>
              <li class="{{ Route::is('myProfile') || Route::is('editMyProfile')?'active':''}}">
                <a href="{{ route('myProfile') }}"><i class="fa fa-user"></i> My Profile</a>

              </li>
              <li class="{{ Route::is('dashboard')?'active':''}}">
                <a href="{{ route('dashboard') }}"><i class="fa fa-bullhorn"></i> Leads</a>

              </li>
           {{--    <li><a href="dashboard-favourite-ads.html"><i class="fa fa-bookmark-o"></i>My Plans
                  </a></li>
 --}}           {{--    <li><a href="dashboard-archived-ads.html"><i class="fa fa-file-archive-o"></i>Special Offers
                  </a></li> --}}

              <li class="{{ Route::is('myLeads') || Route::is('myLeadsDetail')?'active':''}}"><a href="{{ route('myLeads') }}"><i class="fa fa-bolt"></i> My Leads<span></span></a>
              </li>
              <li class="{{ Route::is('myPurchase')?'active':''}}"><a href="{{ route('myPurchase') }}"><i class="fa fa-shopping-cart"></i> My Purchase<span></span></a>
              </li>

              {{-- <li><a href="dashboard-pending-ads.html"><i class="fa fa-bolt"></i> My Payments<span></span></a> --}}
              </li>
              <li><a href="{{ route('home')}}"><i class="fa fa-cog"></i> Logout</a></li>
              {{-- <li><a href="#!" data-toggle="modal" data-target="#deleteaccount"><i class="fa fa-power-off"></i>Delete Account</a></li> --}}
            </ul>
          </div>
          
          <!-- delete-account modal -->
          <!-- delete account popup modal start-->
          <!-- Modal -->
          <div class="modal fade" id="deleteaccount" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header border-bottom-0">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body text-center">
                  <img src="images/account/Account1.png" class="img-fluid mb-2" alt="">
                  <h6 class="py-2">Are you sure you want to delete your account?</h6>
                  <p>Do you really want to delete these records? This process cannot be undone.</p>
                  <textarea class="form-control" name="message" id="" cols="40" rows="4" class="w-100 rounded"></textarea>
                </div>
                <div class="modal-footer border-top-0 mb-3 mx-5 justify-content-center">
                  <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                  <button type="button" class="btn btn-danger">Delete</button>
                </div>
              </div>
            </div>
          </div>
          <!-- delete account popup modal end-->
          <!-- delete-account modal -->

        </div>
      </div>