<header>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<nav class="navbar navbar-expand-lg navbar-light navigation">
					<a class="navbar-brand" href="{{ route('home')}}">
						<img src="{{ asset('images/logo.png') }}" alt="">
					</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					 aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav ml-auto main-nav ">
							<li class="nav-item active">
								<a class="nav-link" href="{{ route('home')}}">Home</a>
							</li>
							<li class="nav-item ">
								<a class="nav-link" href="{{ route('aboutus')}}">About Us</a>
							</li>
							<li class="nav-item ">
								<a class="nav-link" href="{{ route('location')}}">Location</a>
							</li>
								<li class="nav-item ">
								<a class="nav-link" href="{{ route('enquiry')}}">Enquiry</a>
							</li>
							{{-- <li class="nav-item dropdown dropdown-slide @@dashboard">
								<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#!">Dashboard<span><i class="fa fa-angle-down"></i></span>
								</a>

								<!-- Dropdown list -->
								<ul class="dropdown-menu">
									<li><a class="dropdown-item @@dashboardPage" href="{{ route('dashboard')}}">Dashboard</a></li>
									<li><a class="dropdown-item @@dashboardMyAds" href="{{ route('dashboard')}}">Dashboard My Ads</a></li>
									<li><a class="dropdown-item @@dashboardFavouriteAds" href="{{ route('dashboard')}}">Dashboard Favourite Ads</a></li>
									<li><a class="dropdown-item @@dashboardArchivedAds" href="{{ route('dashboard')}}">Dashboard Archived Ads</a></li>
									<li><a class="dropdown-item @@dashboardPendingAds" href="{{ route('dashboard')}}">Dashboard Pending Ads</a></li>
									
									<li class="dropdown dropdown-submenu dropright">
										<a class="dropdown-item dropdown-toggle" href="#!" id="dropdown0501" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sub Menu</a>
					
										<ul class="dropdown-menu" aria-labelledby="dropdown0501">
											<li><a class="dropdown-item" href="{{ route('home')}}">Submenu 01</a></li>
											<li><a class="dropdown-item" href="{{ route('home')}}">Submenu 02</a></li>
										</ul>
									</li>
								</ul>
							</li> --}}
							{{-- <li class="nav-item dropdown dropdown-slide @@pages">
								<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Pages <span><i class="fa fa-angle-down"></i></span>
								</a>
								<!-- Dropdown list -->
								<ul class="dropdown-menu">
									<li><a class="dropdown-item @@about" href="{{ route('aboutus')}}">About Us</a></li>
									<li><a class="dropdown-item @@contact" href="{{ route('contactus')}}">Contact Us</a></li>
									<li><a class="dropdown-item @@profile" href="{{ route('profile')}}">User Profile</a></li>
									<li><a class="dropdown-item @@404" href="{{ route('error')}}">404 Page</a></li>
									<li><a class="dropdown-item @@package" href="{{ route('package')}}">Package</a></li>
									<li><a class="dropdown-item @@singlePage" href="{{ route('deta')}}">Single Page</a></li>
									<li><a class="dropdown-item @@store" href="{{ route('storesingle')}}">Store Single</a></li>
									<li><a class="dropdown-item @@blog" href="{{ route('blog')}}">Blog</a></li>
									<li><a class="dropdown-item @@singleBlog" href="{{ route('blogdetails')}}">Blog Details</a></li>
									<li><a class="dropdown-item @@terms" href="{{ route('termsconditions')}}">Terms &amp; Conditions</a></li>
								</ul>
							</li> --}}
							{{-- <li class="nav-item dropdown dropdown-slide @@listing">
								<a class="nav-link dropdown-toggle" href="#!" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Listing <span><i class="fa fa-angle-down"></i></span>
								</a>
								<!-- Dropdown list -->
								<ul class="dropdown-menu">
									<li><a class="dropdown-item @@category" href="{{ route('category')}}">Ad-Gird View</a></li>
									<li><a class="dropdown-item @@listView" href="{{ route('categorylist')}}">Ad-List View</a></li>
									
									<li class="dropdown dropdown-submenu dropleft">
										<a class="dropdown-item dropdown-toggle" href="#!" id="dropdown0201" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sub Menu</a>
					
										<ul class="dropdown-menu" aria-labelledby="dropdown0201">
											<li><a class="dropdown-item" href="{{ route('home')}}">Submenu 01</a></li>
											<li><a class="dropdown-item" href="{{ route('home')}}">Submenu 02</a></li>
										</ul>
									</li>
								</ul>
							</li> --}}
						</ul>
						<ul class="navbar-nav ml-auto mt-10">
							<li class="nav-item">
								@auth
								   <a class="nav-link login-button" href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
						            <i class="mdi mdi-logout mr-2 text-primary"></i> Signout </a>
						            <form id="logout-form" action="{{ route('web.logout') }}" method="POST" class="d-none">
						                @csrf
						            </form>
								@endauth
								@guest
								   <a class="nav-link login-button" href="{{ route('weblogin')}}">Login</a>
								@endguest
								
							</li>
							<li class="nav-item">
								@auth
								  						<a class="nav-link text-white add-button" href="{{ route('dashboard')}}"> Dashboard</a>
								@endauth
								@guest
								 						<a class="nav-link text-white add-button" href="{{ route('enquiry')}}"><i class="fa fa-plus-circle"></i> Add Enquiry</a>
								@endguest
		
							</li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	</div>
</header>