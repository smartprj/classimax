@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
          <h3 class="page-title">Plan</h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Plan</li>
            </ol>
            
          </nav>
        </div>

        @if ($message = Session::get('success'))
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
          <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <p class="card-description"> <a class="btn btn-success btn-sm float-sm-right" href="{{ route('plans.create') }}"> Add Plan</a></p>
                
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Name</th>
                      <th>Price</th>
                      <th>Discount Price</th>
                      <th>Validity</th>
                      <th>Premium</th>
                      <th>Status</th>
                      <th width="280px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($plans as $plan)
                    <tr>
                      <td>{{ ++$i }}</td>
                      <td>{{ $plan->name }}</td>
                      <td>{{ $plan->price }}</td>
                      <td>{{ $plan->discount_price }}</td>
                      <td>{{ $plan->validity }}</td>
                      <td>{{ $plan->is_premium }}</td>
                      <td>
                        @if($plan->status == 1)
                            <label class="badge badge-success">Active</label>
                        @else
                            <label class="badge badge-danger">In-Active</label>
                        @endif
                      </td>
                      <td>
                        <form action="{{ route('plans.destroy',$plan->id) }}" method="POST">
                            <a class="btn btn-primary btn-xs" href="{{ route('plans.edit',$plan->id) }}">Edit</a>
                            <a class="btn btn-info btn-xs" href="{{ route('plans.show',$plan->id) }}">View</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-xs">Delete</button>
                        </form>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {!! $plans->links() !!}
              </div>
            </div>
          </div>
        </div>
    </div>      
@endsection