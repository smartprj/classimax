@extends('layouts.admin')
@section('content')

    <div class="content-wrapper">
        <div class="page-header">
          <h3 class="page-title"> Add Plan </h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Plan</li>
              <li class="breadcrumb-item active" aria-current="page">Add Plan</li>
            </ol>
          </nav>
        </div>

        @if ($errors->any())
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
          <div class="col-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    
                    <form action="{{ route('plans.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                      <div class="form-group">
                        <label for="exampleInputName1"><span style="color: red;">*</span> Name </label>
                        <input type="text" class="form-control" id="exampleInputName1" required="required" placeholder="Name" name="name">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputName1"><span style="color: red;">*</span> Price </label>
                        <input type="number" class="form-control" id="exampleInputName1" required="required" placeholder="Original Price" name="price">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputName1"><span style="color: red;">*</span> Discount Price </label>
                        <input type="number" class="form-control" id="exampleInputName1" required="required" placeholder="Discount Price" name="discount_price">
                      </div>

                      <div class="form-group">
                        <label for="exampleSelectstatus"><span style="color: red;">*</span> Validity Type </label>
                        <select class="form-control" id="exampleSelectstatus" required="required" name="validity_type">
                          <option value="1">Select Validity Type</option>
                          @foreach($validityTypeDropDown as $vtKey => $vtDropdown)
                            <option value="{{$vtKey}}">{{$vtDropdown}}</option>
                          @endforeach
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputName1"><span style="color: red;">*</span> Number of validity </label>
                        <input type="number" class="form-control" id="exampleInputName1" required="required" placeholder="Validity" name="validity">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputName1"><span style="color: red;">*</span> Mobile Number </label>
                        <input type="number" class="form-control" id="exampleInputName1" required="required" placeholder="Enter Mobile Number" name="mobile_no">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputName1"> Alternate Contact Number </label>
                        <input type="number" class="form-control" id="exampleInputName1" placeholder="Enter Alternate Contact Number" name="alternate_no">
                      </div>

                      <div class="form-group">
                        <label> Upload Image</label>
                        <input type="file" name="listing_image" accept="image/*" class="file-upload-default">
                        <div class="input-group col-xs-12">
                          <input type="text" name="file" accept="image/*" class="form-control file-upload-info" disabled placeholder="Upload Image">
                          <span class="input-group-append">
                            <button name="sasa" class="file-upload-browse btn btn-gradient-primary" type="button">Upload</button>
                          </span>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="exampleTextarea1"> Description</label>
                        <textarea class="form-control" id="exampleTextarea1" rows="4" name="description"></textarea>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputName1"> Slug </label>
                        <input type="text" class="form-control" id="exampleInputName1"  placeholder="Slug" name="slug">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputName1">Meta Title </label>
                        <input type="text" class="form-control" id="exampleInputName1" placeholder="Meta Title" name="meta_title">
                      </div>

                      <div class="form-group">
                        <label for="exampleTextarea1">Meta Description</label>
                        <textarea class="form-control" id="exampleTextarea1" rows="4" name="meta_description"></textarea>
                      </div>
                      <div class="form-group">
                        <label for="number">Position </label>
                        <input type="number" class="form-control" id="number" placeholder="Position" name="meta_title">
                      </div>
                        <div class="form-group">
                        <label for="rating"> Rating </label>
                        <select class="form-control" name="rating">

                          @for ($i=5; $i>0 ;$i--)
                          <option value="{{ $i }}">{{ $i }}</option>
                          @endfor
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleSelectstatus"><span style="color: red;">*</span> Status </label>
                        <select class="form-control" id="exampleSelectstatus" required="required" name="status">
                          <option value="1">Active</option>
                          <option value="0">Inactive</option>
                        </select>
                      </div>
                      
                      <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                      <a href="{{route('listings.index')}}" class="btn btn-light">Cancel</a>
                    </form>
                </div>
            </div>
          </div>

        </div>
    </div>
@endsection