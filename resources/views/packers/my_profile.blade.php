@extends('layouts.web')

@section('content')

<section class="dashboard section">
  <!-- Container Start -->
  <div class="container">
    <!-- Row Start -->
    <div class="row">
      @include('inc.sideprofile')
      <div class="col-lg-9">
        @if ($message = Session::get('success'))
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                </div>
            </div>
        @endif
        <!-- Recently Favorited -->
        <div class="widget dashboard-container my-adslist">
          <h3 class="widget-header">My Profile</h3>
          <table class="table table-responsive product-dashboard-table">
            <thead>
              <tr>
                <th>User Details</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="product-details" style="width: 100%;">
                  <span class="add-id"><strong>Name :</strong>{{ $user->name }}</span>
                  <br>
                  <span class="add-id"><strong>Email ID:</strong> {{ $user->email??"-" }}</span>
                  <br>
                  <span class="add-id"><strong>Contact No:</strong> {{ $user->phone_no??"-" }} </span>
                  <br>
                  <span class="add-id"><strong>Address:</strong> {{ $user->address??"-" }} </span>
                  <br>
                  <span class="add-id"><strong>Pin Code:</strong> {{ $user->pin_code??"-" }} </span>
                  <br>
                  <span class="add-id"><strong>Location:</strong> {{ $disName??"-" }} </span>
                  <br>
                  <span class="add-id"><strong>Wallet:</strong> {{ $user->wallet??"-" }} </span>
                  <br>
                  <span class="add-id"><strong>Total Purchase Lead:</strong> {{ $user->total_purchase_leads??"-" }} </span>
                  <br>
                  <span class="add-id"><strong>GST No:</strong> {{ $user->gst_no??"-" }}</span>
                  <br>
                  <span class="add-id"><strong>Type:</strong> {{ $userType[$user->type]??"-" }} </span>
                  <br>
                  <span class="add-id"><strong>Rating:</strong> {{ $rating[$user->rating]??"-" }} </span>
                  <br>
                  <span class="add-id"><strong>Status:</strong> {{ $status[$user->status]??"-" }} </span>
                  <br>
                </td>
                <td class="product-category">
                  <br>
                  <a href="{{ route('editMyProfile') }}" class="btn  btn-outline-info btn-sm">Edit</a>

                </td>
               
                
              </tr>
            
            </tbody>
          </table>

        </div>

        <!-- pagination -->
        <div class="pagination justify-content-center">
          <nav aria-label="Page navigation example">
          </nav>
        </div>
        <!-- pagination -->

      </div>
    </div>
    <!-- Row End -->
  </div>
  <!-- Container End -->
</section>
@endsection