@extends('layouts.web')

@section('content')

<section class="dashboard section">
  <!-- Container Start -->
  <div class="container">
    <!-- Row Start -->
    <div class="row">
          @include('inc.sideprofile')


      <div class="col-lg-9">
   
          <h3 class="widget-header">Purchase History</h3>
          <table class="table table-responsive">
            <thead>
              <tr>
                {{-- <th>Image</th> --}}
                <th class="text-center">Reciept No.</th>
                <th class="text-center">Order Id</th>
                <th class="text-center">Payment Id</th>
                <th class="text-center">Amount</th>
                <th class="text-center">Status</th>
                <th class="text-center">Date</th>
              </tr>
            </thead>
            <tbody>
              @foreach($orders as $order)


              @php


             $tblrow = "table-warning";
             if($order->status==1){

                 $tblrow = "table-success";
             }else  if($order->status==2){
                 $tblrow = "table-danger";

             }
              @endphp
              <tr class="{{ $tblrow }}">
                {{-- <td class="product-thumb">
                  <img width="80px" height="auto" src="images/products/products-1.jpg" alt="image description"></td> --}}
                <td >
               {{ $order->recipt_no }}
                </td>
                <td >
               {{ $order->order_id }}
                </td>
                     <td >
               {{ $order->razorpay_payment_id    }}
                </td>
                 <td >
               {{ $order->amount }}
                </td>
                  <td >

                    @php
                    $text = "Pending";
                    $class = 'label label-warning';
                        if($order->status==1){
                              $class = 'label label-success';
                                $text = "Success";
                        }else if($order->status==2){
                           $class = 'label label-danger';
                              $text = "Rejected";
                        }
                    @endphp
                    <label class="{{  $class }}">{{  $text }}</label>
              
                </td>
                 <td >
                  {{ date('d M Y H:i a',strtotime($order->created_at)) }}
                 </td>
                
              </tr>
              @endforeach
            
            </tbody>
          </table>

      

        <!-- pagination -->
        <div class="pagination justify-content-center">
          <nav aria-label="Page navigation example">
           {{ $orders->links() }}
          </nav>
        </div>
        <!-- pagination -->

      </div>
    </div>
    <!-- Row End -->
  </div>
  <!-- Container End -->
</section>
 <script>

      function changeRegisteredpackerStatus(curr){
        var curr = $(curr);
          $.ajax({
             url: '{{ route('leadbuy') }}',
              data: { lead: curr.data('id')},
              type: "GET",
              dataType:'JSON',
              success: function (data) {

                if(data.type=='fail'){
                  $.notify(data.msg, "warn");
                }else{
              
                    $.notify(data.msg, "success");
                   curr.attr('class','btn btn-success');
                 curr.html('Purchased');
                 getUserWallet();

                }
                 
              }
         });
      }
      function getUserWallet(){
          $.ajax({
             url: '{{ route('wallet') }}',
             
              type: "GET",
              dataType:'JSON',
              success: function (data) {

                if(data.type=='success'){
                  $('.userwallet').text(data.amount);
                }
                 
              }
         });
      }
    </script>  
@endsection