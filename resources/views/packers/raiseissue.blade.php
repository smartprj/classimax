@extends('layouts.web')

@section('content')

<section class="dashboard section">
  <!-- Container Start -->
  <div class="container">
    <!-- Row Start -->
    <div class="row">
      @include('inc.sideprofile')
      <div class="col-lg-9">
        <form action="{{ url()->current() }}" method="post">
          @csrf
      <fieldset class="border border-gary px-3 px-md-4 py-4 mb-5">
        <div class="row">
          <div class="col-lg-12">
            <h3>Raise Issue</h3>
          </div>
          <div class="col-lg-6">
        @if ($message = Session::get('error'))
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-danger">
                        <p>{{ $message }}</p>
                    </div>
                </div>
            </div>
        @endif

        
            <div class="row px-3">
              <div class="col-lg-4 mr-lg-4 my-2 pt-2 pb-1 rounded bg-white">
                <input type="radio" name="type" value="refund"  required="">
                <label for="personal" class="py-2">Refund</label>
              </div>
              <div class="col-lg-4 mr-lg-4 my-2 pt-2 pb-1 rounded bg-white ">
                <input type="radio" name="type" value="other" required="">
                <label for="business" class="py-2">Other </label>
              </div>
            </div>
            <h6 class="font-weight-bold pt-4 pb-1">Description:</h6>
            <textarea name="issue" id="" class="form-control bg-white" rows="7" placeholder="Write issue here" required=""></textarea>
            <br>
            <input type="submit" class="btn btn-info btn-sm" value='Raise Issue'>
          </div>

        </div>
      </fieldset>
    </form>

      </div>
    </div>
    <!-- Row End -->
  </div>
  <!-- Container End -->
</section>
 <script>

      function changeRegisteredpackerStatus(curr){
        var curr = $(curr);
          $.ajax({
             url: '{{ route('leadbuy') }}',
              data: { lead: curr.data('id')},
              type: "GET",
              dataType:'JSON',
              success: function (data) {

                if(data.type=='fail'){
                  $.notify(data.msg, "warn");
                }else{
              
                    $.notify(data.msg, "success");
                   curr.attr('class','btn btn-success');
                 curr.html('Purchased');
                 getUserWallet();

                }
                 
              }
         });
      }
      function getUserWallet(){
          $.ajax({
             url: '{{ route('wallet') }}',
             
              type: "GET",
              dataType:'JSON',
              success: function (data) {

                if(data.type=='success'){
                  $('.userwallet').text(data.amount);
                }
                 
              }
         });
      }
    </script>  
@endsection