@extends('layouts.web')

@section('content')

<section class="dashboard section">
  <!-- Container Start -->
  <div class="container">
    <!-- Row Start -->
    <div class="row">
      @include('inc.sideprofile')
      <div class="col-lg-9">
        <!-- Recently Favorited -->
        <div class="widget dashboard-container my-adslist">
          <h3 class="widget-header">My Profile</h3>
          <form action="{{ route('saveProfileRequest') }}" method="post" >
          @csrf
          @method('PUT')
            <div class="row border border-gary px-3 px-md-4 py-4 mb-5">
              <div class="col-lg-12">
                <h3>Edit Profile</h3>
              </div>
              <div class="col-lg-6">
                <h6 class="font-weight-bold pt-4 pb-1">Name:</h6>
                <input type="text" name="name" value="{{ $user->name }}" placeholder="Enter name" class="form-control bg-white" required="">
                <h6 class="font-weight-bold pt-4 pb-1">Number:</h6>
                <input type="text" name="phone_no" value="{{ $user->phone_no }}" placeholder="Enter Number" class="form-control bg-white" required="">
              </div>
              <div class="col-lg-6">
                <h6 class="font-weight-bold pt-4 pb-1">Email:</h6>
                <input type="email" name="email"  value="{{ $user->email }}" readonly="readonly" disabled="disabled" placeholder="Enter Email" class="form-control" required="">
                <h6 class="font-weight-bold pt-4 pb-1">GST Number:</h6>
                <input type="text" name="gst_no" value="{{ $user->gst_no }}" placeholder="Enter GST Number" class="form-control bg-white">
              </div>
            </div>

            <div class="row border border-gary px-3 px-md-4 py-4 mb-5">
              <div class="col-md-12 pam-form-from-left">
                <div class="space-for-checbox">
                  <span>Address</span>
                </div>
                <div formgroupname="from" class="ng-touched ng-dirty ng-invalid">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                      <textarea class="form-control light-placeholder ta-ver-resize ng-untouched ng-pristine ng-invalid" name="address"  placeholder="Enter Current Address" rows="2">{{ $user->address }}</textarea>
                        <!---->
                      </div>
                    </div>
                  </div>

                  <!---->

                  <div class="row">
                    <div class="col-xs-7 col-sm-8">
                      <div class="form-group">
                        <p class="form-label">Location<!----></p>
                        <input class="form-control location" name="" value="{{ $disName }}" placeholder="Area/Locality" type="text">
                          <input type="hidden" class="locationhidden" name="location" value="{{ $user->location }}">
                        <!---->
                      </div>
                    </div>

                    <div class="col-xs-5 col-sm-4">
                      <div class="form-group">
                        <p class="form-label">Pin Code *</p>
                        <input class="form-control " name="pin_code" value="{{ $user->pin_code }}" maxlength="6" placeholder="Pin Code" type="text">
                        <!---->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-3 offset-md-9">
                <input type="submit" class="btn btn-success pull-right"></input>
              </div>
            </div>

          </form>
        </div>


      </div>
    </div>
    <!-- Row End -->
  </div>
  <!-- Container End -->
</section> 
<script type="text/javascript">
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  $(document).ready(function(){

    $( ".location" ).autocomplete({
      source: function( request, response ) {
        $.ajax({
          url:"{{route('getLocations')}}",
          type: 'post',
          dataType: "json",
          data: {
             _token: CSRF_TOKEN,
             search: request.term
          },
          success: function( data ) {
             response( data );
          }
        });
      },
      select: function (event, ui) {

         $(this).val(ui.item.label);
         // $('#location').val(ui.item.value);
        $(this).closest('.form-group').find('.locationhidden').val(ui.item.value);
         return false;
      }
    });

  });
</script>
@endsection