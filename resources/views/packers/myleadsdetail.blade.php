@extends('layouts.web')

@section('content')

<section class="dashboard section">
  <!-- Container Start -->
  <div class="container">
    <!-- Row Start -->
    <div class="row">
      @include('inc.sideprofile')
      <div class="col-lg-9">
        <!-- Recently Favorited -->
        <div class="widget dashboard-container my-adslist">
          <h3 class="widget-header">Enquiry Detail</h3>
          <table class="table table-responsive product-dashboard-table">

            <thead>
              <tr>
                <th>Contact Details</th>
                <th class="text-center">Product Titles</th>
              </tr>

            </thead>

            <tbody>
              <tr>
                <td class="product-details">
                  <span class="add-id"><strong>Name :</strong>{{ $listing->contact_name }}</span>
                  <span class="add-id"><strong>Email ID:</strong> {{ $listing->contact_email }}</span>
                  <span class="add-id"><strong>Contact No:</strong> {{ $listing->contact_number }} </span>
                  <span><strong>Posted on: </strong>{{ date('d M Y',strtotime($listing['created'])) }}</span>
                  <span class="location"><strong>Location</strong>{{ $listing->callSourceLocation['name'] }} - {{ $listing->callDestinationLocation['name'] }}</span>
                </td>
                <td class="product-details" style="float: right;width: 360px">
                    @if(!empty($quantityArr))
                          @foreach($quantityArr as $cat => $catVal)
                          <span class="add-id"><strong>{{$categoriesArr[$cat]}} :</strong>{{ $catVal??"-" }}</span>
                          @endforeach
                    @endif
                </td>
              </tr>
            </tbody>


            <thead>
              <tr>
                <th>Source Details</th>
                <th class="text-center">Destination Details</th>
              </tr>

            </thead>
            <tbody>
              <tr>
                <td class="product-details" style="border-right: 1px">
                  <span class="add-id"><strong>Address :</strong>{{ $listing->source_address??"-" }}</span>
                  <span class="add-id"><strong>Location:</strong> {{ $listing->callSourceLocation['name']??"-" }}</span>
                  <span class="add-id"><strong>PIN:</strong> {{ $listing->source_pin??"-" }} </span>
                  <span ><strong>House Type: </strong>{{ $listing->source_house_type??"-" }}</span>
                  <span ><strong>Floor: </strong>{{ $listing->source_floor??"-" }}</span>
                  <span ><strong>Lift: </strong>{{ $listing->source_lift??"-" }}</span>
                </td>
                <td class="product-details" style="float: right;width: 360px">
                  <span class="add-id"><strong>Address :</strong>{{ $listing->destination_address??"-" }}</span>
                  <span class="add-id"><strong>Location:</strong> {{ $listing->callDestinationLocation['name']??"-" }}</span>
                  <span class="add-id"><strong>PIN:</strong> {{ $listing->destination_pin??"-" }} </span>
                  <span><strong>House Type: </strong>{{ $listing->destination_house_type??"-" }}</span>
                  <span><strong>Floor: </strong>{{ $listing->destination_lift??"-" }}</span>
                  <span><strong>Lift: </strong>{{ $listing->max_purchase_count??"-" }}</span>
                </td>
              </tr>
            </tbody>

          </table>

        </div>

        <!-- pagination -->
        <div class="pagination justify-content-center">
          <nav aria-label="Page navigation example">
          </nav>
        </div>
        <!-- pagination -->

      </div>
    </div>
    <!-- Row End -->
  </div>
  <!-- Container End -->
</section>
@endsection