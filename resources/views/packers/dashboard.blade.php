@extends('layouts.web')

@section('content')

<section class="dashboard section">
  <!-- Container Start -->
  <div class="container">
    <!-- Row Start -->
    <div class="row">
      @include('inc.sideprofile')
      <div class="col-lg-9">
        <!-- Recently Favorited -->
        
        @if ($message = Session::get('success'))
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                </div>
            </div>
        @endif

        <div class="widget dashboard-container my-adslist">
          <h3 class="widget-header">Leads</h3>
          <table class="table table-responsive product-dashboard-table">
            <thead>
              <tr>
                {{-- <th>Image</th> --}}
                <th>Product Title</th>
                <th class="text-center">Category</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($listings as $listing)


              @php

             $leadid = base64_encode(base64_encode($listing->id))
              @endphp
              <tr>
                {{-- <td class="product-thumb">
                  <img width="80px" height="auto" src="images/products/products-1.jpg" alt="image description"></td> --}}
                <td class="product-details">
                  <span class="add-id"><strong>Name :</strong>{{ $listing->contact_name }}</span>
                  <span class="add-id"><strong>Email ID:</strong> XXXXXXX@XXX.com</span>
                  <span class="add-id"><strong>Contact No:</strong> XXXXXX </span>
                  <span><strong>Posted on: </strong>{{ date('d M Y',strtotime($listing['created'])) }}</span>
                  {{-- <span class="status active"><strong>Status</strong>Active</span> --}}
                  <span class="location"><strong>Location</strong>{{ $listing->callSourceLocation['name'] }},{{ $listing->callDestinationLocation['name'] }}</span>
                </td>
                <td class="product-category">

                  <span class="categories">{{ $listing['source_house_type'] }}</span>

                </td>
                <td class="product-category">

                  <span class="status active"><strong >{{ $listing['amount'] }}</strong></span>
                  <br>
                  <a href="javascript:void(0);" data-id = "{{ $leadid }}" onclick='changeRegisteredpackerStatus(this)' class="btn  btn-outline-info btn-sm">Purchase</a>

                </td>
               
                
              </tr>
              @endforeach
            
            </tbody>
          </table>

        </div>

        <!-- pagination -->
        <div class="pagination justify-content-center">
          <nav aria-label="Page navigation example">
           {{ $listings->links() }}
          </nav>
        </div>
        <!-- pagination -->

      </div>
    </div>
    <!-- Row End -->
  </div>
  <!-- Container End -->
</section>
 <script>

      function changeRegisteredpackerStatus(curr){
        var curr = $(curr);
          $.ajax({
             url: '{{ route('leadbuy') }}',
              data: { lead: curr.data('id')},
              type: "GET",
              dataType:'JSON',
              success: function (data) {

                if(data.type=='fail'){
                  $.notify(data.msg, "warn");
                }else{
              
                    $.notify(data.msg, "success");
                   curr.attr('class','btn btn-success');
                 curr.html('Purchased');
                 getUserWallet();

                }
                 
              }
         });
      }
      function getUserWallet(){
          $.ajax({
             url: '{{ route('wallet') }}',
             
              type: "GET",
              dataType:'JSON',
              success: function (data) {

                if(data.type=='success'){
                  $('.userwallet').text(data.amount);
                }
                 
              }
         });
      }
    </script>  
@endsection