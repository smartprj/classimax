<style>
.tbl-2 {
  border-collapse: collapse;
  border-spacing: 0;
  width: 50%;
  border: 1px solid #ddd;
}
.tbl-2 th, td {
  text-align: left;
  padding: 8px;
}
.tbl-2 th::after {
    content: ":";
    display: flex;
    margin-left: 95px;
    padding: 0px 14px;
    margin-top: -17px;
}
.tbl-2 tr:nth-child(even){background-color: #f2f2f2}
</style>
</head>
 <body>
  <p><h4>Dear {{$data['name']??""}},</h4></p>
    Your registration request has been approved by {{ config('app.name') }} Team.
  <br>
  <br>
  <table>
    <tr>
      <td><b>Name</b> </td>
      <td>: {{$data['name']??""}}</td>
    </tr>
    <tr>
      <td><b>Email</b> </td>
      <td>: {{$data['email']??""}}</td>
    </tr>
  </table>
<br>
<h4>Best regards</h4>
{{ config('app.name') }}