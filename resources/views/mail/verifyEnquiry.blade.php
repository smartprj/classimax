<style>
.tbl-2 {
  border-collapse: collapse;
  border-spacing: 0;
  width: 50%;
  border: 1px solid #ddd;
}
.tbl-2 th, td {
  text-align: left;
  padding: 8px;
}
.tbl-2 th::after {
    content: ":";
    display: flex;
    margin-left: 95px;
    padding: 0px 14px;
    margin-top: -17px;
}
.tbl-2 tr:nth-child(even){background-color: #f2f2f2}
</style>
</head>
 <body>
  <p><h4>Dear {{$data['contact_name']??""}},</h4></p>
    Welcome to {{ config('app.name') }}.
  <br>
  <br>
  <table>
    <tr>
      <td><b>Contact Name</b> </td>
      <td>: {{$data['contact_name']??""}}</td>
    </tr>
    <tr>
      <td><b>Contact Email</b> </td>
      <td>: {{$data['contact_email']??""}}</td>
    </tr>
    <tr>
      <td><b>Token</b> </td>
      <td>: {{$data['token']??""}}</td>
    </tr>
  </table>
<br>
<h4>Best regards</h4>
{{ config('app.name') }}