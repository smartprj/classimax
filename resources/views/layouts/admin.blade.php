<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ config('app.name', 'Calssimax') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('admininc.css')
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" />
  </head>
  <body>
    <div class="container-scroller">
      @include('admininc.header')
      <div class="container-fluid page-body-wrapper">
        @include('admininc.sidebar')
        <div class="main-panel">
          @yield('content')
          @include('admininc.footer')
        </div>
      </div>
    </div>
    @include('admininc.js')
  </body>
</html>