<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>{{ config('app.name', __('Classimax')) }}</title>
  <!-- ** Mobile Specific Metas ** -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="Agency HTML Template">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0">
  <meta name="author" content="Themefisher">
  <meta name="generator" content="Themefisher Classified Marketplace Template v1.0">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- favicon -->
  <link href="{{ asset('images/favicon.png') }}" rel="shortcut icon">
  @include('inc.css')
</head>
<body class="body-wrapper">
@include('inc.header')
@yield('content')
@include('inc.footer')
@include('inc.js')
</body>
</html>