@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
          <h3 class="page-title">Enquiry Form List</h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Enquiry Form List</li>
            </ol>
            
          </nav>
        </div>

        @if ($message = Session::get('success'))
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
          <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <p class="card-description"></p>
                
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Contact Name</th>
                      <th>Contact Email</th>
                      <th>Contact Number</th>
                      <th>Source</th>
                      <th>Designation</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($enquiries as $enquery)
                    <tr>
                      <td>{{ ++$i }}</td>
                      <td>{{ $enquery->contact_name }}</td>
                      <td>{{ $enquery->contact_email }}</td>
                      <td>{{ $enquery->contact_number }}</td>
                      <td>{{ $enquery->callSourceLocation['name'] }} </td>
                      <td>{{ $enquery->callDestinationLocation['name'] }}</td>
                      <td>
                        <label class="badge badge-{{$statusColor[$enquery->status]}}">{{$status[$enquery->status]}}</label>
                      </td>
                      <td>
                        <form action="{{ route('enquiries.destroy',$enquery->id) }}" method="POST">
                            <a class="btn btn-primary btn-xs" href="{{ route('enquiries.edit',$enquery->id) }}">Edit</a>
                            <a class="btn btn-info btn-xs" href="{{ route('enquiries.show',$enquery->id) }}">View</a>
                            @csrf
                            @method('DELETE')
                            <!-- <button type="submit" class="btn btn-danger btn-xs">Delete</button> -->
                        </form>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {!! $enquiries->links() !!}
              </div>
            </div>
          </div>
        </div>
    </div>      
@endsection