@extends('layouts.admin')
@section('content')



    <div class="content-wrapper">
        <div class="page-header">
          <h3 class="page-title"> Enquiry </h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Enquiry</li>
              <li class="breadcrumb-item active" aria-current="page">Edit Enquiry</li>
            </ol>
          </nav>
        </div>

        @if ($errors->any())
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
          <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    
                    <form action="{{ route('enquiries.update',$enquiry->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                      <div class="form-group">
                        <h3>Details</h3>
                        <table class="table table-hover">
                            <tr>
                              <th>Contact Name</th>
                              <td>{{ $enquiry->contact_name??"-" }}</td>

                              <th>Contact Email</th>
                              <td>{{ $enquiry->contact_email??"-" }}</td>
                            </tr>
                            <tr>
                              <th>Contact Number</th>
                              <td>{{ $enquiry->contact_number??"-" }}</td>

                              <th>Source Address</th>
                              <td>{{ $enquiry->source_address??"-" }}</td>
                            </tr>
                            <tr>
                              <th>Source Location</th>
                              <td>{{ $enquiry->callSourceLocation['name']??"-" }}</td>

                              <th>Source PIN</th>
                              <td>{{ $enquiry->source_pin??"-" }}</td>
                            </tr>
                            <tr>
                              <th>Source House Type</th>
                              <td>{{ $enquiry->source_house_type??"-" }}</td>

                              <th>Source Floor</th>
                              <td>{{ $enquiry->source_floor??"-" }}</td>
                            </tr>
                            <tr>
                              <th>Source Lift</th>
                              <td>{{ $enquiry->source_lift??"-" }}</td>

                              <th>Designation Address</th>
                              <td>{{ $enquiry->destination_address??"-" }}</td>
                            </tr>
                            <tr>
                              <th>Designation Location</th>
                              <td>{{ $enquiry->callDestinationLocation['name']??"-" }}</td>

                              <th>Designation PIN</th>
                              <td>{{ $enquiry->destination_pin??"-" }}</td>
                            </tr>
                            <tr>
                              <th>Designation House Type</th>
                              <td>{{ $enquiry->destination_house_type??"-" }}</td>

                              <th>Designation Lift</th>
                              <td>{{ $enquiry->destination_lift??"-" }}</td>
                            </tr>
                        </table>
                      </div>

                      <!-- enquiry listing categories START -->
                      @if(!empty($quantityArr))
                      <div class="form-group">
                        <h3>Cateories</h3>
                        <table class="table table-hover">
                            <tr>
                              <th>Category</th>
                              <th>Quantity</th>
                            </tr>
                            @foreach($quantityArr as $cat => $catVal)
                              <tr>
                                <td>{{$categoriesArr[$cat]}}</td>
                                <td>{{ $catVal??"-" }}</td>
                              </tr>
                              
                            @endforeach
                        </table>
                      </div>
                      <br>
                      <br>
                      @endif
                      <!-- enquiry listing categories END -->

                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="exampleInputName1"><span style="color: red;">*</span> Max Purchase Count </label>
                              <input type="number" class="form-control" id="exampleInputName1" value="{{$enquiry->max_purchase_count??''}}" required="required" placeholder="Enter Max Purchase Count" name="max_purchase_count">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                            <label for="exampleSelectstatus"><span style="color: red;">*</span> Status</label>
                            <select class="form-control" id="exampleSelectstatus" required="required" name="status">
                              @foreach($status as $key => $val)
                                <option value="{{$key}}" <?= ($enquiry->status == $key)?"Selected":""?>>{{$val}}</option>
                              @endforeach
                            </select>
                          </div>
                          </div>

                        </div>
                        
                      </div>

                      
                      
                      <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                      <a href="{{route('categories.index')}}" class="btn btn-light">Cancel</a>
                    </form>
                </div>
            </div>
          </div>

        </div>
    </div>
    
@endsection