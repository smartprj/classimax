@extends('layouts.admin')
@section('content')

    <div class="content-wrapper">
        <div class="page-header">
          <h3 class="page-title"> View Enquiry </h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Enquiry</li>
              <li class="breadcrumb-item active" aria-current="page">View Enquiry</li>
            </ol>
          </nav>
        </div>

        <div class="row">
          <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                  <!-- enquiry listing categories START -->
                  @if(!empty($quantityArr))
                  <div class="form-group">
                    <h3>Cateories</h3>
                    <table class="table table-hover">
                        <tr>
                          <th>Category</th>
                          <th>Quantity</th>
                        </tr>
                        @foreach($quantityArr as $cat => $catVal)
                          <tr>
                            <td>{{$categoriesArr[$cat]}}</td>
                            <td>{{ $catVal??"-" }}</td>
                          </tr>
                          
                        @endforeach
                    </table>
                  </div>
                  <br>
                  <br>
                  @endif
                  <!-- enquiry listing categories END -->

                  <div class="form-group">
                    <h3>Details</h3>
                    <table class="table table-hover">
                        <tr>
                          <th>Contact Name</th>
                          <td>{{ $enquiry->contact_name??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Contact Email</th>
                          <td>{{ $enquiry->contact_email??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Contact Number</th>
                          <td>{{ $enquiry->contact_number??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Source Address</th>
                          <td>{{ $enquiry->source_address??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Source Location</th>
                          <td>{{ $enquiry->callSourceLocation['name']??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Source PIN</th>
                          <td>{{ $enquiry->source_pin??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Source House Type</th>
                          <td>{{ $enquiry->source_house_type??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Source Floor</th>
                          <td>{{ $enquiry->source_floor??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Source Lift</th>
                          <td>{{ $enquiry->source_lift??"-" }}</td>
                        </tr>

                        <tr>
                          <th>Designation Address</th>
                          <td>{{ $enquiry->destination_address??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Designation Location</th>
                          <td>{{ $enquiry->callDestinationLocation['name']??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Designation PIN</th>
                          <td>{{ $enquiry->destination_pin??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Designation House Type</th>
                          <td>{{ $enquiry->destination_house_type??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Designation Lift</th>
                          <td>{{ $enquiry->destination_lift??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Max Purchase Count</th>
                          <td>{{ $enquiry->max_purchase_count??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Amount</th>
                          <td>{{ $enquiry->amount??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Available Count</th>
                          <td>{{ $enquiry->available_count??"-" }}</td>
                        </tr>
                        <tr>
                          <th>Status</th>
                          <td>
                            <label class="badge badge-{{$statusColor[$enquiry->status]}}">{{$status[$enquiry->status]}}</label>
                          </td>
                        </tr>
                        <tr>
                          <th colspan="2"><a href="{{route('enquiries.index')}}" class="btn btn-primary">Enquiry Listing</a></th>
                        </tr>
                    </table>
                  </div>

                </div>
            </div>
          </div>

        </div>
    </div>

@endsection