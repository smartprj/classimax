-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 07, 2021 at 10:21 PM
-- Server version: 8.0.23-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `classimax`
--

-- --------------------------------------------------------

--
-- Table structure for table `payment_history`
--

CREATE TABLE `payment_history` (
  `id` int NOT NULL,
  `order_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `currency` varchar(10) COLLATE utf8_unicode_ci DEFAULT 'INR',
  `status` int DEFAULT '0',
  `emp_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `razorpay_payment_id` text COLLATE utf8_unicode_ci,
  `razorpay_signature` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `recipt_no` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_coins` double DEFAULT '0',
  `user_last_wallet` double DEFAULT '0',
  `response` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment_history`
--

INSERT INTO `payment_history` (`id`, `order_id`, `amount`, `currency`, `status`, `emp_id`, `created_at`, `updated_at`, `razorpay_payment_id`, `razorpay_signature`, `recipt_no`, `payment_coins`, `user_last_wallet`, `response`) VALUES
(1, 'OD1001', 100, 'INR', 1, '2', '2021-04-03 00:44:00', '2021-04-03 00:44:00', NULL, NULL, NULL, 0, 0, NULL),
(4, NULL, NULL, 'INR', NULL, '2', '2021-04-03 09:35:47', '2021-04-03 09:35:47', NULL, NULL, NULL, 0, 0, NULL),
(5, 'order_GuQBB1SkbVnwHW', NULL, 'INR', 0, '2', '2021-04-03 10:50:27', NULL, NULL, NULL, 'RC23', 0, 0, NULL),
(6, 'order_GuT9NLk3WR7zjD', NULL, 'INR', 0, '2', '2021-04-03 13:44:48', NULL, NULL, NULL, 'RC24', 0, 0, NULL),
(7, 'order_GuT9Zg0w1Oh8kk', NULL, 'INR', 0, '2', '2021-04-03 13:45:00', NULL, NULL, NULL, 'RC25', 0, 0, NULL),
(8, 'order_GuTC4KVKZ64yYO', NULL, 'INR', 0, '2', '2021-04-03 13:47:22', NULL, NULL, NULL, 'RC26', 0, 0, NULL),
(9, 'order_GuTEMiafnifoYA', NULL, 'INR', 0, '2', '2021-04-03 13:49:32', NULL, NULL, NULL, 'RC27', 0, 0, NULL),
(10, 'order_GuTGLdp309LSpy', NULL, 'INR', 0, '2', '2021-04-03 13:51:25', NULL, NULL, NULL, 'RC28', 0, 0, NULL),
(11, 'order_GuTI53I9PROgB5', NULL, 'INR', 0, '2', '2021-04-03 13:53:03', NULL, NULL, NULL, 'RC29', 0, 0, NULL),
(12, 'order_GuTLU1jdOVWb0c', NULL, 'INR', 0, '2', '2021-04-03 13:56:17', NULL, NULL, NULL, 'RC210', 0, 0, NULL),
(13, 'order_GuTMEio66xWrS3', NULL, 'INR', 0, '2', '2021-04-03 13:56:59', NULL, NULL, NULL, 'RC211', 0, 0, NULL),
(14, 'order_GuUYWT51uGDhJ1', 100, 'INR', 0, '2', '2021-04-03 15:07:19', NULL, NULL, NULL, 'RC212', 0, 0, NULL),
(15, 'order_GuUaXu1hD69Rca', 100, 'INR', 0, '2', '2021-04-03 15:09:14', NULL, NULL, NULL, 'RC213', 0, 0, NULL),
(16, 'order_GuUcJ1vOLjksEl', 100, 'INR', 1, '2', '2021-04-03 15:10:54', '2021-04-03 15:31:24', 'pay_GuUcgFTUfFzdbl', '1025bb05d279435b0d78ac75ff3699c50ddf414fdf5d5e3e6a8ea01785410802', 'RC214', 10, 70, NULL),
(17, 'order_GuVDV7aKXyfomE', 100, 'INR', 1, '2', '2021-04-03 15:46:06', '2021-04-03 15:46:29', 'pay_GuVDnmcp1En5Bf', '4bbe1e29e18b147abb9114a7e1edbdc567ba4e4069161b3749c55eebaea21ee0', 'RC215', 10, 80, NULL),
(18, 'order_GuVEaq7Fbyepnx', 100, 'INR', 1, '2', '2021-04-03 15:47:09', '2021-04-03 15:47:29', 'pay_GuVEr3WxX1rr2J', 'd7545007657b01ef1ed7f41c396d39178acfd0ff60cc4cf86794a58da3c661ff', 'RC216', 10, 90, NULL),
(19, 'order_GuVFAfEm5Dy3KF', 100, 'INR', 1, '2', '2021-04-03 15:47:41', '2021-04-03 15:48:26', 'pay_GuVFpWYGDXFdlP', '086c7c378c6d460a02f5e0830ec033ae04aadc8185e984d19b0af318a45c9654', 'RC217', 10, 100, NULL),
(20, 'order_GuVKCm9I7RVpmt', 100, 'INR', 0, '2', '2021-04-03 15:52:27', NULL, NULL, NULL, 'RC218', 0, 0, NULL),
(21, 'order_GuVM2RTVpHSAc9', 100, 'INR', 0, '2', '2021-04-03 15:54:11', NULL, NULL, NULL, 'RC219', 0, 0, NULL),
(22, 'order_GuVO8XSxgqH2Ts', 100, 'INR', 0, '2', '2021-04-03 15:56:10', NULL, NULL, NULL, 'RC220', 0, 0, NULL),
(23, 'order_GuVRFOloXQtndH', 100, 'INR', 0, '2', '2021-04-03 15:59:08', NULL, NULL, NULL, 'RC221', 0, 0, NULL),
(24, 'order_GuVRo7jeKi6JCN', 100, 'INR', 0, '2', '2021-04-03 15:59:39', NULL, NULL, NULL, 'RC222', 0, 0, NULL),
(25, 'order_GuVzR9MDgLgwKg', 100, 'INR', 0, '2', '2021-04-03 16:31:30', NULL, NULL, NULL, 'RC223', 0, 0, NULL),
(26, 'order_GuW1mwO0XqPya3', 100, 'INR', 0, '2', '2021-04-03 16:33:43', NULL, NULL, NULL, 'RC224', 0, 0, NULL),
(27, 'order_GuW2wPxUhD3d1n', 100, 'INR', 0, '2', '2021-04-03 16:34:49', NULL, NULL, NULL, 'RC225', 0, 0, NULL),
(28, 'order_GuW3f77MATxcnN', 100, 'INR', 0, '2', '2021-04-03 16:35:29', NULL, NULL, NULL, 'RC226', 0, 0, NULL),
(29, 'order_GuW4aWXkouWuQs', 100, 'INR', 0, '2', '2021-04-03 16:36:22', NULL, NULL, NULL, 'RC227', 0, 0, NULL),
(30, 'order_GuW5RYFLWp8Miy', 100, 'INR', 1, '2', '2021-04-03 16:37:11', '2021-04-03 16:38:23', 'pay_GuW6dCVmJP1dRb', '29a2cda12728edbeb16d7bece5b99844b69f3dfe77c9c26025d7acd6b9ebf465', 'RC228', 10, 110, '{\"code\":\"BAD_REQUEST_ERROR\",\"description\":\"Payment failed\",\"source\":\"gateway\",\"step\":\"payment_authorization\",\"reason\":\"payment_failed\",\"metadata\":{\"payment_id\":\"pay_GuW6Rt6NfDHC0I\",\"order_id\":\"order_GuW5RYFLWp8Miy\"}}'),
(31, 'order_GuWAVR2zpaFtkN', 100, 'INR', 0, '2', '2021-04-03 16:39:22', NULL, NULL, NULL, 'RC229', 0, 0, NULL),
(32, 'order_GuWAW0K7aT1fhJ', 100, 'INR', 1, '2', '2021-04-03 16:39:22', '2021-04-03 16:39:46', 'pay_GuWAmeEenV2KKZ', '9441042eee617d1d71445cffc8eba9e449046f0c898e9f8ea492c8e8da891ed2', 'RC229', 10, 120, NULL),
(33, 'order_GuWB7USF61Pk8v', 100, 'INR', 2, '2', '2021-04-03 16:39:56', '2021-04-03 16:40:11', 'pay_GuWBIIHEdjOgNY', NULL, 'RC231', 0, 0, '{\"code\":\"BAD_REQUEST_ERROR\",\"description\":\"Payment failed\",\"source\":\"gateway\",\"step\":\"payment_authorization\",\"reason\":\"payment_failed\",\"metadata\":{\"payment_id\":\"pay_GuWBIIHEdjOgNY\",\"order_id\":\"order_GuWB7USF61Pk8v\"}}');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `payment_history`
--
ALTER TABLE `payment_history`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `payment_history`
--
ALTER TABLE `payment_history`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
